function sendPostData(event, field_id, error_message, content_amount, no_of_pcs, stock_id, product_version_id, controller_url_method) {

    var no_of_pcs_val = document.getElementById(no_of_pcs).value;
    var content_amount_id = document.getElementById(content_amount).value;

    let positive_int_regex = /^\+?[1-9][\d]*$/;
    if (!no_of_pcs_val.match(positive_int_regex)) {
        validate(field_id, error_message);
        return false;
    } else {
        $('#' + field_id + '-col div').removeClass('has-error');
        $('#' + field_id).removeClass('has-error');
        $('#' + field_id + '-col div span').remove();
        $.ajax({
            type: 'POST',
            url: controller_url_method,
            data: {
                no_of_pcs: no_of_pcs_val,
                content_amount_id: content_amount_id,
                stock_id: stock_id,
                product_version_id: product_version_id
            },
            success: function(data) {
                if (data.success) {
                    $('#product-added-to-stock-modal').modal('show');
                }
            },
            error: function() {
                alert("false submission fail");
            }

        });
        event.preventDefault();

        var pcs_field = document.getElementById(no_of_pcs);
        pcs_field.value = 1;
    }
}