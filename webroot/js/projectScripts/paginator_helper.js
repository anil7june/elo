$(document).ready(function(){
  var a = $("ul.myPagination form");
  var allowed = ['make_id', 'type_id', 'cat_code', 'ecr_code',
   'min_price', 'max_price', 'min_weight', 'max_weight' , 'product_status_id', 'advanced_search_button'];
  var params = getUrlVars(allowed);

  a.css('display', 'inline-block');
  a.css('width', '60px');

  var name, value;
  for(var i = 0; i < params.length; i++){
    name = decodeURIComponent(params[i][0]);
    value = decodeURIComponent(params[i][1])
    a.append("<input type=\"hidden\" name=\"" + name + "\"" +"value=\"" + value + "\"" + "/>");
  }

});


function getUrlVars(params){
  var vars = [], hash;
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for(var i = 0; i < hashes.length; i++){
      hash = hashes[i].split('=');
      if (params.includes(hash[0]))
        vars.push([hash[0], hash[1]]);
  }
  return vars;
}
