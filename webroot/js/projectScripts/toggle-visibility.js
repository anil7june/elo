function ToggleVisibility(divClass, defaultDisplay)
{
    var els = document.getElementsByClassName(divClass);
    for(var i = 0; i < els.length; i++)
    {
        els[i].style.display = els[i].style.display == defaultDisplay ? "none" : defaultDisplay;
    }
}
