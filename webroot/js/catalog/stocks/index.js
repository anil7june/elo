function createFormsValidationConfig() {
  return {
    'edit-form': {
      'no-of-pcs': nonEmptyFieldPositiveInteger('no-of-pcs'),
      'content-amount-id': nonEmptyFieldConf('content-amount-id'),
      'price': nonEmptyFieldTwoDecimalPlaces('price'),
    },
    'create-stock-form': {
      'name': nonEmptyFieldConf('name'),
      'margin': nonEmptyFieldTwoDecimalPlaces('margin'),
      'usd-margin': nonEmptyFieldTwoDecimalPlaces('usd-margin'),
    },
  }
}

function sendFormDataToServer(form) {
  var url = form.attr('action');
  $.ajax({
    type: "POST",
    url: url,
    data: form.serialize(),
    success: function () {
      location.reload();
    }
  });
}

function catalogStocksIndex() {
  let formsValidationConfig = createFormsValidationConfig();
  $('#edit-form').submit(function (e) {
    e.preventDefault();
    if (validateForm($(this), formsValidationConfig['edit-form'])) {
      sendFormDataToServer($(this));
    } else {
      return false;
    }
  });
  $('#create-stock-form').submit(function () {
    return validateForm($(this), formsValidationConfig['create-stock-form']);
  })
}

$(document).ready(function () {
  $.when(
    $.getScript('/webroot/js/forms/form_validator.js'),
    $.getScript('/webroot/js/forms/form_field_validator.js'),
    $.getScript('/webroot/js/forms/validation_message_controller.js'),
    $.getScript('/webroot/js/forms/form_config_helper.js'),
  ).done(function () {
    catalogStocksIndex();
  });
});