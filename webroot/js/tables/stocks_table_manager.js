/** if(time) divide this file into several files */

function stockId(arrow) {
    return arrow.attr("id").split('-')[0];
}

function toggleStockInfoDOM(stock_id) {
    if (stockInfoDOM(stock_id).hasClass('hide')) {
        stockInfoDOM(stock_id).removeClass('hide');
        showArrowDown(stock_id);
    } else {
        stockInfoDOM(stock_id).addClass('hide');
        showArrowUp(stock_id);
    }
}

function showArrowDown(stock_id) {
    arrowDown(stock_id).removeClass('hide');
    arrowUp(stock_id).addClass('hide');
}

function showArrowUp(stock_id) {
    arrowUp(stock_id).removeClass('hide');
    arrowDown(stock_id).addClass('hide');
}

function arrowDown(stock_id) {
    return $('#' + stock_id + '-arrow-down');
}

function arrowUp(stock_id) {
    return $('#' + stock_id + '-arrow-up');
}

function dataInserted(stock_id) {
    return stockInfoOnlyTable(stock_id).has('tbody tr').length > 0 || stockSummaryTable(stock_id).has('tbody tr').length > 0;
}

function insertData(action_url, stock_id) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: action_url,
        data: {
            stock_id: stock_id
        },
        success: function (data) {
            insertDataToDOMs(data, stock_id);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function insertDataToDOMs(data, stock_id) {

    insertStockProductsDataToStockInfoOnlyTable(data.stock_products, stock_id);
    insertStockSummaryDataToStockSummaryTable(data.stock_summary, stock_id);
}

function insertStockProductsDataToStockInfoOnlyTable(stock_products_data, stock_id) {
    stockInfoOnlyTableRows(stock_products_data).forEach(function (row, _) {
        addRowToTable(stockInfoOnlyTable(stock_id), row);
    });
}

function insertStockSummaryDataToStockSummaryTable(stock_summary_data, stock_id) {
    stockSummaryTableRows(stock_summary_data).forEach(function (row, _) {
        addRowToTable(stockSummaryTable(stock_id), row);
    });
}

function stockSummaryTableRows(stock_summary_data) {
    rows = [];
    stock_summary_data.forEach(function (row_data, _) {
        rows.push(createStockSummaryRow(row_data));
    });
    return rows;
}

function createStockSummaryRow(row_data) {
    let row = $('<tr>');
    Object.entries(row_data).forEach(function (pair, index) {
        addStockSummaryKeyValuePairToRow(row, pair);
        if (index !== Object.keys(row_data).length - 1) {
            addContentToRow(row, '');
        }
    });
    return row
}

function addStockSummaryKeyValuePairToRow(row, pair) {
    pair.forEach(function (content, _) {
        addContentToRow(row, content);
    });
}

function stockInfoOnlyTableRows(stock_products_data) {
    let rows = [];
    stock_products_data.products_list.forEach(function (product_data, _) {
        rows.push(cretateStockProductRow(product_data, stock_products_data.info));
    });
    return rows;
}

function cretateStockProductRow(product_data, info) {
    let row = $('<tr>');
    addStaticDataToStockInfoOnlyRow(product_data, row);
    addPricesToStockInfoOnlyRow(product_data, info, row);
    return row;
}

function addStaticDataToStockInfoOnlyRow(product_data, row) {

    stockInfoOnlyTableRowStaticData(product_data).forEach(function (content, _) {
        addContentToRow(row, content);
    });
}

function stockInfoOnlyTableRowStaticData(product_data) {

    return [
        productImage(product_data.product_version_image),
        product_data.product_version.cat_code,
        product_data.no_of_pcs,
        product_data.content_amount.name,
    ];
}

function addPricesToStockInfoOnlyRow(product_data, info, row) {

    stockInfoOnlyTableRowPrices().forEach(function (price, _) {
        addContentToRow(row, product_data[price] + " " + info.currency_code);
    });
}

function stockInfoOnlyTableRowPrices() {
    return ['catalogue_value', 'user_value', 'profit'];
}

function addContentToRow(row, content) {

    row.append(createColumnWithContent(content))
}

function createColumnWithContent(content) {
    return $('<td>').append(content);
}

function productImage(image_name) {
    let image_path = (image_name === undefined) ? "/img/default-product-image.png" : "/uploads/cat_images/" + image_name;
    return $('<img>').attr('src', image_path).attr('class', 'product-image');
}

function stockInfoDOM(stock_id) {
    return $('#stock-' + stock_id + '-details');
}

function stockHeaderRow(stock_id) {
    return $('#stock-' + stock_id + '-header-row');
}

function stockInfoOnlyTable(stock_id) {
    return $('#stock-' + stock_id + '-info-only-table');
}

function stockSummaryTable(stock_id) {
    return $('#stock-' + stock_id + '-summary-table');
}

function addRowToTable(table, row) {
    table.find('tbody').append(row);
}