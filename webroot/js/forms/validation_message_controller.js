
function addValidationMessage(form_group, message) {
    addErrorClassToFormGroup(form_group);
    addErrorClassToInput(getInputFromFormGroup(form_group));
    addErrorMessage(form_group, message);
}

function removeValidationMessage(form_group) {
    form_group.removeClass(errorClass());
    removeErrorMessage(form_group);
}

function removeErrorMessage(form_group) {
    form_group.find('span.help-block.error-message').remove();
}

function addErrorClassToFormGroup(form_group) {
    addClassAsFirst(form_group, errorClass());
}

function addErrorClassToInput(form_input) {
    addClass(form_input, errorClass());
}

function addErrorMessage(form_group, message) {
    if (hasMessage(form_group) === false) {
        appendMessage(form_group, message);
    }
}

function hasMessage(form_input) {
    return form_input.find('span.help-block.error-message').length > 0;
}

function appendMessage(form_input, message) {
    form_input.append('<span class="help-block error-message">' + message + '</span>');
}

function errorClass() {
    return 'has-error';
}

function getInputFromFormGroup(form_group) {
    return form_group.find('input');
}

function addClass(dom, str_class) {
    dom.addClass(str_class);
}

function addClassAsFirst(dom, str_class) {

    let current_classes = dom.prop("class");
    dom.removeClass(current_classes).addClass(str_class + " " + current_classes);
}