function nonEmptyFieldConf(field_name) {
	return {
		id: field_name,
		condition: 'none',
		allowEmpty: false,
		message: 'This field can not be empty'
	}
}

function nonEmptyFieldTwoDecimalPlaces(field_name) {
	return {
		id: field_name,
		condition: 'positiveTwoDecimalPlacesNumber',
		allowEmpty: false,
		message: 'Insert positive number with two decimal places'
	}
}

function nonEmptyFieldThreeDecimalPlaces(field_name) {
	return {
		id: field_name,
		condition: 'positiveThreeDecimalPlacesNumber',
		allowEmpty: false,
		message: 'Insert positive number with three decimal places'
	}
}

function dateAllowEmptyFieldConf(field_name) {
	return {
		id: field_name,
		condition: 'dateMMDDYYYY',
		allowEmpty: true,
		message: 'Error: required date format is MM/DD/YYYY',
	}
}

function nonEmptyFieldPositiveInteger(field_name) {
	return {
		id: field_name,
		condition: 'positiveIntegerNumber',
		allowEmpty: false,
		message: 'Error: positive number required'
	};
}

function nonEmptyFieldNonNegativeInteger(field_name) {
	return {
		id: field_name,
		condition: 'nonNegativeIntegerNumber',
		allowEmpty: false,
		message: 'Error: non negative number required'
	};
}