function validateForm(form, form_params) {
    var result = true;
    form.find('.form-group').each(function () {
        processFormGroup($(this), form_params);
        if (!processFormGroup($(this), form_params)) {
            result = false;
        }
    });
    return result;
}

function processFormGroup(form_group, form_params) {
    let field_params = getFieldParams(form_group, form_params);
    if (field_params != undefined) {
        if (validationMessageRequired(field_params)) {
            addValidationMessage(form_group, field_params.message);
            return false;
        } else {
            removeValidationMessage(form_group);
        }
    }
    return true;
}

function validationMessageRequired(field_params) {
    return !validateField(getFormControlValue(field_params.id), field_params.condition, field_params.allowEmpty);
}

function messageAlreadyDisplayed(field_params) {
    return $('#' + field_params.id).parent().find('span').length > 0;
}

function getFormControlId(form_group) {
    return form_group.find('.form-control').attr('id');
}

function getFieldParams(form_group, params) {
    let field_id = getFormControlId(form_group);
    return params[field_id];
}

function getFormControlValue(id) {
    return $('#' + id).val();
}