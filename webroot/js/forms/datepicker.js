
function initializeDatepickers(ids) {
    ids.forEach(function (id, _) {
        initializeDatepicker(id);
    });
}

function initializeDatepicker(id) {
    start(id);
    setFocusEvent(id);
    setFocusoutEvent(id);
}

function start(id) {
    getDatepickerById(id).datepicker({
        uiLibrary: 'bootstrap4'
    });
}

function setFocusEvent(id) {
    getDatepickerById(id).focus(function () {
        $(this).parent().css('border-color', '#FF9E66');
    });
}

function setFocusoutEvent(id) {
    getDatepickerById(id).focusout(function () {
        $(this).parent().css('border-color', '#b0b0b0');
    });
}

function getDatepickerById(id) {
    return $('#' + id);
}