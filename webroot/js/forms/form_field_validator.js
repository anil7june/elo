let validationConditions = {
    'nonNegativeIntegerNumber': nonNegativeIntegerNumber,
    'positiveTwoDecimalPlacesNumber': positiveTwoDecimalPlacesNumber,
    'positiveThreeDecimalPlacesNumber': positiveThreeDecimalPlacesNumber,
    'positiveIntegerNumber': positiveIntegerNumber,
    'dateMMDDYYYY': dateMMDDYYYY,
    'validPassword': validPassword,
    'notEmpty': notEmpty,
    'none': noConditions,
}

function validateField(value, condition, allowEmpty) {
    if (!value.trim()) {
        return allowEmpty;
    }
    return validationConditions[condition](value);
}

function validPassword(value) {
    return passwordRegex().test(value);
}

function positiveTwoDecimalPlacesNumber(value) {
    return positiveTwoDecimalPlacesRegex().test(value);
}

function positiveThreeDecimalPlacesNumber(value) {
    return positiveThreeDecimalPlacesRegex().test(value);
}

function positiveIntegerNumber(value) {
    return positiveIntegerRegex().test(value);
}

function nonNegativeIntegerNumber(value) {
    return positiveIntegerNumber(value) || value == 0;
}

function dateMMDDYYYY(value) {
    return MMDDYYYYDateRegex().test(value);
}

function notEmpty(value) {
    return value !== "";
}

function noConditions(value) {
    return true;
}

function MMDDYYYYDateRegex() {
    return /^(?:(0[1-9]|1[012])[\/.](0[1-9]|[12][0-9]|3[01])[\/.](19|20)[0-9]{2})$/;
}

function positiveTwoDecimalPlacesRegex() {
    return /^[-+]?[0-9]+\.[0-9]{2}$/;
}

function positiveThreeDecimalPlacesRegex() {
    return /^[-+]?[0-9]+\.[0-9]{3}$/;
}

function positiveIntegerRegex() {
    return /^\+?[1-9][\d]*$/;
}

function passwordRegex() {
    return /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])^[a-zA-Z0-9]{8,15}$/;
}
