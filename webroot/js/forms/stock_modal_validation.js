function validate(field_name, message) {
    $('#' + field_name + '-col .form-group').removeClass("form-group text");
    $('#' + field_name + '-col div').addClass("form-group has-error text");
    $('#' + field_name).addClass("has-error");
    if ($('#' + field_name + '-col div span').length == 0) {
        $('#' + field_name + '-col div').append('<span class="help-block error-message">' + message + '</span>');
    }
}

function remove_validation(field_name) {
    $('#' + field_name + '-col .form-group').removeClass("has-error");
    $('#' + field_name + '-col .form-group').find('span').remove();
}