function adminCurrenciesIndex() {
  $.ajax({
    type: "POST",
    url: window.location.origin + '/admin/currencies/get-non-based-currencies-ids',
    processData: false,
    contentType: false,
    data: {},
  }).done(function (response) {
    let formConfig = getUpdateCurrenciesFormConfig(response.ids)
    $("#update-currencies").submit(function (event) {
      return validateForm($(this), formConfig);
    })
  })
}

function getUpdateCurrenciesFormConfig(ids) {
  result = {};
  ids.forEach(obj => {
    result[obj['id']] = nonEmptyFieldThreeDecimalPlaces(obj['id'])
  });
  return result;
}

$(document).ready(function () {
  $.when(
    $.getScript('/webroot/js/forms/form_validator.js'),
    $.getScript('/webroot/js/forms/form_field_validator.js'),
    $.getScript('/webroot/js/forms/validation_message_controller.js'),
    $.getScript('/webroot/js/forms/form_config_helper.js'),
    $.getScript('/webroot/js/element/product_form.js'),
  ).done(function () {
    adminCurrenciesIndex();
  });
});