function getUpdatePgmPricesFormConfig() {
  params = [
    'pt_price', 'pt',
    'pd_price', 'pd',
    'rh_price', 'rh'
  ]
  result = {}
  params.forEach(element => {
    result[element] = nonEmptyFieldNonNegativeInteger(element)
  });
  return result;
}

function adminPgmPricesIndex() {
  $("#update-pgm-prices-form").submit(function (event) {
    return validateForm($(this), getUpdatePgmPricesFormConfig());
  })
}

$(document).ready(function () {
  $.when(
    $.getScript('/webroot/js/forms/form_validator.js'),
    $.getScript('/webroot/js/forms/form_field_validator.js'),
    $.getScript('/webroot/js/forms/validation_message_controller.js'),
    $.getScript('/webroot/js/forms/form_config_helper.js'),
    $.getScript('/webroot/js/element/product_form.js'),
  ).done(function () {
    adminPgmPricesIndex();
  });
});