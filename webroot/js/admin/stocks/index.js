function createFormsValidationConfig() {
	return {
		'date-from': dateAllowEmptyFieldConf('date-from'),
		'date-to': dateAllowEmptyFieldConf('date-to'),
	}
}

function adminStocksIndex() {
	let formsValidationConfig = createFormsValidationConfig();
	$("#search-filter").submit(function () {
		return validateForm($(this), formsValidationConfig);
	});
	initializeDatepickers([
		'date-to',
		'date-from',
	]);
}

$(document).ready(function () {
	$.when(
		$.getScript('/webroot/js/forms/form_validator.js'),
		$.getScript('/webroot/js/forms/form_field_validator.js'),
		$.getScript('/webroot/js/forms/validation_message_controller.js'),
		$.getScript('/webroot/js/forms/form_config_helper.js'),
		$.getScript('/webroot/js/admin/stocks/datepicker.js'),
	).done(function () {
		adminStocksIndex();
	});
});