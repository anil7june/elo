var applyTestIcon = '<img src="/img/icons/icon-checked-tests.png" class="test-ico clickable apply">';
var disableTestIcon = '<img src="/img/icons/icon-close-black.png" class="test-ico clickable disable">';
var removeTestIcon = '<img src="/img/stock_icons/icon-remove-stock.png" class="test-ico clickable remove">';

let testStructure = ['pt', 'pd', 'rh', 'analysis_type_name', 'status'];

let form_images_data = new FormData();

function getFormData() {
	let requestData = getImagesData();
	requestData.append('product', JSON.stringify(getProductData()));
	requestData.append('tests', JSON.stringify(getTestData()));
	return requestData;
}

function bindValidationToCreateProductForm(formConfig) {
	$("#create-product-form").submit(function (e) {
		e.preventDefault();
		if (validateForm($(this), formConfig)) {
			$.ajax({
				type: "POST",
				url: window.location.origin + '/admin/products/add-request-handler',
				processData: false,
				contentType: false,
				data: getFormData(),
			}).done(function (response) {
				console.log(response);
				window.location.replace(response["redirect_url"]);
			});
		}
	});
}

function getImagesData() {
	let count = 0;
	let form_data = new FormData();
	$('.multi-images-input').each(function () {
		let file = $(this)[0].files[0];
		if (file) {
			form_data.append('file_' + count, file, file.name);
			count++;
		}
	})
	return form_data;
}

function getTestData() {

	result = [];
	let testIndex = 0;
	$("tr.js-added-row").each(function () {
		let testParamIndex = 0;
		result.push({});
		$(this).children().each(function () {
			if (testParamIndex < testStructure.length) {
				result[testIndex][testStructure[testParamIndex]] = this.textContent;
			}
			testParamIndex++;
		})
		testIndex++;
	});
	return result;
}

function getProductData() {
	let result = {};
	$("#create-product-form").serializeArray().forEach(function (formField, index) {
		result[formField["name"]] = formField["value"];
	});
	return result;

}

function hiddenFormField(testIndex, testParamIndex, value) {
	return $("<input>").attr({
		type: "hidden",
		id: "test-" + testIndex + "-" + testStructure[testParamIndex],
		name: "test_" + testIndex + "_" + testStructure[testParamIndex].replace('-', '_'),
		value: value
	});
}

function bindSubmitActionToCreateTestForm(formConfig) {
	$("#create-test-form").submit(function (e) {
		e.preventDefault();
		if (validateForm($(this), formConfig)) {
			showTestsTable();
			addRowToTestsTable(getRowData($(this)));
			bindClickEventsToClickableIcons();
			resetModalForm();
			$("#close-create-new-test-form-modal").click();
		}
	});
}

function resetModalForm() {
	$('form#create-test-form input[type="text"]').each(function () {
		$(this).val("");
	})
	$('form#create-test-form select').each(function () {
		$(this).val("");
	})
}

function addRowToTestsTable(rowData) {
	$("#tests-table").append(createRow(rowData));
}

function bindClickEventsToClickableIcons() {
	$(".test-ico.clickable").each(function () {
		$(this).on("click", function () {
			if ($(this).hasClass("apply")) {
				applyTest($(this));
			} else if ($(this).hasClass("disable")) {
				deactivateAllTests();
			} else if ($(this).hasClass("remove")) {
				removeTest($(this));
			}
		});
	});
}

function applyTest(clickableIcon) {
	deactivateAllTests();
	row = clickableIcon.parent().parent();
	row.children().each(function () {
		if ($(this).children(":first").hasClass("clickable apply")) {
			$(this).empty();
			$(this).append(disableTestIcon);
			$(this).children(":first").on("click", function () {
				deactivateAllTests();
			});
		} else if (this.textContent == "Disabled") {
			$(this).text("Applied");
		}
	});
}

function deactivateAllTests() {
	$("tr.js-added-row").each(function () {
		$(this).children().each(function () {
			if (this.textContent == "Applied") {
				$(this).text("Disabled");
			}
			if ($(this).children(":first").hasClass("test-ico clickable disable")) {
				$(this).empty();
				$(this).append(applyTestIcon);
				$(this).children(":first").on("click", function () {
					applyTest($(this));
				})
			}
		});
	});
}

function removeTest(clickableIcon) {
	clickableIcon.parent().parent().remove();
	if ($(".js-added-row").length == 0) {
		hideTestTable();
	}
}

function createRow(rowData) {
	row = $("<tr>").addClass("js-added-row");
	rowData.forEach(function (columnContent, _) {
		row = addCollumnToRow(row, columnContent);
	});
	return row;
}

function getRowData(form) {
	let paramsToDisplay = ['pt', 'pd', 'rh', 'analysis_type_id'];
	let result = [];
	form.serializeArray().forEach(function (formField, index) {
		if (paramsToDisplay.includes(formField.name)) {
			result.push(getValue(formField.name, formField.value));
		}
	});
	result.push('Disabled');
	result.push(applyTestIcon);
	result.push(removeTestIcon);
	return result;
}

function getValue(key, value) {
	if (key == 'analysis_type_id') {
		return getAnalysisTypeName(value);
	}
	return value;
}

function getAnalysisTypeName(id) {
	let result = "Unknown";
	$("#analysis-type-id").children().each(function () {
		if (this.value == id) {
			result = this.text;
		}
	});
	return result;
}

function addCollumnToRow(row, columnContent) {
	let column = $("<td>").addClass("border-bottom border-right normal-content text-centered").append(columnContent);
	return row.append(column);
}

function showTestsTable() {
	$("#test-table-container").removeClass("hide");
}

function hideTestTable() {
	$("#test-table-container").addClass("hide");
}


function adminProductsAdd() {
	let formsValidationConfig = createFormsValidationConfig(); // imported from product_form.js
	bindValidationToCreateProductForm(formsValidationConfig['create-product-form']);
	bindSubmitActionToCreateTestForm(formsValidationConfig['create-test-form']);
}

$(document).ready(function () {
	$.when(
		$.getScript('/webroot/js/forms/form_validator.js'),
		$.getScript('/webroot/js/forms/form_field_validator.js'),
		$.getScript('/webroot/js/forms/validation_message_controller.js'),
		$.getScript('/webroot/js/forms/form_config_helper.js'),
		$.getScript('/webroot/js/element/product_form.js'),
	).done(function () {
		adminProductsAdd();
	});
});