function bindValidationToForm(form_id, validation_config) {
  $("#" + form_id).submit(function () {
    return validateForm($(this), validation_config);
  });
}

function adminProductsEdit() {
  let formsValidationConfig = createFormsValidationConfig(); // imported from product_form.js
  for (const [form_id, validation_config] of Object.entries(formsValidationConfig)) {
    bindValidationToForm(form_id, validation_config);
  }
}

$(document).ready(function () {
  $.when(
    $.getScript('/webroot/js/forms/form_validator.js'),
    $.getScript('/webroot/js/forms/form_field_validator.js'),
    $.getScript('/webroot/js/forms/validation_message_controller.js'),
    $.getScript('/webroot/js/forms/form_config_helper.js'),
    $.getScript('/webroot/js/element/product_form.js'),
  ).done(function () {
    adminProductsEdit();
  });
});