function bindValidationToForm(form_id, validation_config) {
	console.log(validation_config)
	$("#" + form_id).submit(function () {
		return validateForm($(this), validation_config);
	})
}

function createFormsValidationConfig() {
	return {
		'create-brand-form': {
			'brand-name': nonEmptyFieldConf('brand-name')
		},
		'create-model-form': {
			'model-name': nonEmptyFieldConf('model-name'),
			'model-make-id': nonEmptyFieldConf('model-make-id')
		},
		'create-analysis-type-form': {
			'model-name': nonEmptyFieldConf('model-name')
		}
	}
}

function adminProductsMain() {
	let formsValidationConfig = createFormsValidationConfig();
	for (const [form_id, validation_config] of Object.entries(formsValidationConfig)) {
		bindValidationToForm(form_id, validation_config);
	}
}

$(document).ready(function () {
	$.when(
		$.getScript('/webroot/js/forms/form_validator.js'),
		$.getScript('/webroot/js/forms/form_field_validator.js'),
		$.getScript('/webroot/js/forms/validation_message_controller.js'),
		$.getScript('/webroot/js/forms/form_config_helper.js'),
	).done(function () {
		adminProductsMain();
	});
});