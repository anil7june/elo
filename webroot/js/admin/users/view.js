function getFormConfig() {
  let non_negative_int_non_empty_parameters = [
    'pt-pd', 'pd-pd', 'rh-pd',
    'pt-rr', 'pd-rr', 'rh-rr',
    'pt-rc', 'pd-rc', 'rh-rc',
    'pt-lc', 'pd-lc', 'rh-lc',
    'pt-ld', 'pd-ld', 'rh-ld',
    'du-pp', 'du-value', 'coins'
  ];
  let formConfig = {}
  non_negative_int_non_empty_parameters.forEach(element => {
    formConfig[element] = nonEmptyFieldNonNegativeInteger(element)
  });
  formConfig['tc'] = nonEmptyFieldTwoDecimalPlaces('tc');
  return formConfig;
}

function bindValidationToEditUserForm() {

  $("#edit-user-form").submit(function (e) {
    console.log(getFormConfig());
    return validateForm($(this), getFormConfig());
  })
}

$(document).ready(function () {
  $.when(
    $.getScript('/webroot/js/forms/form_validator.js'),
    $.getScript('/webroot/js/forms/form_field_validator.js'),
    $.getScript('/webroot/js/forms/validation_message_controller.js'),
    $.getScript('/webroot/js/forms/form_config_helper.js'),
  ).done(function () {
    bindValidationToEditUserForm();
  });
});