$(document).ready(function () {

  var inputs = ['make_id', 'type_id', 'cat_code', 'ecr_code',
    'min_price', 'max_price', 'min_weight', 'max_weight', 'product_status_id', 'region_code'];
  var allInputs = $(":input");

  for (var i = 0; i < allInputs.length; i++) {
    let singleItem = allInputs[i];
    if (inputs.includes(singleItem.name) && singleItem.value)
      return;
  }
  $('#search_form').hide();


});

$(function () {

  $('#show_form').on('click', function () {
    $('#search_form').toggle(50);
  });
});
