function initEditStockProductModal() {
	$('#edit-stock-product-modal').on('show.bs.modal', function (e) {
		var data = e.relatedTarget.dataset.product;
		var extracted_data = jQuery.parseJSON(data);
		$('#content-amount-id').val(extracted_data.content_amount_id);
		$('#no-of-pcs').val(extracted_data.no_of_pcs);
		$('#id').val(extracted_data.id);
		$('#price').val(extracted_data.catalogue_value);
	});
}

$(document).ready(function () {
	initEditStockProductModal();
});