function createFormsValidationConfig() {
	return {
		'create-product-form': {
			'cat-code': nonEmptyFieldConf('cat-code'),
			'make-id': nonEmptyFieldConf('make-id'),
			'additional-ref-data': nonEmptyFieldConf('additional-ref-data'),
			'description': nonEmptyFieldConf('description'),
			'cat-make-id': nonEmptyFieldConf('cat-make-id'),
			'weight': nonEmptyFieldTwoDecimalPlaces('weight'),
			'product-status-id': nonEmptyFieldConf('product-status-id')
		},
		'create-test-form': {
			'pt': nonEmptyFieldPositiveInteger('pt'),
			'pd': nonEmptyFieldPositiveInteger('pd'),
			'rh': nonEmptyFieldPositiveInteger('rh'),
			'analysis-type-id': nonEmptyFieldConf('analysis-type-id')
		}
	};
}