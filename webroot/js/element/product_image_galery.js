var images_paths = [];
var image_index = 0;

$(document).ready(function () {
  let product_id = $("#product-id-hide").val();
  console.log(product_id);
  getProductImages(product_id);
})

function getProductImages(product_id) {
  $.ajax({
    type: "POST",
    url: window.location.origin + "/catalog/products/get-product-image-paths-to-display",
    data: { product_id: product_id },
  }).done(function (response) {
    console.log(response);
    images_paths = response.image_paths;
    initializeGalery()
  });
}

function initializeGalery() {
  insertImageToGalery(image_index);
  bindEventsToLeftRightArrows();
}

function insertImageToGalery(index) {
  $("#main-galery-image").attr('src', images_paths[index]);
}

function bindEventsToLeftRightArrows() {
  $("#prev-image").click(function () {
    image_index = (image_index - 1 + images_paths.length) % images_paths.length;
    insertImageToGalery(image_index);
  })
  $("#next-image").click(function () {
    image_index = (image_index + 1) % images_paths.length;
    insertImageToGalery(image_index);
  })
}
