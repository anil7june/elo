function initImageModal(product_id) {
    $.ajax({
        type: "POST",
        url: window.location.origin + "/catalog/products/get-product-image-paths",
        data: { product_id: product_id },
    }).done(function (response) {
        console.log(response);
        createModalWithProductImages(response.image_paths)
    });
}

function createRowImage(image_path) {
    row = $('<div></div>').addClass('row image-row');
    image = $('<img />').attr('src', image_path)
    row.append(image);
    return row;
}

function createModalWithProductImages(image_paths) {
    image_paths.forEach(image_path => {
        $("#product-image-modal-body").append(createRowImage(image_path))
    });
}

function imgPopup(product_id) {
    initImageModal(product_id);
    $('.modal.img-modal').modal('show');
}

$(document).ready(function () {
    $('.modal.img-modal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('.modal.img-modal').modal('hide');
    $("#close-img-modal").click(function () {
        $("#product-image-modal-body").empty();
    })
})