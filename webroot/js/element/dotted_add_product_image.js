function createNewFileInput(count) {
  let row = $('<div></div>').addClass('row');
  let col_with_input = $('<div></div>').addClass('col-md-10');
  let input = $('<input/>').addClass('multi-images-input').attr('id', 'file-' + count).attr('name', 'file_' + count).attr('type', 'file');
  col_with_input.append(input);

  let col_with_discard_icon = $('<div></div>').addClass('col-md-2');
  let discard_icon = $('<img>').attr('src', '/img/stock_icons/icon-remove-stock.png').attr('class', 'test-ico image-ico clickable');
  col_with_discard_icon.append(discard_icon);

  row.append(col_with_input);
  row.append(col_with_discard_icon);
  return row;
}

function bindRemoveEventToDiscardIcons() {
  $('.test-ico.image-ico.clickable').each(function () {
    $(this).on('click', function () {
      let row = $(this).parent().parent().remove();
    })
  })
}

$(document).ready(function () {
  bindRemoveEventToDiscardIcons()
  $('#add-more-images').click(function (event) {
    event.preventDefault();
    let count = $('#file-inputs-data input').length;
    $("#file-inputs-data").append(createNewFileInput(count));
    bindRemoveEventToDiscardIcons()
  })

})