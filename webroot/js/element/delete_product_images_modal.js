function submitFormData(form_data) {
  $.ajax({
    type: "POST",
    url: window.location.origin + '/admin/products/delete-images-from-product',
    data: form_data,
    processData: false,
    contentType: false,
  }).done(function (response) {
    console.log(response);
    window.location.replace(response["redirect_url"]);
  });
}

$(document).ready(function () {
  $("#submit-delete-pictures-modal").click(function (event) {
    event.preventDefault();
    form = document.getElementById('delete-images-from-product-form');
    form_data = new FormData(form);
    submitFormData(form_data);
  })
})