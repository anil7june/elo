# web #

To set up the project you need to:

1. In the root directory run command "composer install"
2. Run the tools/upload.sh script (it will set the permissions, and set up the crontab - you need not to worry about the timezone differencies, the script will do it for you). Note: to run this script you will need python3 installed on your machine.

You can also:

1. Clear the cache (cakephp3 cache and composer cache), you need to run tools/clear_cache.sh
2. In folder webroot/uploads/cat_images there is a file default-product-image.png. This is the default image to be displayed, when product has no photo assigned. You can replace this file with
your default image. There is one restriction, name of this file has to be "default-product-image.png".

