#!/bin/bash
set -e
mysqldump --no-data --result-file=schema.sql "$@"
mysqldump --no-create-info --no-create-db --result-file=data.sql "$@"
