#!/bin/bash
#Script for uploading elemental from repo

#clone from repo and scp to the server

cd ~/

git clone https://ucwmiu@bitbucket.org/impicode/web.git
tar -zcvf elemental_stable.gz web
scp elemental_stable.gz toor@13.90.22.44:/var/www/html/stable

sudo rm -r web
sudo rm elemental_stable.gz
