import os


crontab_conf = {
    '/var/www/html/stable/web/bin/cake rhRates': (9, 11, 13, 15, 17),
    '/var/www/html/stable/web/bin/cake pdRates': (9, 13),
    '/var/www/html/stable/web/bin/cake ptRates': (9, 13),
}

self_update_line = '30 6 * * * python /var/www/html/stable/web/webroot/scripts/update_crontab.py'

username = "maciek"


def get_time_from_console_date_str(console_date_str: str) -> str:
    return console_date_str.split(",")[2].split(":")[0].strip()


def time_difference() -> int:
    polish_time_hour = get_time_from_console_date_str(os.popen(
        'TZ=Europe/Warsaw date').read())
    server_time_hour = get_time_from_console_date_str(os.popen(
        'date').read())
    return int(polish_time_hour) - int(server_time_hour)


def write_to_crontab(line: str):
    os.system(
        '(crontab -l 2>/dev/null; echo "' + line + '") | crontab -')


def create_crontab_line(command, hour):
    return "3 " + str(hour) + " * * * " + command


def write_command_to_crontab(command, hours):
    time_diff = time_difference()
    for hour in hours:
        write_to_crontab(create_crontab_line(command, hour - time_diff))


def update_crontab():
    os.system("crontab -r")
    write_to_crontab(self_update_line)
    for command in crontab_conf:
        write_command_to_crontab(command, crontab_conf[command])


update_crontab()
