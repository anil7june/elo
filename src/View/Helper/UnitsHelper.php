<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class UnitsHelper extends Helper
{

    public function dolar()
    {
        return '&#36;';
    }

    public function dolarToz()
    {
        return '&#36;/toz';
    }

    public function percent()
    {
        return '&#37;';
    }

    public function ppm()
    {
        return 'ppm';
    }

    public function dolarKg()
    {
        return '&#36;/kg';
    }

    public function kg()
    {
        return 'kg';
    }

    public function getCurrencySymbol($currency_code)
    {
        return TableRegistry::get('Currencies')->getCurrencySymbol($currency_code);
    }

}
