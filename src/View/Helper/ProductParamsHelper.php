<?php

namespace App\View\Helper;

use Cake\View\Helper;

class ProductParamsHelper extends Helper
{

    private static $_map_param_to_model_name = [
        'brand' => 'Make',
        'analysis-type' => 'AnalysisType',
        'model' => 'CatMake',
    ];

    public function modelName($param_name)
    {
        return self::$_map_param_to_model_name[$param_name];
    }
}
