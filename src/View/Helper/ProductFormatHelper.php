<?php

namespace App\View\Helper;

use App\Utility\Formater;
use Cake\View\Helper;

class ProductFormatHelper extends Helper
{

    public function formatNumber($value, $user_id)
    {

        return Formater::formatNumber($value, $user_id);
    }

    public function imagePath($image_name = null)
    {
        return Formater::imagePath($image_name);
    }

}
