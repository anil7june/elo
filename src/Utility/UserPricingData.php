<?php

// src/Utility/UserPricingData.php
namespace App\Utility;

use Cake\ORM\TableRegistry;

class UserPricingData {

  public $_elements = [
    'Pt', 'Pd', 'Rh'
  ];

  public $_RR = [];
  public $_PD = [];
  public $_RC = [];
  public $_TC = 0;
  public $_DU_pp = 0;
  public $_DU_value = 0;

  public function setUserData($user_id) {

    $users = TableRegistry::get('Users');
    $user = $users->get($user_id);


    $this->setUserPD($user);
    $this->setUserRC($user);
    $this->setUserRR($user);
    $this->setUserParams($user);

  }

  private function setUserRR($user) {

    $this->_RR = [
			'Pt' => ( (float)$user->pt_RR )*(0.01),
			'Pd' => ( (float)$user->pd_RR)*(0.01),
			'Rh' => ( (float)$user->rh_RR)*(0.01)
		];

  }

  private function setUserPD($user) {

		$this->_PD = [
			'Pt' => (float)$user->pt_PD,
			'Pd' => (float)$user->pd_PD,
			'Rh' => (float)$user->rh_PD
		];
  }

  private function setUserRC($user) {

		$this->_RC = [
			'Pt' => (float)$user->pt_RC,
			'Pd' => (float)$user->pd_RC,
			'Rh' => (float)$user->rh_RC
		];
  }

  private function setUserParams($user) {

    $this->_TC = $user->TC;
    $this->_DU_pp = $user->DU_pp;
    $this->_DU_value = $user->DU_value;

  }

}
