<?php
namespace App\Utility;

use Cake\ORM\TableRegistry;

require_once dirname(__DIR__) . '/Utility/simple_html_dom.php';

class PgmPriceUpdater
{
    private $url = 'https://www.kitco.com/market/';

    private $config = [
        'pt' => [
            'row' => 3,
            'col' => 3,
        ],
        'pd' => [
            'row' => 4,
            'col' => 3,
        ],
        'rh' => [
            'row' => 5,
            'col' => 4,
        ],
    ];

    public function updatePtPrice()
    {
        $this->updateElementPrice('Platinum', $this->getPriceFromHtml('pt'));
    }

    public function updatePdPrice()
    {
        $this->updateElementPrice('Palladium', $this->getPriceFromHtml('pd'));
    }

    public function updateRhPrice()
    {
        $this->updateElementPrice('Rhodium', $this->getPriceFromHtml('rh'));
    }

    private function updateElementPrice($element_full_name, $element_price)
    {
        TableRegistry::get('PgmPrices')->setElementPrice($element_full_name, $element_price);
    }

    private function getPriceFromHtml($element)
    {
        return $this->getElementPriceFromHtmlTable($this->getHtmlTable(), $element);
    }

    private function getHtmlTable()
    {
        return \file_get_html($this->url)
            ->find('div.table_container.spot_price', 0)
            ->find('table', 0);
    }

    private function getElementPriceFromHtmlTable($table, $element)
    {
        return $table->find('tr', $this->config[$element]['row'])
            ->find('td', $this->config[$element]['col'])
            ->innertext;
    }
}
