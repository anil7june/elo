<?php

namespace App\Utility;

use Cake\ORM\TableRegistry;
use App\Utility\Converter;
use App\Utility\ProductPricingData;
use App\Utility\UserPricingData;

class Prices {

  public static function getProductsPrices($products, $user_id) {

    $roles = TableRegistry::get('Roles');

		if($roles->hasGranted($user_id, 'see_gross_value')) {
			return Prices::getGrossValues($products, $user_id);
		} else if($roles->hasGranted($user_id, 'see_catalogue_value')) {
			return Prices::getCatalogueValues($products, $user_id);
		}

	}

  public static function getStockProductsPrices($products, $user_id, $stock_id) {

    $stocks = TableRegistry::get('Stocks');
    $stock = $stocks->get($stock_id);

    $result = [];
		foreach ($products as $product) {
			$catalogue_value = Prices::catalogueValue($product->id, $user_id);
			$result[$product->id] = Converter::convert(Prices::userValue($catalogue_value, $stock->margin, $stock->USD_margin), $user_id);
		}
		return $result;
  }

	private static function getGrossValues($products, $user_id) {

		$result = [];
		foreach ($products as $product) {
			$result[$product->id] = Converter::convert(Prices::grossValue($product->id), $user_id);
		}

		return $result;
	}

	private static function getCatalogueValues($products, $user_id) {

		$result = [];
		foreach ($products as $product) {
			$result[$product->id] = Converter::convert(Prices::catalogueValue($product->id, $user_id), $user_id);
		}
		return $result;
	}

  public static function catalogueValue($product_id, $user_id) {

		$productData = new ProductPricingData();
		$productData->setProductData($product_id);

		$userData = new UserPricingData();
		$userData->setUserData($user_id);

    $users = TableRegistry::get('Users');
    $user = $users->get($user_id);

		$catalogueValue = Prices::calculateCatalogueValue($productData, $userData);

    if($catalogueValue < 0) {
      $catalogueValue = 0;
    }

		return $catalogueValue;
	}

	public static function grossValue($product_id) {

		$productData = new ProductPricingData();
		$productData->setProductData($product_id);

		$grossValue = Prices::calculateGrossValue($productData);
    if($grossValue < 0) {
      $grossValue = 0;
    }

		return $grossValue;

	}


  public static function userValue($catalogueValue, $margin, $USD_margin) {

    $result = $catalogueValue * (1.0 - 0.01 * $margin) - $USD_margin;
    return $result;
  }


  private static function calculateGrossValue($productData) {

    $_gramOunce = 32.1507;

    $elements = $productData->_elements;

    $content = $productData->_content;
    $weight = $productData->_weight;
    $prices = $productData->_prices;

    $result = 0;
    foreach ($elements as $element) {
      $result += ($weight*$content[$element]*$prices[$element]*$_gramOunce)/(10**6);
    }
    return $result;
  }


  private static function calculateCatalogueValue($productData, $userData) {

    $_gramOunce = 32.1507;

    $elements = $userData->_elements;

    $RR = $userData->_RR;
    $PD = $userData->_PD;
    $RC = $userData->_RC;
    $TC = $userData->_TC;
    $DU_pp = $userData->_DU_pp;
    $DU_value = $userData->_DU_value;

    $content = $productData->_content;
    $weight = $productData->_weight;
    $prices = $productData->_prices;
    $PAD = $productData->_discounts;

    $result = 0;
    foreach ($elements as $element) {
      $iteration = (float)( $RR[$element]*$weight*$content[$element]*$_gramOunce*
        ($prices[$element] - $PD[$element] - $PAD[$element]) ) / (10**6)
        - ( $weight*$content[$element]*$RR[$element]*$RC[$element] ) / (10**6);
      $result += $iteration;
    }
    $result -= ( $TC*$weight + ($DU_value*(100.0 - $DU_pp))/100.0 );

    return $result;
  }
}
