<?php

// src/Utility/ProductPricingData.php
namespace App\Utility;

use Cake\ORM\TableRegistry;

class ProductPricingData {

  public $_elements = [
    'Pt', 'Pd', 'Rh'
  ];

  public $_content = [];
  public $_prices = [];
  public $_discounts = [];
  public $_weight = 0;

  public function setProductData($product_id) {

    $products = TableRegistry::get('Products');
    $product = $products->get($product_id);

    $this->getElementPrices();
    $this->getElementsDiscounts();
    $this->getProductWeight($product);
    $this->getProductElementsContent($product);

  }

  public function getProductElementsContent($product) {

    if(!$product->test_id) {
      return 0.0;
    }

    $tests = TableRegistry::get('Tests');

    foreach ($this->_elements as $element) {
      $this->_content[$element] = $tests->getElementContentFromTest($element, $product->test_id);
    }

    return $this->_content;
  }

  public function getProductWeight($product) {

    $productVersion = TableRegistry::get('ProductVersions')->find()
      ->where([
        'product_id' => $product->id,
        'version' => $product->version
      ])
      ->first();

      $this->_weight = (float)$productVersion->weight;

      return $this->_weight;
  }

  public function getElementPrices() {

    $pgmPrices = TableRegistry::get('PgmPrices');

    foreach ($this->_elements as $element) {
      $this->_prices[$element] = (float)$pgmPrices->getElementPrice($element);
    }

    return $this->_prices;
  }


  public function getElementsDiscounts() {

    $pgmPrices = TableRegistry::get('PgmPrices');

    foreach ($this->_elements as $element) {
      $this->_discounts[$element] = (float)$pgmPrices->getElementDiscount($element);
    }

    return $this->_discounts;
  }

}
