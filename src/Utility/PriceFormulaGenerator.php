<?php

namespace App\Utility;

class PriceFormulaGenerator
{
    private $params;
    private $elements = ['pt', 'pd', 'rh'];

    public function __construct(array $pricingParams)
    {
        $this->params = $pricingParams;
    }

    public function generateCatalogueValueFormula()
    {
        $cost_with_no_discount_formula = $this->costWithNoDiscountFormula();
        $percent_discount_coefficient = '((100-' . $this->params['user_discount']['percent'] . ')/100)';
        $value_discount = $this->params['user_discount']['value'];
        return $cost_with_no_discount_formula . '*' . $percent_discount_coefficient . ' - ' . $value_discount;
    }

    private function costWithNoDiscountFormula()
    {
        $basic_value_formula = $this->generateBasicValueFormula();
        $cost_formula = $this->generateCostFormula();
        return '( ( ' . $basic_value_formula . ' ) - ( ' . $cost_formula . ' ) )';
    }

    private function generateCostFormula()
    {
        $result = $this->generateTreatmentChargeCostFormula();
        foreach ($this->elements as $element) {
            $element_full_cost = $this->generateElementFullCostFormula($element);
            $result = $result . '+' . $element_full_cost;
        }
        return $result;
    }

    private function generateElementFullCostFormula($element)
    {
        $refining_charge_formula = $this->generateRefiningChargeCostFormulaForElement($element);
        $metal_discount_formula = $this->generateMetalDiscountCostFormulaForElement($element);
        $leasing_charge_formula = $this->generateLeasingChargeCostFormulaForElement($element);

        $result_formula = '(' . $refining_charge_formula . ') + (' . $metal_discount_formula . ') + (' . $leasing_charge_formula . ')';
        return $result_formula;
    }

    private function generateTreatmentChargeCostFormula()
    {
        return 'product_version.weight * ' . $this->params['treatment_charge'];
    }

    private function generateRefiningChargeCostFormulaForElement($element)
    {
        $element_recovery_in_toz = $this->generateFormulaForElementRecoveryInToz($element);
        $rc_element = $this->params['refining_charge'][$element];
        return $element_recovery_in_toz . '*' . $rc_element;
    }

    private function generateMetalDiscountCostFormulaForElement($element)
    {
        $element_recovery_in_toz = $this->generateFormulaForElementRecoveryInToz($element);
        $element_discount = $this->params['metal_discount'][$element];
        return $element_recovery_in_toz . ' * ' . $element_discount;
    }

    private function generateLeasingChargeCostFormulaForElement($element)
    {
        $element_recovery_in_toz = $this->generateFormulaForElementRecoveryInToz($element);
        $element_price = $this->params['pgm_prices'][$element];
        $element_leasing_days = $this->params['leasing_days'][$element];
        $element_leasing_cost = $this->params['leasing_charge'][$element];
        return $element_recovery_in_toz . '*' . $element_price . '*(' . $element_leasing_cost . '/100)*(' . $element_leasing_days . '/360)';
    }

    private function generateBasicValueFormula()
    {
        $formula = '0';
        foreach ($this->elements as $element) {
            $element_basic_value = $this->generateFormulaForElementBasicValue($element);
            $formula = $formula . ' + ' . $element_basic_value;
        }
        return $formula;
    }

    private function generateFormulaForElementBasicValue($element)
    {
        $element_recovery_in_toz = $this->generateFormulaForElementRecoveryInToz($element);
        $element_price = $this->params['pgm_prices'][$element];
        return '(' . $element_recovery_in_toz . '*' . $element_price . ')';
    }

    private function generateFormulaForElementRecoveryInToz($element)
    {
        $test_result_in_toz = $this->convertTestResultToToz($element);
        $element_recovery_rate = $this->params['recovery_rate'][$element];
        return $test_result_in_toz . '*' . $element_recovery_rate;
    }

    private function convertTestResultToToz($element)
    {
        $formula = $this->testPpmsValueExpression($element) . '/' . '1000' . '*' . 'product_version.weight' . '/' . $this->params['gram_ounce'];
        return '(' . $formula . ')';
    }

    public function testPpmsValueExpression($element)
    {
        if ($element === 'rh') {
            $boundary = '50';
        } else {
            $boundary = '150';
        }
        return 'IF(test.' . $element . ' > ' . $boundary . ', test.' . $element . ', 0)';
    }
}
