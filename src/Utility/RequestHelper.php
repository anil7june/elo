<?php

namespace App\Utility;

class RequestHelper{

  private $request;
  private $isPost;
  
  function __construct($request){
    $this->request = $request;

    $this->isPost = $this->request->is(['post']);
  }

  public function getRequest(){
    return $this->request;
  }

  public function getParam($name){
    $result;

    if ($this->isPost){
      $result = $this->request->getData($name);
    } else{
      $result = $this->request->getQuery($name);
    }

    return $result;
  }

  public function getAllParams() {
    $result;

    if($this->isPost){
      $result =  $this->request->getData();
    } else {
      $result = $this->request->getQueryParams();
    }

    return $result;
  }

  public function is($arr) {
    return $this->request->is($arr);
  }

}

?>
