<?php

// src/Utility/StockStats.php
namespace App\Utility;

use App\Utility\Converter;
use Cake\ORM\TableRegistry;

class StockStats
{

    public function __construct($stock_id = 0, $active_user_id = 0, $stock_products = null)
    {

        if ($stock_id) {
            $this->setStats($stock_id, $active_user_id);

            foreach ($stock_products as $stockProduct) {

                $catalogueValue = $stockProduct->catalogue_value;
                $userValue = $stockProduct->user_value;

                $this->catalogueValues[$stockProduct->id] = Converter::format(Converter::convert($catalogueValue, $active_user_id), $active_user_id);
                $this->userValues[$stockProduct->id] = Converter::format(Converter::convert($userValue, $active_user_id), $active_user_id);
                $this->profits[$stockProduct->id] = Converter::format(Converter::convert($catalogueValue - $userValue, $active_user_id), $active_user_id);
            }
        }
    }

    private $catalogueValues = [];
    private $userValues = [];
    private $profits = [];
    private $value_to_hedge = [
        'pt' => 0.0,
        'pd' => 0.0,
        'rh' => 0.0,
    ];

    private $stats = [
        'totalCount' => 0.0,
        'totalUserValue' => 0.0,
        'totalCatalogueValue' => 0.0,
        'totalProfit' => 0.0,
        'totalProfitPerPc' => 0.0,
        'totalProfitPerKg' => 0.0,
        'catalogValuePerKg' => 0.0,
        'avgValuePerPc' => 0.0,
        'avgCatValuePerPc' => 0.0,
        'avgMarginValuePerPc' => 0.0,
    ];

    public function getCatalogueValues()
    {
        return $this->catalogueValues;
    }

    public function getUserValues()
    {
        return $this->userValues;
    }

    public function getProfits()
    {
        return $this->profits;
    }

    public function getStats()
    {
        return $this->stats;
    }

    public function getTotalCount()
    {
        return $this->stats['totalCount'];
    }

    public function getTotalUserValue()
    {
        return $this->stats['totalUserValue'];
    }

    public function getTotalCatalogueValue()
    {
        return $this->stats['totalCatalogueValue'];
    }

    public function getTotalProfit()
    {
        return $this->stats['totalProfit'];
    }

    public function getTotalProfitPerPc()
    {
        return $this->stats['totalProfitPerPc'];
    }

    public function getTotalProfitPerKg()
    {
        return $this->stats['totalProfitPerKg'];
    }

    public function getCatalogValuePerKg()
    {
        return $this->stats['catalogValuePerKg'];
    }

    public function getAvgValuePerPc()
    {
        return $this->stats['avgValuePerPc'];
    }

    public function getAvgCatValuePerPc()
    {
        return $this->stats['avgCatValuePerPc'];
    }

    public function getAvgMarginValuePerPc()
    {
        return $this->stats['avgMarginValuePerPc'];
    }

    public function getValueToHedge()
    {
        return $this->value_to_hedge;
    }

    public function setStats($stock_id, $user_id)
    {

        $stockProductsTable = TableRegistry::get('StockProducts');

        $products = $stockProductsTable->find('all')
            ->select([
                'no_of_pcs',
                'catalogue_value',
                'user_value',
                'ProductVersions.weight',
            ])
            ->contain([
                'ProductVersions',
            ])
            ->where([
                'stock_id' => $stock_id,
            ]);

        $this->stats['totalCount'] = $this->calculateTotalCount($products);
        $total_count = (float) $this->stats['totalCount'];
        if ($this->stats['totalCount'] > 0) {

            $total_catalogue_value = Converter::convert($this->calculateTotalCatalogueValue($products), $user_id);
            $total_user_value = Converter::convert($this->calculateTotalUserValue($products), $user_id);
            $total_profit = $total_catalogue_value - $total_user_value;
            $total_weight = $this->getTotalWeight($products);

            $this->stats['totalUserValue'] = Converter::format($total_user_value, $user_id);
            $this->stats['totalCatalogueValue'] = Converter::format($total_catalogue_value, $user_id);
            $this->stats['totalProfit'] = Converter::format($total_profit, $user_id);
            $this->stats['totalProfitPerPc'] = Converter::format($total_profit / $total_count, $user_id);
            if ($total_weight > 0) {
                $this->stats['totalProfitPerKg'] = Converter::format($total_profit / $this->getTotalWeight($products), $user_id);
                $this->stats['catalogValuePerKg'] = Converter::format($total_catalogue_value / $this->getTotalWeight($products), $user_id);
            }
            $this->stats['avgValuePerPc'] = Converter::format($total_user_value / $total_count, $user_id);
            $this->stats['avgCatValuePerPc'] = Converter::format($total_catalogue_value / $total_count, $user_id);
            $this->stats['avgMarginValuePerPc'] = Converter::format($total_profit / $total_count, $user_id);
        }

        $this->calculateValueToHedge($stock_id);

    }

    private function calculateValueToHedge($stock_id)
    {

        $stock_products = TableRegistry::get('StockProducts');
        $products = $stock_products->find()
            ->select([
                'test.pt',
                'test.pd',
                'test.rh',
                'StockProducts.no_of_pcs',
                'content_amount.value',
            ])
            ->join([
                'content_amount' => [
                    'table' => 'content_amounts',
                    'type' => 'LEFT',
                    'conditions' => [
                        'StockProducts.content_amount_id = content_amount.id',
                    ],
                ],
                'product_version' => [
                    'table' => 'product_versions',
                    'type' => 'LEFT',
                    'conditions' => [
                        'StockProducts.product_version_id = product_version.id',
                    ],
                ],
                'product' => [
                    'table' => 'products',
                    'type' => 'LEFT',
                    'conditions' => [
                        'product.version = product_version.version',
                        'product.id = product_version.product_id',
                    ],
                ],
                'test' => [
                    'table' => 'tests',
                    'type' => 'LEFT',
                    'conditions' => [
                        'test.id = product.test_id',
                    ],
                ],
            ])
            ->where([
                'stock_id' => $stock_id,
            ]);

        foreach ($products as $product) {
            foreach (['pt', 'pd', 'rh'] as $elem) {
                $this->value_to_hedge[$elem] += $product->no_of_pcs * $product->test[$elem] * $product->content_amount['value'] * 0.8;
            }

        }
    }

    private function calculateTotalCount($productList)
    {

        $result = 0;
        foreach ($productList as $product) {
            $result += $product->no_of_pcs;
        }
        return $result;
    }

    private function calculateTotalUserValue($productList)
    {

        $result = 0;
        foreach ($productList as $product) {
            $result += $product->user_value * $product->no_of_pcs;
        }
        return $result;
    }

    private function calculateTotalCatalogueValue($productList)
    {

        $result = 0;
        foreach ($productList as $product) {
            $result += $product->catalogue_value * $product->no_of_pcs;
        }
        return $result;
    }

    private function getTotalWeight($productList)
    {

        $totalWeight = 0;
        foreach ($productList as $product) {
            $totalWeight += $product->product_version->weight * $product->no_of_pcs;
        }
        return $totalWeight;
    }

}