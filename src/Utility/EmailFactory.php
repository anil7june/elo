<?php

namespace App\Utility;

use Cake\Mailer\Email;

class EmailFactory
{
    private static function createDefaultEmailWithSubject($subject = '')
    {
        $email = new Email();
        $email->transport('default');
        $email->emailFormat('html');
        $email->subject($subject);
        return $email;
    }

    private static function createTemplateEmail($template_name, array $template_variables, $subject = '')
    {
        $email = self::createDefaultEmailWithSubject($subject);
        $email->template($template_name);
        $email->viewVars($template_variables);
        $email->from('no-reply@ecrportal.biz');

        return $email;
    }

    public function createUserRegistrationRequestAdminNotificationEmail($user)
    {
        return self::createTemplateEmail('requested_logged', ['user' => $user], __('New User Request - Elemental App'));
    }

    public function createUserAccountActivationNotificationEmail($user)
    {
        return self::createTemplateEmail('account_activated', ['user' => $user], __('Account Activated - Elemental App'));
    }

    public function createRecoveryEmail($user, $recovery_url)
    {
        return self::createTemplateEmail(
            'recovery_email',
            [
                'user' => $user,
                'recovery_url' => $recovery_url,
            ],
            __('Reset Your Password'));
    }
}
