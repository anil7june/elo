<?php

namespace App\Utility;

use App\Utility\FileHandler;
use Cake\ORM\TableRegistry;

class CSVParser
{

    private static $map = [
        'ID' => 'ID',
        'Make' => 'Makes.name',
        'Main Reference Number' => 'ProductVersions.cat_code',
        'Additional Reference Data' => 'ProductVersions.additional_ref_data',
        'Description' => 'ProductVersions.description',
        'Weight' => 'ProductVersions.weight',
        'Pt' => 'Tests.pt',
        'Pd' => 'Tests.pd',
        'Rh' => 'Tests.rh',
        'Catalyst Manufacturer' => 'CatMakes.name',
        'Analysis Type' => 'AnalysisTypes.name',
        'Picture Name' => 'ProductVersions.image',
    ];

    public static function getColumnsNameTable()
    {
        return array_keys(self::$map);
    }

    public static function getColumnsNameTableForImport()
    {
        $result = array_keys(self::$map);
        unset($result[0]);
        return $result;
    }

    public function parseCSV($tmp_file_name)
    {

        $handle = fopen(FileHandler::getImagePath($tmp_file_name, 'csv'), "r");
        $columns = self::getColumns($handle);
        if (count($columns) != count(self::$map)) {
            fclose($handle);
            return __('The number of columns in your CSV file is incorrect.');
        }

        $match_error = false;
        $required_cols_names = self::getColumnsNameTable($map);
        foreach ($columns as $name => $key) {
            if ($columns[$key] !== $required_cols_names[$key]) {
                $match_error = true;
            }
        }

        if ($match_error) {
            fclose($handle);
            return __('Some of the names or order of columns in your CSV file are incorrect.');
        }

        $index = 1;
        $ids_to_codes = [];
        while (($row = \fgetcsv($handle)) != null) {
            $data = self::getEntityDataFromRow($row, $columns);
            $error = self::parseData($data, $ids_to_codes);
            if ($error) {
                $error = __('Your file is not correct. Error found: row') . " " . $index . '. : ' . $error;
                fclose($handle);
                return $error;
            }
            $index++;
        }
        fclose($handle);
    }

    public static function importProducts($tmp_file_name)
    {
        $count = 0;
        $handle = fopen(FileHandler::getImagePath($tmp_file_name, 'csv'), "r");
        $columns = self::getColumns($handle);
        $ids = []; //aray that maps fakeID to real product id in db

        while (($row = \fgetcsv($handle)) != null) {
            $data = self::getEntityDataFromRow($row, $columns);
            $id_exists = array_key_exists($data['ID'], $ids);
            if (!$id_exists) {
                $count++;
                $new_product_id = self::saveData($data);
                if ($new_product_id === -1) {
                    fclose($handle);
                    return __('Error: import error, please contact your tech support.');
                } else {
                    $ids[$data['ID']] = $new_product_id;
                }
            } else {
                $product_id = $ids[$data['ID']];
                if (!self::addTest($data, $product_id)) {
                    fclose($handle);
                    return __('Error: import error, please contact your tech support.');
                }
            }
        }

        fclose($handle);
        return "Success";
    }

    private static function getColumns($handle)
    {

        $headers = fgetcsv($handle);
        $columns = [];

        $index = 0;
        foreach ($headers as $header) {
            $columns[$index] = self::$map[$header];
            $index++;
        }

        return $columns;
    }

    private static function getEntityDataFromRow($row, $columns)
    {

        $result = [];
        $index = 0;
        while ($index < count($columns)) {
            $result[$columns[$index]] = trim($row[$index]);
            $index++;
        }

        return $result;
    }

    private static function parseData($data, &$ids_to_codes)
    {

        if (array_key_exists($data['ID'], $ids_to_codes) && $data['ProductVersions.cat_code'] != $ids_to_codes[$data['ID']]) {
            return __('Product code do not match with the first occurence of the product with given ID.');
        } else if (!array_key_exists($data['ID'], $ids_to_codes)) {
            $ids_to_codes[$data['ID']] = $data['ProductVersions.cat_code'];
        }

        if (!preg_match('/^\d+$/', $data['ID'])) {
            return __('Invalid ID input.');
        }

        $makes = TableRegistry::get('Makes');
        $make = $makes->findByName($data['Makes.name']);
        if ($data['Makes.name'] !== '' && $make->count() === 0) {
            return __('Invalid Make value.');
        }

        $cat_makes = TableRegistry::get('CatMakes');
        $cat_make = $cat_makes->findByName($data['CatMakes.name']);
        if ($data['CatMakes.name'] !== '' && $cat_make->count() === 0) {
            return __('Invalid Catalyst Manufacturer value.');
        }

        $analysis_types = TableRegistry::get('AnalysisTypes');
        $all_types = $analysis_types->find('all')->toArray();
        $analysis_type = $analysis_types->findByName($data['AnalysisTypes.name']);
        if ($data['AnalysisTypes.name'] !== '' && $analysis_type->count() == 0) {
            return __('Invalid Analysis Type value.');
        }

        if (!self::elementContentIsValid($data['Tests.pt']) || !self::elementContentIsValid($data['Tests.pd']) || !self::elementContentIsValid($data['Tests.rh'])) {
            return __('Invalid Pt, Pd or Rh value.');
        }

        if (!is_numeric($data['ProductVersions.weight'])) {
            return __('Invalid Weight value.');
        }

        return null;
    }

    private static function elementContentIsValid($value)
    {

        return $value === "" || is_numeric($value);
    }

    private static function saveData($data)
    {

        $products = TableRegistry::get('Products');
        $product_versions = TableRegistry::get('ProductVersions');
        $tests = TableRegistry::get('Tests');

        $product = $products->newEntity();
        $product_version = $product_versions->newEntity();

        if ($data['Makes.name'] === '') {
            $product_version->make_id = null;
        } else {
            $makes = TableRegistry::get('Makes');
            $make = $makes->findByName($data['Makes.name']);
            $product_version->make_id = $make->first()->id;
        }

        if ($data['CatMakes.name'] === '') {
            $product_version->cat_make_id = null;
        } else {
            $cat_makes = TableRegistry::get('CatMakes');
            $cat_make = $cat_makes->findByName($data['CatMakes.name']);
            $product_version->cat_make_id = $cat_make->first()->id;
        }

        $product_version->weight = $data['ProductVersions.weight'];
        $product_version->additional_ref_data = $data['ProductVersions.additional_ref_data'];
        $product_version->description = $data['ProductVersions.description'];
        $product_version->cat_code = $data['ProductVersions.cat_code'];
        $product_version->version = 1;

        $test = self::createTestEntity($data);
        $product->version = 1;
        $product->deleted = false;

        $product_statuses = TableRegistry::get('ProductStatuses');
        $open_status_id = $product_statuses->findByName('Active')->first()->id;
        $product->product_status_id = $open_status_id;

        if ($result = $products->save($product) == false) {
            return -1;
        }

        $product_version->product_id = $product->id;
        $test->product_id = $product->id;

        if ($product_versions->save($product_version) == false) {
            return -1;
        }

        if ($tests->save($test) == false) {
            return -1;
        }

        $product->test_id = $test->id;
        if ($products->save($product) == false) {
            return -1;
        }

        return $product->id;

    }

    private static function addTest($data, $product_id)
    {
        $tests = TableRegistry::get('Tests');
        $test = self::createTestEntity($data);
        $test->product_id = $product_id;
        return $tests->save($test);
    }

    private function createTestEntity($data)
    {
        $tests = TableRegistry::get('Tests');
        $test = $tests->newEntity();

        if ($data['AnalysisTypes.name'] === '') {
            $test->analysis_type_id = null;
        } else {
            $analysis_types = TableRegistry::get('AnalysisTypes');
            $analysis_type = $analysis_types->findByName($data['AnalysisTypes.name']);
            $test->analysis_type_id = $analysis_type->first()->id;
        }

        $test->pt = $data['Tests.pt'];
        $test->pd = $data['Tests.pd'];
        $test->rh = $data['Tests.rh'];

        return $test;
    }
}
