<?php

namespace App\Utility;

class RandomTokenGenerator
{
    private static $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    private static $length = 64;

    public static function generateToken()
    {
        $pieces = [];
        $max = mb_strlen(self::$keyspace, '8bit') - 1;
        for ($i = 0; $i < self::$length; ++$i) {
            $pieces[] = self::$keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }
}
