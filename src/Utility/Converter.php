<?php

namespace App\Utility;

use Cake\I18n\Number;
use Cake\ORM\TableRegistry;

class Converter
{

    public static function convert($value, $user_id)
    {

        $currencies = TableRegistry::get('Currencies');
        $users = TableRegistry::get('Users');

        $user = $users->get($user_id);
        $currency = $currencies->get($user->currency_id);

        $exchangeRate = $currencies->getExchangeRateForUser($user_id);

        $convertedValue = $value * $exchangeRate;

        return $convertedValue;

    }

    public static function convertToBase($exchange_rate, $value)
    {
        return round($value / $exchange_rate, 2);
    }

    public static function convertToBaseFromUserCurrency($user_id, $value)
    {
        $exchange_rate = TableRegistry::get('Currencies')->getExchangeRateForUser($user_id);
        return self::convertToBase($exchange_rate, $value);
    }

    public static function format($value, $user_id)
    {

        $users = TableRegistry::get('Users');
        $user = $users->get($user_id);

        $languages = TableRegistry::get('Languages');
        $language = $languages->get($user->language_id);

        $nice_value = Number::format($value, [
            'places' => 2,
            'precision' => 2,
            'locate' => $language->locale,
        ]);

        return $nice_value;
    }

    public static function getCurrencyCode($user_id)
    {

        $users = TableRegistry::get('Users');
        $currencies = TableRegistry::get('Currencies');

        $user = $users->get($user_id);
        $currency = $currencies->get($user->currency_id);
        $currency_code = $currency->code;

        return $currency_code;
    }

    public static function number_format($value, $digits)
    {
        return (float) \number_format($value, $digits, '.', '');
    }

}
