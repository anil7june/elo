<?php

namespace App\Utility;

use Cake\ORM\TableRegistry;

class ProductSearch
{

    public static function stockListSearch($stock_ctrl, $stock_id)
    {
        $user_id = $stock_ctrl->Auth->user('id');

        ProductSearch::setAdvancedSearchView($stock_ctrl);
        ProductSearch::setFilterValues($stock_ctrl);

        $stocks_table = TableRegistry::get('Stocks');
        $stock = $stocks_table->findById($stock_id)->first();

        $products_table = TableRegistry::get('Products');
        $products = $products_table->emptySet();

        $content_amounts = TableRegistry::get('ContentAmounts');

        $content_amounts_list = $content_amounts->find('list');
        $default_content_amount_id = $content_amounts->findByName('100%')->first()->id;

        $currency_code = Converter::getCurrencyCode($user_id);

        $stock_ctrl->set(compact('currency_code', 'content_amounts_list', 'default_content_amount_id', 'user_id'));

        $type_of_price = 'user_value';

        if ($stock_ctrl->request->is(['get'])) {
            $search_performed = false;
            $data = $stock_ctrl->request->getQueryParams();

            if ($data && self::emptySearchInput($data) && self::emptyAdvancedSearchForm($data)) {
                $stock_ctrl->Flash->error(__('Please insert some search data'));
                $products = $products_table->emptySet();

            } else if ($data) {
                if ($data['search_button']) {
                    $search_performed = true;
                    $products = self::performSearchAction($stock_ctrl, $data['dataInput'], $user_id, $stock_id);
                } else if ($data['advanced_search_button']) {
                    $search_performed = true;
                    $products = self::performAdvancedSearchAction($stock_ctrl, $data, $type_of_price, $user_id, $stock_id);
                }
            }

            if ($search_performed && $products->count() === 0) {
                return $stock_ctrl->redirect(['prefix' => 'catalog', 'controller' => 'products', 'action' => 'itemNotFound', $stock_id]);
            }
        }

        $stock_ctrl->paginate($products);
        $stock_ctrl->set(compact('products', 'search_performed'));
    }

    public static function search($ctrl)
    {

        $user_id = $ctrl->Auth->User('id');

        self::setAdvancedSearchView($ctrl);
        self::setFilterByProductStatus($user_id, $ctrl);
        self::setFilterValues($ctrl);

        $roles = TableRegistry::get('Roles');

        $type_of_price = 'catalogue_value';
        $products = $ctrl->Products->emptySet();

        $currency_code = Converter::getCurrencyCode($user_id);
        $ctrl->set(compact('currency_code', 'type_of_price', 'user_id'));

        if ($ctrl->request->is('get')) {
            $data = $ctrl->request->getQueryParams();

            if ($data && self::emptyAdvancedSearchForm($data) && self::emptySearchInput($data)) {
                $ctrl->Flash->error(__('Please insert some search data'));
                $products = $ctrl->Products->emptySet();

            } else if ($data) {
                $searchAllowed = self::isSameSearch($ctrl) || self::coinsControl($ctrl, $user_id);
                if ($searchAllowed) {
                    if ($data['search_button']) {
                        $products = self::performSearchAction($ctrl, $data['dataInput'], $user_id);
                    } else if ($data['advanced_search_button']) {
                        $products = self::performAdvancedSearchAction($ctrl, $data, $type_of_price, $user_id);
                    }
                    $search_performed = true;
                    $ctrl->set(compact('search_performed'));

                } else {
                    $ctrl->Flash->error(__('Sorry, you cannot afford to perform search action. You have no coins left. Please contact your admin.'));
                    return $ctrl->redirect(['prefix' => 'catalog', 'controller' => 'products', 'action' => 'index']);
                }
            }
        }
        $ctrl->set(compact('products'));
        $ctrl->paginate($products);
    }

    public static function setProductViewData($product_id, $user_id, $ctrl)
    {
        if (TableRegistry::get('Roles')->hasGranted($user_id, 'see_gross_value')) {
            $ctrl->set(['type_of_price' => 'gross_value']);
        } else {
            $ctrl->set(['type_of_price' => 'catalogue_value']);
        }
        $product = $ctrl->Products->getProductFullDataQuery($product_id, $user_id)->first();
        $value_per_kg = $product->catalogue_value / $product->product_version['weight'];
        $prev_location_url = $ctrl->referer();
        $ctrl->set(compact('prev_location_url', 'user_id', 'product', 'value_per_kg'));
    }

    private function emptySearchInput($data)
    {
        return strlen(trim($data['dataInput'])) === 0;
    }

    private function emptyAdvancedSearchForm($data)
    {

        return empty(self::getFormConditions($data)) && strlen(trim($data['max_price'])) === 0 && strlen(trim($data['min_price'])) === 0;
    }

    private static function isSameSearch($ctrl)
    {
        $oldParams = [];
        $temp = parse_url($ctrl->referer());
        parse_str($temp['query'], $oldParams);

        $newParams = $ctrl->request->getQueryParams();

        unset($oldParams['page']);
        unset($oldParams['limit']);
        unset($newParams['page']);
        unset($newParams['limit']);

        if (empty($oldParams) || empty($newParams) || count($newParams) != count($oldParams)) {
            return false;
        }

        foreach ($newParams as $key => $value) {
            if (!array_key_exists($key, $oldParams) || $oldParams[$key] !== $value) {
                return false;
            }
        }

        return true;
    }

    public function isPageRefreshed()
    {
        $httpCacheControl = $_SERVER['HTTP_CACHE_CONTROL'];
        return (isset($httpCacheControl) && (($httpCacheControl === 'max-age=0') || ($httpCacheControl == 'no-cache')));
    }

    public static function get_search_keys($input)
    {
        $search_keys = preg_split('/\s+/', $input);
        foreach ($search_keys as $key => $word) {
            $search_keys[$key] = self::sqlEscapeString($word);
        }
        return $search_keys;
    }

    public static function performSearchAction($ctrl, $input, $user_id, $stock_id = 0)
    {
        $products_table = TableRegistry::get('Products');

        $search_parameters = [
            'product_version.cat_code',
            'make.name',
        ];

        $ctrl->set(compact('input'));

        $search_keys = self::get_search_keys($input);
        $query = $products_table->findByKeyWords($search_keys);
        if ($stock_id == 0) {
            $query = $products_table->addProductPricesToQuery($query, $user_id);
        } else {
            $query = $products_table->addUserPricesToQuery($query, $stock_id, $user_id);
        }

        return $query;
    }

    public static function performAdvancedSearchAction($ctrl, $data, $type_of_price, $user_id, $stock_id = 0)
    {

        $products_table = TableRegistry::get('Products');
        $conditions = self::getFormConditions($data);

        $query = $products_table->findByConditions($conditions);
        if ($type_of_price === 'user_value') {
            $query = $products_table->addUserPricesToQuery($query, $stock_id, $user_id);
        } else {
            $query = $products_table->addProductPricesToQuery($query, $user_id);
        }

        if ($data['min_price'] !== '') {
            $min_price = (float) $data['min_price'];
            $query = $query->having([
                $type_of_price . ' >=' => $min_price,
            ]);
        }

        if ($data['max_price'] !== '') {
            $max_price = (float) $data['max_price'];
            $query = $query->having([
                $type_of_price . ' <=' => $max_price,
            ]);
        }

        return $query;
    }

    private static function sqlEscapeString($string)
    {
        $escaped_backslashes = str_replace('\\', '\\\\', $string);
        $result = str_replace("%", '\%', $escaped_backslashes);

        return $result;
    }

    private static function getFormConditions($data)
    {

        $conditions = [];

        if ($data['make_id']) {
            $conditions['product_version.make_id'] = $data['make_id'];
        }
        if ($data['cat_make_id']) {
            $conditions['product_version.cat_make_id'] = $data['cat_make_id'];
        }
        if ($data['cat_code']) {
            $conditions['product_version.cat_code LIKE'] = '%' . self::sqlEscapeString($data['cat_code']) . '%';
        }
        if ($data['ecr_code']) {
            $conditions['product_version.ecr_code LIKE '] = '%' . self::sqlEscapeString($data['ecr_code']) . '%';
        }
        if ($data['min_weight']) {
            $conditions['product_version.weight >= '] = $data['min_weight'];
        }
        if ($data['max_weight']) {
            $conditions['product_version.weight <= '] = $data['max_weight'];
        }
        if ($data['product_status_id']) {
            $conditions['Products.product_status_id'] = $data['product_status_id'];
        }
//         if ($data['region_code']) {
//             $conditions['Products.region'] = $data['product_status_id'];
//         } Вот тут нужно дабивить поиск по этому полю.

        echo($conditions);

        return $conditions;
    }

    public static function getProductConditionsForActiveUser($user_id)
    {

        $product_statuses = TableRegistry::get('ProductStatuses');
        $can_see_all_products = false;

        $result = [];
        $result['Products.deleted'] = false;
        if (!$can_see_all_products) {
            $active_status = $product_statuses->findByName('Active')->first();
            $result['Products.product_status_id'] = $active_status->id;
        }
        return $result;
    }

    private static function setFilterValues($ctrl)
    {
        $filterValues = $ctrl->request->getQueryParams();

        $ctrl->set(compact('filterValues'));
    }

    private static function setAdvancedSearchView($ctrl)
    {

        $makes = TableRegistry::get('Makes');
        $makes_list = $makes->find('list');

        $cat_makes = TableRegistry::get('CatMakes');
        $cat_makes_list = $cat_makes->find('list');

        $countries = TableRegistry::get('Countries');
        $countries_list = $countries->find('list');

        $ctrl->set(compact('makes_list', 'cat_makes_list', 'countries_list'));
    }

    private static function setFilterByProductStatus($user_id, $ctrl)
    {
        if (TableRegistry::get('Roles')->hasGranted($user_id, 'see_product_status')) {
            $ctrl->set([
                'show_product_status' => true,
                'product_statuses_list' => TableRegistry::get('ProductStatuses')->find('list'),
            ]);
        }
    }

    private static function getMakesNameArray()
    {

        $makesTable = TableRegistry::get('Makes');
        $makes = $makesTable->find();

        $result = [];
        foreach ($makes as $make) {
            $result[] = $make->name;
        }
        return $result;
    }

    private static function getTypesNameArray()
    {

        $typesTable = TableRegistry::get('Types');
        $types = $typesTable->find();

        foreach ($types as $type) {
            $result[] = $type->name;
        }
        return $result;
    }

    private static function getECRCodesArray($user_id)
    {

        $products_table = TableRegistry::get('Products');

        $products = $products_table->find()
            ->select(['data.ecr_code'])
            ->join([
                'data' => [
                    'table' => 'product_versions',
                    'type' => 'LEFT',
                    'conditions' => [
                        'data.product_id = Products.id',
                        'data.version = Products.version',
                    ],
                ],
            ])
            ->where(ProductSearch::getProductConditionsForActiveUser($user_id));

        $result = [];
        foreach ($products as $product) {
            if ($product->data['ecr_code']) {
                $result[] = $product->data['ecr_code'];
            }
        }
        return $result;
    }

    private static function coinsControl($ctrl, $user_id)
    {

        $users = TableRegistry::get('Users');

        $roleName = TableRegistry::get('Users')->find()->contain('Roles')
            ->where(['Users.id' => $user_id])->first()->name;

        if ($roleName === 'Admin') {
            return true;
        }

        $user = $users->get($user_id);
        if ($user->coins == 0) {
            return false;
        } else {
            $user->coins -= 1;
            $ctrl->set('coins', $user->coins);
            $users->save($user);
            return true;
        }
    }

}
