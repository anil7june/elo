<?php

namespace App\Utility;

class FileHandler
{

    public static function saveFile($file, $folder_name)
    {
        if ($file['name']) {
            $file_name = time() . '_' . $file['name'];
            $file_path = WWW_ROOT . 'uploads' . DS . $folder_name . DS . $file_name;
            if (move_uploaded_file($file['tmp_name'], $file_path)) {
                return $file_name;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public static function saveImage($image_data)
    {
        return self::saveFile($image_data, 'cat_images');
    }

    public static function removeImage($file_name)
    {
        return self::removeFile($file_name, 'cat_images');
    }

    public static function removeFile($file_name, $folder_name)
    {

        $file_path = WWW_ROOT . 'uploads' . DS . $folder_name . DS . $file_name;
        return unlink($file_path);
    }

    public static function getImagePath($file_name, $folder_name)
    {
        return WWW_ROOT . 'uploads' . DS . $folder_name . DS . $file_name;
    }

    public static function getProductImagePathPrefix()
    {
        return WWW_ROOT . 'uploads' . DS . 'cat_images';
    }

    public static function getComapnyLogoPathPrefix()
    {
        WWW_ROOT . 'uploads' . DS . 'company_logo';
    }
}
