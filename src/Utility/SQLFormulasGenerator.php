<?php

namespace App\Utility;

class SQLFormulasGenerator
{
    public static function everyWordLikeAtLeastOneParameterExpression($key_words, $parameters)
    {
        $param_expresions = [];
        foreach ($key_words as $key_word) {
            $or_expresion = [];
            foreach ($parameters as $param) {
                $or_expresion[] = [$param . ' LIKE' => '%' . $key_word . '%'];
            }
            $param_expresions[] = ['OR' => $or_expresion];
        }
        $conditions = ["AND" => $param_expresions];

        return $conditions;
    }

    public static function splitInputIntoEscapedSqlWords($input)
    {
        $words = preg_split('/\s+/', $input);
        foreach ($words as $word) {
            $words[] = self::SqlEscapeString($word);
        }
        return $words;
    }

    private function SqlEscapeString($str)
    {
        $escaped_backslashes = str_replace('\\', '\\\\', $str);
        $result = str_replace("%", '\%', $escaped_backslashes);

        return $result;
    }
}
