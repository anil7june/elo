<?php

namespace App\Utility;

use Cake\I18n\Number;
use Cake\ORM\TableRegistry;

class Formater
{
    public static function formatNumber($value, $user_id)
    {

        $users = TableRegistry::get('Users');
        $user = $users->get($user_id);

        $languages = TableRegistry::get('Languages');
        $language = $languages->get($user->language_id);

        $nice_value = Number::format($value, [
            'places' => 2,
            'precision' => 2,
            'locate' => $language->locale,
        ]);

        return '' . $nice_value;
    }

    public static function imagePath($image_name = null)
    {
        $path = '/uploads/cat_images/' . $image_name;
        return $path;
    }

    public static function truncate($float_number)
    {
        if ($float_number < 0) {
            return ceil((string) ($float_number * 100)) / 100;
        }
        return floor((string) ($float_number * 100)) / 100;
    }
}
