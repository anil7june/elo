
    <div class="search-input-margin"></div>
    <?=$this->element('search_input', [$search_data])?>
    <div class="margin-15"></div>

    <?php if ($search_performed && $products->count() > 0): ?>
    <?=$this->element('search_results', [$products, $currency_code, $content, $prices, $product_statuses_list, $product_status_search])?>
    <?php endif;?>

    <?php if ($search_performed && $products->count() == 0): ?>
    <?=$this->element('search_results_not_found', ['message' => __('It seems, that what you are looking for does not exist in our database.')])?>
    <?php endif;?>
