<!-- File: src/Template/Catalog/Products/preproduct_created.ctp -->

<?=$this->element('header_with_back', [
    'back_url' => $prev_location,
    'header_text' => '',
    'back_link_text' => __('Back to stock'),
])?>
<?=$this->element('message_done', [
    'bold_message_text' => __('Custom product "') . $preproduct_code . '"',
    'normal_message_text' => __('has been added to your stock list')])?>
