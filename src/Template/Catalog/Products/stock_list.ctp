    <?=$this->element('header_with_back', [
    'back_url' => $this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'index']),
    'header_text' => __('ADDING PRODUCTS TO ') . strtoupper($stock->name),
    'back_link_text' => __('Back to ') . $stock->name])?>

    <?=$this->element('search_input', [$search_data])?>
    <div class="margin-15"></div>
    <?php if ($search_performed && $products->count() !== 0): ?>

    <div class="paginator-style">
        <div class="left">
            <?=h($this->Paginator->params()['start'] . ' - ' . $this->Paginator->params()['end'] . __(' from ') . $this->Paginator->params()['count'])?>
        </div>
        <div class="right">
            <?=$this->element('paginator_menu')?>
        </div>
    </div>
    <div class="table-responsive-lg">
    <table class="table product-table shadow-header-table">
        <thead>
            <tr>
                <th></th>
                <th>
                    <?=h(__('Code'))?>
                </th>
                <th>
                    <?=h(__('Manufacturer'))?>
                </th>
                <th>
                    <?=h(__('Weight'))?>
                </th>
                <th colspan="2" class="price-header">
                    <?=h(__('Price'))?>
                </th>
                <th>
                    <?=h(__('Content'))?>
                </th>
                <th>
                    <?=h(__('Count'))?>
                </th>
                <th></th>
            </tr>
        </thead>


        <?php foreach ($products as $product): ?>

        <tr id="<?=h($product->id . '_row')?>">
            <td><img class="product-image" src="<?=h($this->ProductFormat->imagePath($product->product_version['image']))?>" onclick="imgPopup('<?=h($this->ProductFormat->imagePath($product->product_version['image']))?>')"/></td>
            <td><?=h($product->product_version['cat_code'])?></td>
            <td><?=h($product->make['name'])?></td>
            <td><?=h($product->product_version['weight'])?></td>
            <td class="price-value"><?=h($this->ProductFormat->formatNumber($product->user_value, $user_id))?>
            </td>
            <td class="price-currency"><?=h($currency_code)?></td>
            <td>
                <?=$this->Form->control('content_amount_id_' . $product->id, ['label' => '', 'type' => 'select', 'options' => $content_amounts_list, 'value' => $default_content_amount_id])?>
            </td>
            <td id="count-<?=h($product->id)?>-col">
                <?=$this->Form->control('no_of_pcs_' . $product->id, ['label' => '', 'type' => 'number', 'value' => "1", 'min' => '1'])?>
            </td>
            <td>
                <input id="<?='button_' . h($product->id)?>" type="submit" class="btn submit-btn" value=<?=__("Add")?>
                    onclick="sendPostData(event,
              '<?='count-' . h($product->id)?>',
              '<?=h(__('Number is required.'))?>',
              '<?=h('content-amount-id-' . $product->id)?>',
              '<?=h('no-of-pcs-' . $product->id)?>',
              '<?=h($stock->id)?>',
              '<?=h($product->product_version['id'])?>',
              '<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'products', 'action' => 'addProductToStock', $product->id, $stock->id])?> ')">
            </td>
        </tr>

        <?php endforeach;?>
    </table>
    </div>

    <div class="modal" id="product-added-to-stock-modal">
        <div class="modal-dialog modal-dialog-centered modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <?=h(__('Product has been added to your stock.'))?>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="btn-group float-right">
                            <button class="cancel-btn" data-dismiss="modal"><?=h(__('OK'))?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif;?>
<?=$this->element('img_modal')?>
<?=$this->Html->script(['projectScripts/img_modal.js', 'forms/stock_modal_validation.js', 'projectScripts/stock_list_form'])?>