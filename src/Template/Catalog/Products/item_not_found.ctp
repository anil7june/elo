<!-- File: src/Template/Catalog/Products/item_not_found.ctp -->
<?=$this->element('header_with_back', ['back_url' => $prev_location, 'header_text' => '', 'back_link_text' => __('Back to stocks')])?>

<div class="not-found-item">
	<h1>
		<?=h(__('No results found'))?>
	</h1>
  	<p>
    	<?=h(__('Check the spelling or') . ' ')?>
		<a href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#add-preproduct-modal">
    		<?=h(__('add custom product to your stock list'))?>
    	</a>
	</p>
</div>

<div class="modal" id="add-preproduct-modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <?=h(__('Add custom product to stock list'))?>
                </div>
            </div>
            <div class="modal-body">
				<?=$this->Form->create('new_preproductt', ['type' => 'file', 'id' => 'new-preproduct'])?>
				<div class="row">
					<div class="col-md-12">
						<?=$this->Form->control('cat_code', ['label' => __('Code'), 'placeholder' => __('Enter Code...')]);?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<?=$this->Form->control('value', ['label' => __('Estimated value (in USD)'), 'placeholder' => '0.00']);?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
    					<?=$this->Form->control('make_id', ['type' => 'select', 'label' => __('Brand'), 'options' => $makes_list, 'value' => $default_make_id]);?>
					</div>
					<div class="col-md-6">
    					<?=$this->Form->control('cat_make_id', ['type' => 'select', 'label' => __('Model'), 'options' => $cat_makes_list, 'empty' => __('Choose one')]);?>
					</div>
				</div>
				<div class="request-frame">
					<p class="bold"><?=h(__('Do you want to send an enquiry to Admin?'))?></p>
					<p class="normal"><?=h(__('(with a request to add this product to your stock list)'))?></p>
					<div class="row justify-content-center">
						<div class="col-md-12 text-center">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="contact_admin" id="contact-admin-yes" value="1">
								<label class="form-check-label" for="contact-admin-yes"><?=h(__('Yes'))?></label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="contact_admin" id="contact-admin-no" value="0" checked>
								<label class="form-check-label" for="contact-admin-no"><?=h(__('No'))?></label>
							</div>
						</div>
					</div>
				</div>
				<div class="row submit-row" style="margin-bottom:7px;">
                    <div class="col-md-12">
                        <div class="btn-group float-right" style="height: 36px;">
							<input type="submit" name="submit" class="btn submit-btn" value="<?=h(__('Save'))?>">
                            <button class="cancel-btn" data-dismiss="modal"><?=h(__('Cancel'))?></button>
                        </div>
                    </div>
				</div>
				<?=$this->Form->end()?>
            </div>
        </div>
	</div>
</div>

<?=$this->Html->script([
    'forms/datepicker.js',
    'forms/validation_message_controller.js',
    'forms/form_field_validator.js',
    'forms/form_validator.js',
])?>

<script>
	$(window).resize(function() {
        if ($(window).width() < 675) {
			$('.file-input.logo-change').addClass('centered');
        } else {
			$('.file-input.logo-change').removeClass('centered');
        }
    });
    $(document).ready(function() {
        if ($(window).width() < 675) {
			$('.file-input.logo-change').addClass('centered');
        } else {
			$('.file-input.logo-change').removeClass('centered');
        }
    });
	$('#new-preproduct').submit(function() {
		return validateForm($(this), {
			'cat-code': {
				id: 'cat-code',
				condition: 'none',
				allowEmpty: false,
				message: '<?=h(__('This field can not be empty'))?>'
			},
			'make-id' : {
				id: 'make-id',
				condition: 'none',
				allowEmpty: false,
				message: '<?=h(__('This field can not be empty'))?>'
			},
			'value': {
				id: 'value',
				condition: 'positiveTwoDecimalPlacesNumber',
				allowEmpty: false,
				message: '<?=h(__('Please insert a floating-point number with two decimal places'))?>'
			},
		});
	});

</script>