<!-- File: src/Template/Catalog/Currencies/index.ctp -->

<h3><?= h(__("Choose Currency")) ?></h3>

<div class="resultsContainer">
  <div id="tempContainer">
  <table class="table paleBlueRows">
    <thead>
      <tr>
        <th><h5><?= __('Currency');?></h5></th>
        <th><h5><?= __('Exchange rate')?></h5></th>
        <th></th>
      </tr>
    <thead>
      <tr>
        <td><?= $currentCurrency['code']?></td>
        <td><?= $currentCurrency['exchange_rate']?></td>
        <td><?= __('Current')?></td>
      </tr>
    <?php foreach($currencyList as $c):?>
      <tr>
        <td><?= $c->code?></td>
        <td><?= $c->versions['exchange_rate'];?></td>
        <td><a href="<?= $this->Url->build([
          "prefix" => "catalog",
          "controller" => "currencies",
          "action" => "updateCurrency",
          "?" => ['currency' => $c->id]
        ]);?>"><?= __('Choose')?></a> </td>
      </tr>
    <?php endforeach;?>
  </table>
</div>
</div>
