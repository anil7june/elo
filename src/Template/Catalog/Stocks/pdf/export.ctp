
<div class="table-responsive">
<table class="table product-table shadow-header-table">
    <thead>
        <tr>
            <th>
                <?=h(__("Code"))?>
            </th>
            <th>
                <?=h(__("Count"))?>
            </th>
            <th class="price" colspan="2">
                <?=h(__('Catalogue value'))?>
            </th>
            <th class="price" colspan="2">
                <?=h(__('Price offered'))?>
            </th>
            <th class="price" colspan="2">
                <?=h(__('Profit'))?>
            </th>
        </tr>
    </thead>

    <?php foreach ($stock_products as $product): ?>
    <tr>
        <td><?=h($product->product_version['cat_code'])?></td>
        <td style="text-align:center;"><?=h($product->no_of_pcs)?></td>
        <td style="text-align:right"><?=h($this->ProductFormat->formatNumber($product['catalogue_value'], $user_id))?></td>
        <td style="text-align:left"><?=h($currency_code)?></td>
        <td style="text-align:right"><?=h($this->ProductFormat->formatNumber($product['user_value'], $user_id))?></td>
        <td style="text-align:left"><?=h($currency_code)?></td>
        <td style="text-align:right"><?=h($this->ProductFormat->formatNumber($product['profit'], $user_id))?></td>
        <td style="text-align:left"><?=h($currency_code)?></td>
    </tr>

    <?php endforeach;?>
</table>
</div>

<div style="margin-top:52px;"></div>
<div class="table-responsive">
<table class="table product-table info-table shadow-header-table">
    <thead>
        <tr>
            <th class="price" colspan="8"><?=h(__('Stock summary'))?></th>
        </tr>
    </thead>
    <tr>
        <td><?=h(__('Products total') . ': ')?></td>
        <td><?=h($this->ProductFormat->formatNumber($stock->total_count, $user_id))?></td>
        <td></td>
        <td><?=h(__('Total value') . ': ')?></td>
        <td><?=h($this->ProductFormat->formatNumber($stock->total_catalogue_value, $user_id))?></td>
        <td></td>
        <td><?=h(__('Avg margin per pc') . ': ')?></td>
        <td><?=h($this->ProductFormat->formatNumber($stock->avg_margin_per_pc, $user_id))?></td>
    </tr>
    <tr>
        <td><?=h(__('Margin') . ': ')?></td>
        <td><?=h($this->ProductFormat->formatNumber($stock->margin, $user_id) . "%, " . $this->ProductFormat->formatNumber($stock->USD_margin, $user_id) . " USD")?></td>
        <td></td>
        <td><?=h(__('Total price') . ': ')?></td>
        <td><?=h($this->ProductFormat->formatNumber($stock->total_user_value, $user_id))?></td>
        <td></td>
        <td><?=h(__('Avg price offered per pc') . ': ')?></td>
        <td><?=h($this->ProductFormat->formatNumber($stock->avg_user_value_per_pc, $user_id))?></td>
    </tr>
    <tr>
        <td><?=h(__('Catalog value per kg') . ': ')?></td>
        <td><?=h($this->ProductFormat->formatNumber($stock->catalogue_value_per_kg, $user_id))?></td>
        <td></td>
        <td><?=h(__('Total profit') . ': ')?></td>
        <td><?=h($this->ProductFormat->formatNumber($stock->total_profit_value, $user_id))?></td>
        <td></td>
        <td><?=h(__('Avg catalog value per pc') . ': ')?></td>
        <td><?=h($this->ProductFormat->formatNumber($stock->avg_catalogue_value_per_pc, $user_id))?></td>
    </tr>
    <?php if ($stock->pt_to_hedge && $stock->pd_to_hedge && $stock->rh_to_hedge): ?>
    <tr>
        <td><?=h(__('Pt to hedge:') . ' ')?></td>
        <td><?=h($this->ProductFormat->formatNumber($stock->pt_to_hedge, $user_id))?></td>
        <td></td>
        <td><?=h(__('Pd to hedge:') . ' ')?></td>
        <td><?=h($this->ProductFormat->formatNumber($stock->pd_to_hedge, $user_id))?></td>
        <td></td>
        <td><?=h(__('Rh to hedge:') . ' ')?></td>
        <td><?=h($this->ProductFormat->formatNumber($stock->rh_to_hedge, $user_id))?></td>
    </tr>
    <?php endif;?>
</table>
</div>