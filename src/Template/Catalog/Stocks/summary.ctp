<?=$this->element('header_with_back', ['back_url' => $back_url, 'header_text' => __('STOCK SUMMARY'), 'back_link_text' => __('Back to basic stock view')])?>
<div class="search-input-margin"></div>
<div class="table-responsive">
    <table class="table product-table info-table shadow-header-table">
        <thead>
            <tr>
                <th colspan="13"><?=h(__('Stock summary'))?></th>
            </tr>
        </thead>
        <?php foreach ($stock_summary_data as $row): ?>
            <tr>
                <?php foreach ($row as $key => $value): ?>
                    <td><?=h($key)?>
                    <td><?php echo $value; ?>
                    <td></td>
                <?php endforeach;?>
            </tr>
        <?php endforeach;?>
    </table>
</div>