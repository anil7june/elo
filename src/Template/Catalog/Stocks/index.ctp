<!-- File: src/Template/Catalog/Stocks/index.ctp -->


<?=$this->element('/stock_index/stock_index_header')?>
<div class="search-input-margin"></div>
<div class="search-input-margin"></div>

<?php if ($stock_open): ?>
<?=$this->element('stock_list')?>
<div class="search-input-margin"></div>
<div class="search-input-margin"></div>
<?php endif;?>

<?php if ($stock->id && $stock_products->count() != 0): ?>
<?=$this->element('stock_table', [$stock_products, $currency_code, $stock])?>
<?php elseif (!$stocks): ?>
<h1> <?=h(__("You have no stocks created"))?> </h1>
<?php elseif (!$stock->id): ?>
<h1> <?=h(__("You have no stock selected"))?> </h1>
<?php else: ?>
<h1> <?=h(__("Your stock is empty"))?> </h1>
<?php endif;?>

<?=$this->Html->script('catalog/stocks/index')?>
