<!-- File: src/Template/Contact/index.ctp -->

<?=$this->element('header', ['header_text' => __('CONTACT US')])?>

<?=$this->Form->create('contact_form', ['id' => 'contact-form', 'type' => 'file'])?>
<div class="contact-form-container">
    <div class="row justify-content-center">
        <?=$this->Form->control('subject', ['type' => 'text', 'label' => __('Subject')])?>
    </div>
    <div class="row justify-content-center">
        <?=$this->Form->control('content', ['type' => 'textarea', 'label' => __('Message')])?>
    </div>
    <div class="search-input-margin"></div>
    <?=$this->element('dotted_attachment', ['header_text' => __('Attachment'), 'upload_message' => __('Upload file'), 'file_name' => "attachment_file", 'file_id' => 'attachment-file', 'no_file_message' => __('No file uploaded')])?>
    <div class="row justify-content-end">
        <input type="submit" class="btn submit-btn" value="<?=h(__('Send message'))?>"/>
    </div>
</div>
<?=$this->Form->end()?>

<?=$this->Html->script([
    'forms/datepicker.js',
    'forms/validation_message_controller.js',
    'forms/form_field_validator.js',
    'forms/form_validator.js',
])?>

<script>
    $(document).ready(function () {
        $("#contact-form").submit(function () {
            return validateForm($(this), {
                'subject': {
                    id: 'subject',
                    condition: 'none',
                    allowEmpty: false,
                    message: '<?=h(__('This field can not be empty'))?>'
                },
                'content': {
                    id: 'content',
                    condition: 'none',
                    allowEmpty: false,
                    message: '<?=h(__('This field can not be empty'))?>'
                }
            });
        });
    });
</script>