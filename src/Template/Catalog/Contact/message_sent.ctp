<!-- File: src/Template/Contact/message_sent.ctp -->

<?=$this->element('message_done', [
    'bold_message_text' => __('Your email has been sent!'),
    'normal_message_text' => __('A member of our support staff will respond as soon as possible.'),
])?>
