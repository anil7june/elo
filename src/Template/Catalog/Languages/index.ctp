<!-- TODO change it  -->
<!-- File: src/Template/Catalog/Currencies/index.ctp -->

<h3><?= __('Choose Language')?></h3>

<br>

<div align='center'>
  <?= $this->Form->create('Set your language', [
    'url' => [
      'prefix' => 'catalog',
      'controller' => 'languages',
      'action' => 'update_language',
      $user->id
    ]
  ]); ?>
  <?= $this->Form->control('language', ['label' => '', 'type' => 'select', 'options' => $languageList, 'style' => 'width: 200px;', 'value' => $user->language_id]); ?>
  <?= $this->Form->submit(__('Accept'), ['name' => 'btn']); ?>
  <?= $this->Form->end(); ?>
</div>
