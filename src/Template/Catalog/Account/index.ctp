<!-- File: src/Template/Acoount/index.ctp -->
<?=$this->element('header', ['header_text' => __('MY ACCOUNT')])?>
<?=$this->Form->create($user, ['type' => 'file'])?>
<div class="row my-account">
    <div class="col-md-2 my-col" id="coins-detail-col">
        <div class="row justify-content-center">
            <img id="my-account-icon" src="/img/icons/icon-my-account.png"/>
        </div>
        <div class="row justify-content-center">
            <span id="coins-info">
                <img id="coins-icon" src="/img/icons/coin-elemental.png"/><?=h($user->coins . ' ' . __('coins'))?>
            </span>
        </div>
        <div class="row justify-content-center">
            <span class="txt-info"><?=h(__('Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do lorem blah blah'))?></span>
        </div>
    </div>
    <div class="col-md-5 my-col" id="personal-details">
        <div class="form-container">
            <div class="row header">
                <p><?=h(__('Personal details'))?></p>
            </div>
            <div class="row">
                <div class="col-md-6 split-col">
                    <?=$this->Form->control('first_name', ['label' => __('First Name'), 'type' => 'text'])?>
                </div>
                <div class="col-md-6 split-col">
                    <?=$this->Form->control('last_name', ['label' => __('Last Name'), 'type' => 'text'])?>
                </div>
            </div>
            <div class="row">
                <?=$this->Form->control('email', ['type' => 'email', 'label' => __('Email address')])?>
            </div>
            <div class="row">
                <?=$this->Form->control('phone', ['type' => 'text', 'label' => __('Cell phone')])?>
            </div>
        </div>
    </div>
    <div class="col-md-5 my-col" id="company-details">
        <div class="form-container">
            <div class="row header">
                <p><?=h(__('Other details'))?></p>
            </div>
            <div class="row">
                <?=$this->Form->control('company', ['type' => 'text', 'label' => __('Company name')])?>
            </div>
            <div class="row">
                <?=$this->Form->control('city', ['type' => 'text', 'label' => __('City')])?>
            </div>
            <div class="row">
                <?=$this->Form->control('country_id', ['type' => 'select', 'options' => $countries, 'label' => __('Country')])?>
            </div>
        </div>
    </div>
</div>
<div class="row my-account">
    <div class="col-md-4 my-col" id="change-logo-col">
        <div class="form-container">
            <div class="row header">
                <p><?=h(__('Change company logo'))?></p>
            </div>
            <div class="row">
                <div class="col-md-4 split-wide-col break">
                    <span class="txt-info centered"><?=h(__('LOGO PREFERENCES:'))?><br><?=h(__('.png file with transparent background'))?></span>
                    <div class="logo-checkbox-wrapper">
                        <?=$this->Form->control('show_logo_in_header', ['type' => 'checkbox', 'label' => ['text' => '<p>' . __('Show in header') . '</p>', 'escape' => false], 'checked' => $user->show_logo_in_header == 1 ? true : false])?>
                    </div>
                </div>
                <div class="col-md-8 split-wide-col logo-col break">
                    <?php if ($user->company_logo): ?>
                    <?=$this->element('change_logo', ['user' => $user])?>
                    <?php else: ?>
                    <?=$this->element('add_first_logo')?>
                    <?php endif;?>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 my-col" id="change-password-col">
        <div class="form-container">
            <div class="row header">
                <p><?=h(__('Change password'))?></p>
            </div>
            <div class="row">
                <div class="col-md-6 split-wide-col break">
                    <span class="txt-info"><?=h(__('After changing the password, you will need to log in again on all devices'))?></span>
                </div>
                <div class="col-md-6 split-wide-col break">
                    <?=$this->Form->control('current_password', ['type' => 'password', 'label' => __('Current password'), 'placeholder' => __('Enter current password')])?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 split-wide-col break">
                    <?=$this->Form->control('password', ['type' => 'password', 'label' => __('New password'), 'placeholder' => __('Enter new password'), 'value' => ''])?>
                </div>
                <div class="col-md-6 split-wide-col break">
                    <?=$this->Form->control('password_2', ['type' => 'password', 'label' => __('Confirm new password'), 'placeholder' => __('Confirm new password')])?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="btn-group float-right">
    <button type="button" onclick="location.href='<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'account', 'action' => 'index'])?>'" id="discard-changes" class="btn white-btn"><?=h(__('Discard the changes'))?></button>
    <input type="submit" class="btn submit-btn" value="<?=h(__('Save changes'))?>"/>
</div>
<?=$this->Form->end()?>

<script>
    $('.logo-col').change(function(event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        $("#logo-change-img").fadeIn("fast").attr('src', URL.createObjectURL(event.target.files[0]));
        $("#logo-text").hide();
        $(".logo-wrapper").addClass("transparent");
    });
    $(window).resize(function() {
        if ($(window).width() < 1320) {
            $('#company-details').insertAfter($('#change-logo-col'));
        } else {
            $('#company-details').insertAfter($('#personal-details'));
        }
    });
    $(document).ready(function() {
        if ($(window).width() < 1320) {
            $('#company-details').insertAfter($('#change-logo-col'));
        } else {
            $('#company-details').insertAfter($('#personal-details'));
        }
    });
</script>