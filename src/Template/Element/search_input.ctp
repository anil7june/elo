<div class="search-input-container">
    <form method="get" accept-charset="utf-8" id="search-input-form" autocomplete="off">
        <div class="search-frame" id="search-frame">
            <span id="search-icon"><img src="/img/icons/icon-search.png"></span>
            <span><input type="search" class="form-control" name="dataInput" id="search-input"
                    placeholder="<?=__('Search')?>"></span>
            <a href="#" class="ico-input" id="drop-down"><img src="/img/icons/icon-arrow-down.png"
                    id="adv-form-btn"></a>
            <a href="#" class="ico-input" id="close"><img class="hide" src="/img/icons/icon-close-black.png"
                    id="close-icon"></a>
        </div>
        <input type="submit" class="btn search-input-button" name="search_button" id="search-button" value="<?=__('Search')?>">
        <div style="display:none;">
            <input type="hidden" name="_method" value="POST">
        </div>
    </form>
    <div class="live-search-container hide">
        <div class="live-search-list">
            <ul>
            </ul>
        </div>
    </div>
    <div class="adv-search-frame hide">
        <?=$this->Form->create('advanced-search-form', ['type' => 'get', 'id' => 'advanced-search-form', 'style' => 'width:100%;'])?>
        <div class="row">
            <div class="col-md-6">
                <?=$this->Form->control('make_id', ['label' => __('Brand'), 'type' => 'select', 'options' => $makes_list, 'empty' => __('Not Chosen'), 'value' => $filterValues['make_id']]);?>
            </div>
            <div class="col-md-6">
                <?=$this->Form->control('cat_make_id', ['label' => __('Model'), 'type' => 'select', 'options' => $cat_makes_list, 'empty' => __('Not Chosen'), 'value' => $filterValues['cat_make_id']]);?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?=$this->Form->control('cat_code', ['label' => 'Catalyst code', 'type' => 'text', 'value' => $filterValues['cat_code']]);?>
            </div>
            <div class="col-md-6">
                <?=$this->Form->control('ecr_code', ['label' => 'ID', 'type' => 'text', 'value' => $filterValues['ecr_code']]);?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row inside">
                    <div class="control-label"><?=h(__('Monolith Weight'))?></div>
                </div>
                <div class="row inside">
                    <div class="col-md-6 boundary-values">
                        <?=$this->element('units/text_input', ['unit' => $this->Units->kg(), 'field_name' => 'min_weight', 'other_props' => ['value' => $filterValues['min_weight'], 'placeholder' => __('Min')]])?>
                        <div class="dash"></div>
                    </div>
                    <div class="col-md-6 boundary-values">
                        <?=$this->element('units/text_input', ['unit' => $this->Units->kg(), 'field_name' => 'max_weight', 'other_props' => ['value' => $filterValues['max_weight'], 'placeholder' => __('Max')]])?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row inside">
                    <div class="control-label"><?=h(__('Catalyst Price'))?></div>
                </div>
                <div class="row inside">
                    <div class="col-md-6 boundary-values">
                        <?=$this->element('units/text_input', ['unit' => $this->Units->getCurrencySymbol($user_currency_code), 'field_name' => 'min_price', 'other_props' => ['value' => $filterValues['min_price'], 'placeholder' => __('Min')]])?>
                        <div class="dash"></div>
                    </div>
                    <div class="col-md-6 boundary-values">
                        <?=$this->element('units/text_input', ['unit' => $this->Units->getCurrencySymbol($user_currency_code), 'field_name' => 'max_price', 'other_props' => ['value' => $filterValues['max_price'], 'placeholder' => __('Max')]])?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row unit-margin"></div>
        <?php if ($show_product_status): ?>
        <div class="row">
            <div class="col-md-6">
                <?=$this->Form->control('product_status_id', ['label' => __('Product Status'), 'type' => 'select', 'options' => $product_statuses_list, 'empty' => __('Product Status'), 'value' => $filterValues['product_status_id']]);?>
            </div>
            <div class="col-md-6">
                <?=$this->Form->control('region_code', ['label' => 'Region', 'type' => 'select', 'options' => $countries_list, 'value' => $filterValues['country_id']]);?>
            </div>
        </div>
        <?php endif;?>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" class="submit-btn" name="advanced_search_button" value="<?=h(__('Search'))?>">
            </div>
        </div>
        <?=$this->Form->end()?>
    </div>
</div>

<script>
//autofill
function move(direction) {
    var element = $('li div.live-search-element.active');
    var new_id;
    if(element.length === 0) {
        new_id = '0-e';
    } else {
        element.removeClass('active');
        var incr;
        if(direction === "up") {
            incr = 4
        } else if(direction === "down") {
            incr = 1
        }
        new_id = ((parseInt(element.attr("id")) + incr) % 5) + '-e';
    }
    element = $('#' + new_id);
    element.addClass('active');
    $('#search-input').val(element.text());
}

function load_behavior() {
    $('#search-input').unbind('keydown')
    .keydown(function(e) {
        var UP_KEY = 38;
        var DOWN_KEY = 40;
        var ESCAPE_KEY = 27;
        var ENTER_KEY = 13;
        if(e.keyCode == UP_KEY) {
            move("up");
        } else if(e.keyCode == DOWN_KEY) {
            move("down");
        } else if(e.keyCode == ESCAPE_KEY) {
            hide();
        } else if(e.keyCode == ENTER_KEY) {
            hide();
        }
    });
    $('#search-input').unbind('click')
    .click(function() {
        show_or_hide();
    });
    $("div.live-search-element").each(function() {
        $(this).hover(
            function() {
                $(this).addClass('active');
            },
            function() {
                $(this).removeClass('active');
        });
        $(this).click(function() {
            input_value = $(this).text();
            $('#search-input').val(input_value);
            hide();
        });
    });
}

function show_or_hide() {
    input = $('#search-input').val();
    if(input.length > 0 && input != "Search") {
        if($('.live-search-container').hasClass('hide')) {
            $('.live-search-container').removeClass('hide');
        }
    }
    if(input.length == 0 || input == "Search") {
        hide();
    }
}

function hide() {
    if(!$('.live-search-container').hasClass('hide')) {
        $('.live-search-container').addClass('hide');
    }
}

function empty_suggestion_list() {
    $('.live-search-container .live-search-list ul').empty();
}


function no_suggestions() {
    empty_suggestion_list();
    $('.live-search-container .live-search-list ul').append('<li><div class="live-search-element">No suggestions</div></li>');
}

function create_suggestion_list(data) {
    empty_suggestion_list();
    var elem_counter = -1;
    $.each(data, function(index, element) {
        if(index != "empty") {
            elem_counter = elem_counter + 1;
            $('.live-search-container .live-search-list ul').append('<li><div class="live-search-element" id="' + elem_counter + '-e" >' + element.make.name + " " + element.product_version.cat_code + "</div></li>");
        }
    });
}

function loading_suggestions_message() {
    empty_suggestion_list();
    $('.live-search-container .live-search-list ul').append('<li><div class="live-search-element">Loading suggestions...</div></li>');
}

function load_suggestions_from_api() {
    loading_suggestions_message();
    var input_val = $('#search-input').val();
    $.ajax({
        type: "POST",
        url: "<?=h($this->Url->build(['prefix' => 'catalog', 'controller' => 'products', 'action' => 'live_search']))?>",
        data: {
            input: input_val
        },
        success: function(data) {
            if(data.empty == "false") {
                create_suggestion_list(data);
                load_behavior();
                show_or_hide();
            } else {
                no_suggestions();
                show_or_hide();
            }
        },
        error: function() {
            console.log('error!');
        }
    });
}

//global variables to make sure user stoped typing
var textInput = document.getElementById('search-input');
var timeout = null;

textInput.onkeyup = function (e) {

    clearTimeout(timeout);

    timeout = setTimeout(function () {
        load_suggestions_from_api();
    }, 500);
};

//end autofill

$('#search-input').focus(function() {
    var input = $('#search-input').val();
    $('#search-frame').addClass('active');
    show_or_hide();
}).blur(function() {
    $('#search-frame').removeClass('active');
});

$(document).mouseup(function(e) {
    var container = $('live-search-container');
    if (!container.is(e.target) && container.has(e.target).length === 0 && !$('#search-frame').hasClass('active')) {
        hide();
    }
});

function isNumber(str) {
    let decimal_regex = /^[0-9]+\.[0-9]+$/;
    let int_regex = /^[0-9]+$/;

    return str.match(decimal_regex) || str.match(int_regex);
}

$(function() {
    $('#advanced-search-form').submit(function() {
        var validation_result = true;
        $('.col-md-3 .form-group.text').each(function() {
            let input = $(this).find('input');
            let input_val = input.val().trim();
            if (!isNumber(input_val) && input_val) {
                validation_result = false;
                $(this).removeClass('form-group text');
                $(this).addClass("form-group has-error text");
                input.addClass('has-error');
                if ($(this).find('span').length == 0) {
                    $(this).append('<span class="help-block error-message">' +
                        '<?=h(__('Insert a number'))?>' +
                        '</span>');
                }
            } else {
                $(this).removeClass('has-error');
                $(this).find('span').remove();
            }
        });
        return validation_result;
    });
});

$('#search-input').on('input', function() {
    let input = $.trim($('#search-input').val());
    $('#close-icon').removeClass('hide');
    if (!input) {
        $('#close-icon').addClass('hide');
    }
});

$('#close').on('click', function() {
    $('#search-input').val('');
    $('#close-icon').addClass('hide');
    $('#search-input').focus();
});

$('#drop-down').on('click', function() {
    if ($('.adv-search-frame').hasClass('hide')) {
        $('.adv-search-frame').removeClass('hide');
    } else {
        $('.adv-search-frame').addClass('hide');
    }
});
</script>