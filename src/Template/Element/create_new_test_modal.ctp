<div class="modal create-modal" id="create-test-modal">
    <div class=" modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <?=h(__('Add new test to product'))?>
                </div>
            </div>
            <div class="modal-body">
                <?=$this->Form->create('test', ['id' => 'create-test-form', 'url' => $submit_url])?>
                <div class="row">
                    <div class="col-md-6">
                        <?=$this->element('units/text_input', ['field_name' => 'pt', 'unit' => $this->Units->ppm(), 'other_props' => ['label' => __('Platinum'), 'placeholder' => '0']])?>
                    </div>
                    <div class="col-md-6">
                        <?=$this->element('units/text_input', ['field_name' => 'pd', 'unit' => $this->Units->ppm(), 'other_props' => ['label' => __('Palladium'), 'placeholder' => '0']])?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?=$this->element('units/text_input', ['field_name' => 'rh', 'unit' => $this->Units->ppm(), 'other_props' => ['label' => __('Rhodium'), 'placeholder' => '0']])?>
                    </div>
                    <div class="col-md-6">
                        <?=$this->Form->control('analysis_type_id', ['type' => 'select', 'options' => $analysis_types_list, 'label' => __('Analisys Type'), 'empty' => '--- choose ---'])?>
                    </div>
                </div>
                <div class="row submit-row">
                    <div class="col-md-12">
                        <div class="btn-group float-right" style="height: 36px;">
                            <input type="submit" name="submit" class="btn submit-btn" value="<?=h(__('Save'))?>">
                            <button id="close-create-new-test-form-modal" class="cancel-btn" data-dismiss="modal"><?=h(__('Cancel'))?></button>
                      </div>
                    </div>
                </div>
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
</div>