<div class="row product-view-row">
    <div class="col-md-12 my-col product-form-frame">
        <div class="product-details-container" id="product-details-container">
            <div class="row tests-row-header">
                <div class="col-md-6 header-col my-auto">
                    <p><?=h(__('Tests'))?></p>
                </div>
                <div class="col-md-6 header-col">
                    <div class="btn-group float-right my-btn-group">
                        <button class="common-btn new-test-btn" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#create-test-modal">
                            <?=h(__('Add new test'))?>
                        </button>
                    </div>
                </div>
            </div>
            <div class="table-responsive <?php if (!($tests && $tests->count() > 0)): ?><?=h("hide")?><?php endif;?>" id="test-table-container">
                <table class="table product-table info-table shadow-header-table user-view-table" id="tests-table">
                    <tr class="header">
                        <td class="border-bottom border-right bold-content text-centered"><?=h(__('Platinum') . ' (' . $this->Units->ppm() . ')')?></td>
                        <td class="border-bottom border-right bold-content text-centered"><?=h(__('Palladium') . ' (' . $this->Units->ppm() . ')')?></td>
                        <td class="border-bottom border-right bold-content text-centered"><?=h(__('Rhodium') . ' (' . $this->Units->ppm() . ')')?></td>
                        <td class="border-bottom border-right bold-content text-centered"><?=h(__('Assay Provider'))?></td>
                        <td class="border-bottom border-right bold-content text-centered"><?=h(__('Status'))?></td>
                        <td class="border-bottom border-right bold-content text-centered"></td>
                        <td class="border-bottom bold-content text-centered"></td>
                    </tr>
                    <?php foreach ($tests as $test): ?>
                        <tr>
                            <td class="border-bottom border-right normal-content text-centered"><?=h($test->pt)?></td>
                            <td class="border-bottom border-right normal-content text-centered"><?=h($test->pd)?></td>
                            <td class="border-bottom border-right normal-content text-centered"><?=h($test->rh)?></td>
                            <td class="border-bottom border-right normal-content text-centered"><?=h($test->analysis_type['name'])?></td>
                            <td class="border-bottom border-right normal-content text-centered"><?=h($testStatus[$test->id])?></td>
                            <td class="border-bottom border-right normal-content text-centered">
                                <?php if ($testStatus[$test->id] == 'Applied'): ?>
                                    <a href="<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'tests', 'action' => 'disactivateTest', $test->id], true)?>">
                                        <img src="/img/icons/icon-close-black.png" class="test-ico">
                                    </a>
                                <?php elseif ($testStatus[$test->id] == 'Disabled'): ?>
                                    <a href="<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'tests', 'action' => 'activateTest', $test->id], true)?>">
                                        <img src="/img/icons/icon-checked-tests.png" class="test-ico">
                                    </a>
                                <?php endif;?>
                            </td>
                            <td class="border-bottom normal-content text-centered">
                                <a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#delete-test-modal" data-param="<?=h($test->id)?>">
                                    <img src="/img/stock_icons/icon-remove-stock.png" class="test-ico">
                                </a>
                            </td>
                        </tr>
                    <?php endforeach;?>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->element('create_new_test_modal', ['submit_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'tests', 'action' => 'addTestToProduct', $product->id])])?>