<div class="table-responsive">
    <table class="table product-table shadow-header-table">
        <thead>
            <tr>
                <th width="7%"></th>
                <th width="10%">
                    <?=h(__("Code"))?>
                </th>
                <th width="7%" colspan="3" class="price">
                    <?=h(__("Count"))?>
                </th>
                <th width="7%">
                    <?=h(__('Brand'))?>
                </th>
                <th width="7%">
                    <?=h(__('Model'))?>
                </th>
                <th class="price-header" colspan="2">
                    <?=h(__('Price'))?>
                </th>
                <th width="4%">
                    <!--edit -->
                </th>
                <th width="4%">
                    <!-- remove -->
                </th>
            </tr>
        </thead>

        <?php foreach ($stock_products as $product): ?>
        <tr>
            <td class="text-left-with-padding">
                <?php if ($product->image['file_name']): ?>
                    <img class="product-image" src="<?=h($this->ProductFormat->imagePath($product->image['file_name']))?>" onclick="imgPopup('<?=h($product->product['id'])?>')"/>
                <?php endif;?>
            </td>
            <td><?=h($product->product_version['cat_code'])?></td>
            <td class="text-right">
                <?php if ($stock_open): ?>
                    <?php if ($product->no_of_pcs > 1): ?>
                        <a class="stock-ico"
                            href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'decrementStockProduct', $product->id])?>">
                            <img src="/img/stock_icons/icon-minus.png">
                        </a>
                    <?php endif;?>
                <?php endif;?>
            </td>
            <td class="text-centered">
                <?=h($product->no_of_pcs)?></td>
            <td class="text-left">
                <?php if ($stock_open): ?>
                    <a class="stock-ico"
                        href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'incrementStockProduct', $product->id])?>">
                        <img src="/img/stock_icons/icon-plus.png">
                    </a>
                <?php endif;?>
            </td>
            <td><?=h($product->make['name'])?></td>
            <td><?=h($product->cat_make['name'])?></td>
            <td class="price-value"><?=h($this->ProductFormat->formatNumber($product['user_value'], $user_id))?></td>
            <td class="price-currency"><?=h($currency_code)?></td>
            <td>
                <?php if ($stock_open): ?>
                    <a class="stock-ico" data-toggle="modal" data-backdrop="static" data-keyboard="false"
                        href="#edit-stock-product-modal"
                        data-product="<?=h(json_encode(['id' => $product->id, 'no_of_pcs' => $product->no_of_pcs, 'content_amount_id' => $product->content_amount->id, 'catalogue_value' => $product->catalogue_value]))?>">
                        <img src="/img/stock_icons/icon-edit.png">
                    </a>
                <?php endif;?>
            </td>
            <td>
                <?php if ($stock_open): ?>
                    <a class="stock-ico" data-toggle="modal" data-backdrop="static" data-keyboard="false"
                        href="#delete-stock-product-modal" data-param="<?=h($product->id)?>">
                        <img src="/img/stock_icons/icon-remove-stock.png">
                    </a>
                <?php endif;?>
            </td>
        </tr>

        <?php endforeach;?>
    </table>
</div>

<div class="col-md-3 user-stock-summary-col">
    <div class="row">
        <span class="user-stock-summary-header">
            <?=h(__('Summary'))?>
        </span>
    </div>
    <table class="table product-table info-table">
        <tr>
            <td><?=h(__('Total count') . ': ')?></td>
            <td><?=h($stock->total_count)?></td>
        </tr>
        <tr>
            <td><?=h(__('Total price') . ': ')?></td>
            <td><?=h($stock->total_user_value . ' ' . $currency_code)?></td>
        </tr>
    </table>
</div>

<?=$this->element('edit_stock_product_modal')?>

<?=$this->element('confirm_action_modal_with_param', [
    'modal_id' => 'delete-stock-product-modal',
    'modal_title' => __('Delete product from stock'),
    'modal_message' => __('Are you sure you want to delete this product?'),
    'delete_action_url' => $this->Url->build([
        'prefix' => 'catalog',
        'controller' => 'stocks',
        'action' => 'removeStockProduct',
    ]),
    'confirm_button_text' => __('Delete'),
])?>

<?=$this->element('img_modal')?>
