<?=$this->element('confirm_modal', [
    'modal_id' => $modal_id,
    'modal_title' => $modal_title,
    'modal_message' => $modal_message,
    'confirm_action_url' => '',
    'confirm_button_text' => $confirm_button_text,
])?>

<script>
$('#<?=h($modal_id)?>').on('show.bs.modal', function(e) {

    let param = e.relatedTarget.dataset.param;
    $('#<?=h($modal_id)?> a').attr('href', '<?=h($delete_action_url)?>' + '/' + param);

});
</script>
