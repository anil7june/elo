<div class="row">
	<div class="product-photo-container">
		<img src="" class="product-photo-view" id="main-galery-image" />
	</div>
</div>
<div class="row">
	<?=$this->Form->control('product_id_hide', ['value' => $product->id, 'type' => 'hide'])?>
	<div class="col-md-6"><img id="prev-image" src="/img/icons/icon-arrow-left.png" class="clickable"></div>
	<div class="col-md-6" style="text-align:right;"><img id="next-image" src="/img/icons/icon-arrow-right.png" class="clickable"></div>
</div>

<?=$this->Html->script('element/product_image_galery.js')?>
