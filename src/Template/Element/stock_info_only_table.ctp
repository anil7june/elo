<table class="table product-table indent-table" id="stock-<?=$stock_id?>-info-only-table">
    <thead>
        <tr>
            <th class="photo-col"><?=h(__('Product'))?></th>
            <th class="cat-code-col">
                <?=h(__("Code"))?>
            </th>
            <th class="no-of-pcs-col">
                <?=h(__("Count"))?>
            </th>
            <th class="content-amount-col">
                <?=h(__('Content Amount'))?>
            </th>
            <th class="catalogue-value-col">
                <?=h(__('Catalogue value'))?>
            </th>
            <th class="user-value-col">
                <?=h(__('Price offered'))?>
            </th>
            <th class="profit-col">
                <?=h(__('Profit'))?>
            </th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>