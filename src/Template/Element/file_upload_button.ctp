<div class="file-input logo-change">
	<span class="file-input-msg">
        <img src="/img/icons/upload-logo-icon.png"><?=h($upload_message)?>
    </span>
    <input type="file" name="<?=h($file_name)?>" id="<?=h($file_id)?>">
</div>