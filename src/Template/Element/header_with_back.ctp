<div class="row header-row">
    <div class="col-md-3 header-with-link-col" id="back-link-col">
        <a class="back-link" href="<?=h($back_url)?>"><img class="img-left"src="/img/stock_icons/icon-arrow-back.png" /><?=h($back_link_text)?></a>
    </div>
    <div class="col-md-6" id="header-content-col">
        <p class="header"><?=h($header_text)?></p>
    </div>
</div>
