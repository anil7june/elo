<div class="modal" id="<?=h($modal_id)?>">
    <div class=" modal-dialog modal-dialog-centered modal-create">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <?=h($modal_title)?>
                </div>
            </div>
            <div class="modal-body">
            <?=$this->Form->create($new_entity, ['url' => $action_url, 'id' => 'create-' . $param . '-form'])?>
                <?php if ($param == 'model'): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <?=$this->Form->control($param . '_make_id', ['type' => 'select', 'label' => __('Brand'), 'empty' => __('Not Chosen'), 'options' => $makes_list])?>
                        </div>
                </div>
                <?php endif;?>
                <div class="row">
                    <div class="col-md-12">
                        <?=$this->Form->control($param . '_name', ['label' => __('Name'), 'type' => 'text']);?>
                    </div>
                </div>
                <div class="row submit-row" style="margin-bottom:7px;">
                    <div class="col-md-12">
                        <div class="btn-group float-right" style="height: 36px;">
                        <input type="submit" name="submit" class="btn submit-btn" value="<?=h($confirm_button_text)?>">
                            <button class="cancel-btn" data-dismiss="modal"><?=h(__('Cancel'))?></button>
                        </div>
                    </div>
                </div>
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
</div>