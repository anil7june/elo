<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="message success" onclick="this.classList.add('hidden')">
    <div class="alert alert-custom success">
        <p><img src="/img/icons/icon-info-white.png" class="flash-msg-icon" /><?=h($message)?><button id="close-btn"><img
                    src="/img/icons/icon-close-white.png" class="flash-msg-close" /></button></p>
    </div>
</div>

<script src="/js/projectScripts/flash_script.js"></script>
