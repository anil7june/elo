<?php if ($products->count() > 0): ?>

<div class="paginator-style">
    <div class="left">
        <?=h($this->Paginator->params()['start'] . ' - ' . $this->Paginator->params()['end'] . __(' from ') . $this->Paginator->params()['count'])?>
    </div>
    <div class="right">
        <?=$this->element('paginator_menu')?>
    </div>
</div>
<div class="table-responsive">
<table class="table product-table shadow-header-table" id="product-table">
    <thead>
        <tr>
            <th width="5%">
                <?=h(__('Product'))?>
            </th>
            <th width="7%">
                <?=h(__('Code'))?>
            </th>
            <th width="8%">
                <?=h(__('Brand'))?>
            </th>
            <th width="8%">
                <?=h(__('Model'))?>
            </th>
            <th width="7%">
                <?=h(__('Monolith Weight'))?>
            </th>
            <?php if ($show_product_status): ?>
                <th width="7%">
                    <?=h(__('Status'))?>
                </th>
            <?php endif;?>
            <th colspan="2" class="price-header" width="7%">
                <?php if ($type_of_price === "catalogue_value"): ?>
                <?=h(__('Catalogue Price'))?>
                <?php endif;?>
                <?php if ($type_of_price === "gross_value"): ?>
                <?=h(__('Gross Value'))?>
                <?php endif;?>
            </th>
            <th width="4%"></th>
        </tr>
    </thead>

    <?php foreach ($products as $product): ?>

    <tr>
        <td><img class="product-image" src="<?=h($this->ProductFormat->imagePath($product->image['file_name']))?>" onclick="imgPopup('<?=h($product->id)?>')"/></td>
        <td><?=h($product->product_version['cat_code'])?></td>
        <td><?=h($product->make['name'])?></td>
		<td><?=h($product->cat_make['name'])?></td>
        <td><?=h($product->product_version['weight'])?></td>
        <?php if ($show_product_status): ?>
                <td>
                    <?=h($product->product_status['name'])?>
                </td>
            <?php endif;?>
        <td class="price-value">
            <?php if ($type_of_price === "catalogue_value"): ?>
            <?=h($this->ProductFormat->formatNumber($product->catalogue_value, $user_id))?>
            <?php endif;?>
            <?php if ($type_of_price === "gross_value"): ?>
            <?=h($this->ProductFormat->formatNumber($product->gross_value, $user_id))?>
            <?php endif;?>
        </td>
        <td class="price-currency">
            <?=h($currency_code)?>
        </td>
        <td>
            <button onclick="location.href='<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'products', 'action' => 'productView', $product->id])?>'"class="btn btn-view"><?=h(__('View'))?></button>
        </td>
    </tr>
    <?php endforeach;?>
</table>
</div>
<?php endif;?>

<?=$this->element('img_modal')?>