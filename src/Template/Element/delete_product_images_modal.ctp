<div class="modal" id="delete-images-modal">
    <div class=" modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <?=h(__('Delete Images'))?>
                </div>
            </div>
            <div class="modal-body">
				<?=$this->Form->create('', ['id' => 'delete-images-from-product-form'])?>
        <?=$this->Form->control('product_id', ['value' => $product->id, 'type' => 'hidden'])?>
				<div class="row">
				<?php if ($images && $images->count() > 0): ?>
					<?php foreach ($images as $image): ?>
						<div class="row image-row">
							<div class="col-md-10">
								<img src="<?=h($this->ProductFormat->imagePath($image->file_name))?>" class="delete-product-image"/>
							</div>
							<div class="col-md-2">
								<?=$this->Form->control('image_' . $image->id, ['type' => 'checkbox', 'value' => $image->id, 'label' => ['text' => '<p>' . '</p>', 'escape' => false], 'checked' => false])?>
							</div>
						</div>
					<?php endforeach;?>
				<?php else: ?>
					<label><?=h(__('Product has no images...'))?></label>
				<?php endif;?>
				</div>
				<div class="row submit-row">
					<div class="col-md-12">
						<div class="btn-group float-right" style="height: 36px;">
							<?php if ($images && $images->count() > 0): ?>
								<input id="submit-delete-pictures-modal" type="submit" name="submit" class="btn submit-btn" value="<?=h(__('Delete'))?>">
							<?php endif;?>
							<button class="cancel-btn" data-dismiss="modal"><?=h(__('Cancel'))?></button>
						</div>
					</div>
				</div>
				<?=$this->Form->end()?>
            </div>
        </div>
    </div>
</div>
<?=$this->Html->script('element/delete_product_images_modal.js')?>