<div class="paginator-style">
    <div class="left">
        <?=h($this->Paginator->params()['start'] . ' - ' . $this->Paginator->params()['end'] . __(' from ') . $this->Paginator->params()['count'])?>
    </div>
    <div class="right">
        <?=$this->element('paginator_menu')?>
    </div>
</div>
<div class="table-responsive">
<table class="table product-table shadow-header-table">
    <thead>
        <tr>
            <th><?=h('ID')?></th>
            <th class="text-center"><?=h(__('Owner'))?></th>
            <th><?=h(__('Name'))?></th>
            <th><?=h(__('Status'))?></th>
            <th colspan="2"><?=h(__('Catalogue value'))?></th>
            <th colspan="2"><?=h(__('Value saved'))?></th>
            <th colspan="2"><?=h(__('Profit per pc'))?></th>
            <th colspan="2"><?=h(__('Profit per kg'))?></th>
            <th></th>
        </tr>
    </thead>
    <?=$this->Form->control('action_url', ['value' => $this->Url->build(['prefix' => 'admin', 'controller' => 'stocks', 'action' => 'getStockDetails'], true), 'type' => 'hidden'])?>
    <?php foreach ($stocks as $stock): ?>
        <tr id="stock-<?=$stock->id?>-header-row">
            <td><?=h($stock->id)?></td>
            <td class="text-center">
                <span class="user-name"><?=h($stock->user->first_name . ' ' . $stock->user->last_name)?></span><br>
                <span class="user-mail"><?=h($stock->user->email)?></span>
            </td>
            <td><?=h($stock->name)?></td>
            <td><?=h($stock->stock_status->name)?></td>
            <td class="text-right"><?=h($this->ProductFormat->formatNumber($stock['total_catalogue_value'], $user_id))?></td>
            <td class="text-left"><?=h($currencyCode)?></td>
            <td class="text-right"><?=h($this->ProductFormat->formatNumber($stock['total_user_value'], $user_id))?></td>
            <td class="text-left"><?=h($currencyCode)?></td>
            <td class="text-right"><?=h($this->ProductFormat->formatNumber($stock['profit_per_pc'], $user_id))?></td>
            <td class="text-left"><?=h($currencyCode)?></td>
            <td class="text-right"><?=h($this->ProductFormat->formatNumber($stock['profit_per_kg'], $user_id))?></td>
            <td class="text-left"><?=h($currencyCode)?></td>
            <td>
                <a href="#" class="toggle-link" id="<?=h($stock->id)?>-arrow-up"><img class="users-stocks-icon" src="/img/icons/icon-arrow-down-users-stocks.png" /></a>
                <a href="#" class="toggle-link hide" id="<?=h($stock->id)?>-arrow-down"><img class="users-stocks-icon" src="/img/icons/icon-arrow-up-users-stocks.png" /></a>
            </td>
        </tr>
        <tr class="manage-users-stocks-row hide" id="stock-<?=h($stock->id)?>-details">
            <td class="manage-users-stocks-td" colspan="13">
                <?=$this->element('stock_info_only_table', ['stock_id' => $stock->id])?>
                <?=$this->element('stock_summary_table', ['stock_id' => $stock->id])?>
            </td>
        </tr>
    <?php endforeach;?>
</table>
</div>
<?=$this->Html->script('tables/stocks_table_manager.js')?>
<script>
    $('.toggle-link').each(function() {
        $(this).click(function() {
            let stock_id = stockId($(this));
            let action_url = $('#action-url').val();
            if(!dataInserted(stock_id)) {
                insertData(action_url, stock_id);
            }
            toggleStockInfoDOM(stock_id);
        });
    });

</script>