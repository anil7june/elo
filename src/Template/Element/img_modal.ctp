<div class="modal img-modal">
    <div class=" modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button id="close-img-modal" type="button" class="close close-modal-btn" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="product-image-modal-body">
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('element/img_modal.js')?>
