<div class="centered btn-logo-change-container">
    <div class="logo-wrapper change transparent">
        <img src="<?=h("/uploads/company_logo/" . $user->company_logo)?>"  id="logo-change-img" />
    </div>
</div>
<div class="centered btn-logo-change-container">
    <?=$this->element('file_upload_button', ['upload_message' => __('Change logo'), 'file_name' => 'company_logo_file', 'file_id' => 'company-logo-file'])?>
</div>