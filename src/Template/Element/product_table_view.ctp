<div class="table-responsive-md">
    <table class="table product-table info-table shadow-header-table user-view-table">
        <tr>
            <td class="border-bottom"><?=h(__('Code'))?></td>
            <td class="border-bottom text-right"><?=h($product->product_version['cat_code'])?></td>
        </tr>
        <tr>
            <td class="border-bottom"><?=h(__('Product ID'))?></td>
            <td class="border-bottom text-right"><?=h($product->product_version['ecr_code'])?></td>
        </tr>
        <tr>
            <td class="border-bottom"><?=h(__('Additional Ref. Data'))?></td>
            <td class="border-bottom text-right"><?=h($product->product_version['additional_ref_data'])?></td>
        </tr>
        <tr>
            <td class="border-bottom"><?=h(__('Brand'))?></td>
            <td class="border-bottom text-right"><?=h($product->make['name'])?></td>
        </tr>
        <tr>
            <td class="border-bottom"><?=h('Model')?></td>
            <td class="border-bottom text-right"><?=h($product->cat_make['name'])?></td>
        </tr>
        <tr>
            <td class="border-bottom"><?=h('Monolith Weight')?></td>
            <td class="border-bottom text-right"><?=h($product->product_version['weight'])?></td>
        </tr>
        <tr>
            <td class="border-bottom"><?=h(__('Catalogue Price'))?></td>
            <td class="border-bottom text-right"><?=h($this->ProductFormat->formatNumber($product->catalogue_value, $user_id))?></td>
        </tr>
        <tr>
            <td class="no-border"><?=h(__('Value Per KG'))?></td>
            <td class="no-border text-right"><?=h($this->ProductFormat->formatNumber($value_per_kg, $user_id))?></td>
        </tr>
    </table>
</div>