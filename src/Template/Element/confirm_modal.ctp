<div class="modal" id="<?=h($modal_id)?>">
    <div class=" modal-dialog modal-dialog-centered modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <?=h($modal_title)?>
                </div>
            </div>
            <div class="modal-body">
                <p><?=h($modal_message)?></p>
                <div class="row submit-row" style="margin-bottom:7px;">
                    <div class="col-md-12">
                        <div class="btn-group float-right" style="height: 36px;">
                            <a href="<?=h($confirm_action_url)?>"><button type="button"
                                    class="btn submit-btn"><?=h($confirm_button_text)?></button></a>
                            <button class="cancel-btn" data-dismiss="modal"><?=h(__('Cancel'))?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>