<div class="modal" id="edit-stock-product-modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <?=h(__('Edit stock product'))?>
                </div>
            </div>
            <div class="modal-body">
                <?=$this->Form->create('edit-form', ['url' => ['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'editStockProduct'], 'id' => 'edit-form'])?>
                <div class="row">
                    <div class="col-md-6" id="content-amount-col">
                        <?=$this->Form->control('content_amount_id', ['type' => 'select', 'options' => $content_amounts_list, 'label' => __('Content')])?>
                    </div>
                    <div class="col-md-6" id="no-of-pcs-col">
                        <?=$this->Form->control('no_of_pcs', ['type' => 'text', 'label' => __('Count')])?>
                        <?=$this->Form->hidden('id', ['id' => 'id'])?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?=$this->element('units/text_input', ['field_name' => 'price', 'unit' => $this->Units->getCurrencySymbol($user_currency_code), 'other_props' => ['label' => __('Custom Price')]])?>
                    </div>
                </div>
                <div class="row submit-row" style="margin-bottom:7px;">
                    <div class="col-md-12">
                        <div class="btn-group float-right" style="height: 36px;">
                            <input type="submit" name="submit" class="btn submit-btn" value="<?=h(__('Save'))?>">
                            <button class="cancel-btn" data-dismiss="modal"><?=h(__('Cancel'))?></button>
                        </div>
                    </div>
                </div>
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('element/edit_stock_product_modal')?>