<?php if ($products->count() > 0): ?>

<div class="paginator-style">
    <div class="left">
        <?=h($this->Paginator->params()['start'] . ' - ' . $this->Paginator->params()['end'] . __(' from ') . $this->Paginator->params()['count'])?>
    </div>
    <div class="right">
        <?=$this->element('paginator_menu')?>
    </div>
</div>
<div class="table-responsive">
  <table class="table product-table shadow-header-table">
    <thead>
  	<tr>
		<th></th>
		<th><?=h(__('Code'))?></th>
		<th><?=h(__('Brand'))?></th>
		<th><?=h(__('Model'))?></th>
		<th><?=h(__('Monolith Weight'))?></th>
		<th><?=h(__('Status'))?></th>
		<th><?=h('Pt' . ' (' . $this->Units->ppm() . ')')?></th>
		<th><?=h('Pd' . ' (' . $this->Units->ppm() . ')')?></th>
		<th><?=h('Rh' . ' (' . $this->Units->ppm() . ')')?></th>
		<th colspan="2" class="price-header"><?=h(__('Gross Value'))?></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
  	</tr>
  </thead>

  	<?php foreach ($products as $product): ?>

  		<tr>
        	<td>
				<img class="product-image" src="<?=h($this->ProductFormat->imagePath($product->image['file_name']))?>" onclick="imgPopup('<?=h($product->id)?>')"/>
			</td>
  			<td><?=h($product->product_version['cat_code'])?></td>
  			<td><?=h($product->make['name'])?></td>
			<td><?=h($product->cat_make['name'])?></td>
			<td><?=h($product->product_version['weight'])?></td>
			<td><?=h($product->product_status['name'])?></td>
        	<td><?=h($product->test['pt'])?></td>
        	<td><?=h($product->test['pd'])?></td>
       		<td><?=h($product->test['rh'])?></td>
  			<td class="price-value"><?=h($this->ProductFormat->formatNumber($product->catalogue_value, $user_id))?> </td>
        	<td class="price-currency"><?=h($currency_code)?></td>
  			<td><?=$this->Html->link(__('View'), ['prefix' => 'admin', 'controller' => 'products', 'action' => 'productView', $product->id])?> </td>
        	<td>
          		<a class="stock-ico" href="<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'products', 'action' => 'edit', $product->id])?>">
                	<img src="/img/stock_icons/icon-edit.png">
          		</a>
        	</td>
        	<td>
				<a class="stock-ico" data-toggle="modal" data-backdrop="static" data-keyboard="false"
                	href="#delete-product-modal" data-param="<?=h($product->id)?>">
                	<img src="/img/stock_icons/icon-remove-stock.png">
            	</a>
        	</td>
  		</tr>
    <?php endforeach;?>
  </table>
</div>

<?php endif;?>
<?=$this->element('confirm_action_modal_with_param', ['modal_id' => 'delete-product-modal', 'modal_title' => __('Delete test'), 'modal_message' => __('Are you sure? This operation is irreversible.'), 'delete_action_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'products', 'action' => 'delete']), 'confirm_button_text' => __('Delete')])?>

<?=$this->element('img_modal')?>
