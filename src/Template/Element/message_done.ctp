<div class="icon-done-container">
    <img src = "/img/icons/icon-done.png" class="icon-done" />
</div>
<div class="row success-row justify-content-center">
    <p class="bold-done">
        <?=h($bold_message_text)?>
    </p>
</div>
<div class="row success-row justify-content-center">
    <p class="normal-done">
        <?=h($normal_message_text)?>
    </p>
</div>