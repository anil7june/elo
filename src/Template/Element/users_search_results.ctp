<div class="table-responsive">
<table class="table product-table shadow-header-table">
    <thead>
        <tr>
            <th><?=h('ID')?></th>
            <th><?=h(__('Name'))?></th>
            <th><?=h(__('Company'))?></th>
            <th><?=h(__('City'))?></th>
            <th><?=h(__('Country'))?></th>
            <th><?=h(__('Role'))?></th>
            <th><?=h(__('Status'))?></th>
            <th></th>
        </tr>
    </thead>
    <?php foreach ($users as $user): ?>
        <tr>
            <td><?=h($user->id)?></td>
            <td>
                <span class="user-name"><?=h($user->first_name . ' ' . $user->last_name)?></span><br>
                <span class="user-mail"><?=h($user->email)?></span>
            </td>
            <td><?=h($user->company)?></td>
            <td><?=h($user->city)?></td>
            <td><?=h($user->country->name)?></td>
            <td><?=h($user->role->name)?></td>
            <td><?=h($user->user_status->name)?></td>
            <td><a href="<?=h($this->Url->build(['prefix' => 'admin', 'controller' => 'users', 'action' => 'view', $user->id]))?>">Zoom User</a></td>
        </tr>
    <?php endforeach;?>
</table>
</div>