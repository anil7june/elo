<div class="modal create-modal" id="create-stock-modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <?=h(__('Create new stock'))?>
                </div>
            </div>
            <div class="modal-body">
                <?=$this->Form->create($new_stock, ['url' => ['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'create'], 'id' => 'create-stock-form'])?>
                <div class="row">
                    <div class="col-md-12">
                        <?=$this->Form->control('name', ['label' => __('Name'), 'type' => 'text']);?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?=$this->element('units/text_input', ['field_name' => 'margin', 'unit' => $this->Units->percent(), 'other_props' => ['label' => __('Margin'), 'placeholder' => '0.00']])?>
                    </div>
                    <div class="col-md-6">
                        <?=$this->element('units/text_input', ['field_name' => 'USD_margin', 'unit' => $this->Units->getCurrencySymbol($user_currency_code), 'other_props' => ['label' => __('Margin'), 'placeholder' => '0.00']])?>
                    </div>
                </div>
                <div class="row submit-row">
                    <div class="col-md-12">
                        <div class="btn-group float-right" style="height: 36px;">
                            <input type="submit" name="submit" class="btn submit-btn" value="<?=h(__('Save'))?>">
                            <button class="cancel-btn" data-dismiss="modal"><?=h(__('Cancel'))?></button>
                        </div>
                    </div>
                </div>
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
</div>