<div class="row header-row justify-content-center align-items-center">
    <div class="header-col col-md-4">
        <p class="header stock-header"><?=h(__('STOCK STATUS') . ': ')?><span class="value"><?=h($stock->stock_status->name)?></span></p>
    </div>
    <div class="header-col col-md-4">
        <p class="header stock-header"><?=h(__('SHOWING STOCK') . ': ')?><span class="value"><?=h($stock->name)?></span></p>
    </div>
    <div class="header-col col-md-4">
        <div class="btn-group float-right my-btn-group">
            <button id="create-stock-btn" data-toggle="modal" data-backdrop="static" data-keyboard="false"
                data-target="#create-stock-modal">
                <span id="create-new-stock-full-text"><?=h(__('Create new stock'))?></span>
                <span id="create-new-stock-small-res"><?=h(__('Create'))?></span>
            </button>
        </div>
    </div>
</div>
<div class="margin-15"></div>

<div class="btn-group my-btn-group">
    <?php if ($stocks): ?>
    <div class="dropdown">
        <button class="dropdown-btn btn-grey dropdown-toggle" id="dropdown-select-stock" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <?=h(__('Select stock'))?><img src="/img/stock_icons/icon-arrow-down.png">
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdown-select-stock">
            <?php foreach ($stocks as $stock_item): ?>
            <a class="dropdown-item"
                href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'select', $stock_item->id])?>">
                <?=h($stock_item->name)?>
            </a>
            <?php endforeach;?>
        </div>
    </div>
    <?php endif;?>
    <?php if ($stock->id): ?>
    <?php if ($stock_open): ?>
    <button class="common-btn small-square" data-toggle="modal" data-backdrop="static" data-keyboard="false"
        data-target="#refresh-stock-modal"><img
            src="/img/stock_icons/icon-refresh.png"><?=h(__('Refresh'))?>
    </button>
    <button class="common-btn small-square" data-toggle="modal" data-backdrop="static" data-keyboard="false"
        data-target="#delete-stock-modal"><img
            src="/img/stock_icons/icon-remove-stock.png"><?=h(__('Remove stock'))?>
    </button>
    <button class="common-btn small-square" data-toggle="modal"
        data-backdrop="static" data-keyboard="false" data-target="#close-stock-modal">
        <?=h(__('Close'))?>
    </button>
    <?php elseif ($stock_closed): ?>
        <button class="common-btn small-square" data-toggle="modal"
            data-backdrop="static" data-keyboard="false" data-target="#cancel-stock-modal">
            <?=h(__('Cancel'))?>
        </button>
    <?php elseif ($stock_cancelled || $stock_rejected): ?>
        <button class="common-btn small-square"
            onclick="location.href='<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'reopen', $stock->id])?>';">
                <?=h(__('Re-open'))?>
        </button>
    <?php endif;?>
    <button class="common-btn small-square"
        onclick="location.href='<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'export', $stock->id])?>';"><img
            src="/img/stock_icons/icon-export.png"><?=h(__('Export to PDF'))?>
    </button>
    <button class="common-btn small-square"
        onclick="location.href='<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'summary', $stock->id])?>';">
        <?=h(__('Stock stats'))?>
    </button>
    <?php endif;?>
    <?php if ($stock->id): ?>
    <div class="dropdown">
        <div class="dropdown-menu" aria-labelledby="dropdown-change-stock-status">
            <?php foreach ($stock_statuses as $stock_status): ?>
            <a class="dropdown-item"
                href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'changeStatus', $stock->id, $stock_status])?>">
                <?=h($stock_status)?>
            </a>
            <?php endforeach;?>
        </div>
    </div>
    <?php endif;?>
</div>

<?=$this->element('/stock_index/create_stock_modal')?>
<?=$this->element('confirm_modal', ['modal_id' => 'refresh-stock-modal', 'modal_title' => __('Refresh stock'), 'modal_message' => __('Are you sure? This operation is irreversible.'), 'confirm_action_url' => $this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'refresh', $stock->id]), 'confirm_button_text' => __('Refresh')])?>
<?=$this->element('confirm_modal', ['modal_id' => 'delete-stock-modal', 'modal_title' => __('Delete stock'), 'modal_message' => __('Are you sure? This operation is irreversible.'), 'confirm_action_url' => $this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'removeStock', $stock->id]), 'confirm_button_text' => __('Delete')])?>
<?=$this->element('confirm_modal', ['modal_id' => 'close-stock-modal', 'modal_title' => __('Close stock'), 'modal_message' => __('Request to admin will be send, and you will be no longer able to edit this stock. Are you sure?'), 'confirm_action_url' => $this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'close', $stock->id]), 'confirm_button_text' => __('Close stock')])?>
<?=$this->element('confirm_modal', ['modal_id' => 'cancel-stock-modal', 'modal_title' => __('Cancel stock'), 'modal_message' => __('Request to admin will be withdraw. Are you sure?'), 'confirm_action_url' => $this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'cancel', $stock->id]), 'confirm_button_text' => __('Cancel stock')])?>
