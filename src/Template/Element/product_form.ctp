<?=$this->Form->create($product_version, ['type' => 'file', 'id' => 'create-product-form'])?>
<div class="col-md-12 my-col product-form-frame">
	<div class="row product-form-header">
        <?=h(__('Product Parameters'))?>
    </div>
	<div class="row product-form-row">
		<div class="col-md-6">
			<div class="row">
				<?=$this->Form->control('cat_code', ['type' => 'text', 'label' => __('Catalyst Code'), 'value' => $product->product_version['cat_code']])?>
			</div>
			<div class="row">
				<?=$this->Form->control('make_id', ['type' => 'select', 'label' => __('Brand'), 'value' => $product->make['id'], 'options' => $makes_list, 'empty' => __('Not Chosen')])?>
			</div>
			<div class="row">
				<?=$this->Form->control('additional_ref_data', ['type' => 'text', 'label' => __('Additional Ref Data'), 'value' => $product->product_version['additional_ref_data']])?>
			</div>
			<div class="row">
				<?=$this->Form->control('description', ['type' => 'textarea', 'label' => __('Description'), 'value' => $product->product_version['description']])?>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<?=$this->Form->control('ecr_code', ['type' => 'text', 'label' => __('Catalyst ID'), 'value' => $product->product_version['ecr_code']])?>
			</div>
			<div class="row">
				<?=$this->Form->control('cat_make_id', ['type' => 'select', 'label' => __('Model'), 'value' => $product->cat_make['id'], 'options' => $cat_makes_list, 'empty' => __('Not Chosen')])?>
			</div>
			<div class="row row-half-split">
				<div class="col-md-6">
					<?=$this->element('units/text_input', ['field_name' => 'weight', 'unit' => $this->Units->kg(), 'other_props' => ['label' => __('Monolith Weight'), 'value' => $product->product_version['weight']]])?>
				</div>
				<div class="col-md-6">
					<?=$this->Form->control('product_status_id', ['type' => 'select', 'label' => __('Status'), 'value' => $product->product_status_id, 'options' => $product_statuses_list, 'empty' => __('Product Status')])?>
				</div>
			</div>
			<?php if ($mode === 'edit'): ?>
				<span>
					<div class="btn-group float-right my-btn-group">
						<a data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#delete-images-modal" data-product="<?=h($product->id)?>">
							<?=h(__('Delete Some Images'))?>
						</a>
					</div>
				</span>
			<?php endif;?>
			<?=$this->element('dotted_add_product_image', ['header_text' => __('Add Images'), 'upload_message' => __('Upload Image'), 'file_name' => "image", 'file_id' => 'image', 'no_file_message' => __('No file uploaded')])?>
		</div>
	</div>
	<div class="row product-form-row">
		<div class="col-md-12">
			<div class ="row justify-content-end">
				<div class="btn-group float-right">
					<input type="submit" class="btn submit-btn" value="<?=h($submit_text)?>"/>
				</div>
			</div>
		</div>
	</div>
</div>
<?=$this->Form->end()?>

<?php if ($mode === 'edit'): ?><?=$this->element('delete_product_images_modal')?> <?php endif;?>