<div class="input-group unity-input">
	<?php
if (!$other_props) {
    $other_props = [];
}
echo $this->Form->control($field_name, array_merge(['type' => 'text', 'aria-describedby' => 'unit_' . $field_name, 'class' => 'unit-input'], $other_props));
?>
	<span class="input-group-addon" id=<?php echo 'unit_' . $field_name ?>><?php echo $unit ?></span>
</div>