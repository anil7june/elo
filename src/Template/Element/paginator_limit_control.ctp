<form method="get" accept-charset="utf-8" role="form" style="display:inline-block;width:60px;">
    <select name="limit" onchange="this.form.submit()" id="limit">
        <?php foreach ($limits as $l): ?>
        <option value="<?=$l?>" <?=$l == $this->Paginator->param('perPage') ? 'selected="selected"' : ""?>><?=$l?>
        </option>
        <?php endforeach;?>
    </select>
    <?php foreach ($hidden as $k => $v): ?>
    <?php if ($k !== 'limit' && $k !== 'page'): ?>
    <input type="hidden" name="<?=$k?>" value<?=$v ? "=\"$v\"" : ""?>>
    <?php endif;?>
    <?php endforeach;?>
</form>