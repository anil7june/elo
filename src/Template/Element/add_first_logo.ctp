<div class="centered btn-logo-change-container">
    <div class="logo-wrapper change">
            <div id="logo-text">
                <?=h(__('Company Logo'))?>
            </div>
        <img src="" style="display:none;" id="logo-change-img" />
    </div>
</div>
<div class="centered btn-logo-change-container">
    <?=$this->element('file_upload_button', ['upload_message' => __('Add your logo'), 'file_name' => 'company_logo_file', 'file_id' => 'company-logo-file'])?>
</div>