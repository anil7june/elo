<?php
$temp = parse_url($this->request->here());
parse_str($temp['query'], $params);
unset($params['page']);
?>

<input type="hidden" id="prev" value="<?php
if ($this->Paginator->params()['page'] > 1) {
    echo h($this->Paginator->prev());
} else {
    echo "begin";
}
?>">
<input type="hidden" id="next" value="<?php
if ($this->Paginator->params()['page'] < $this->Paginator->params()['pageCount']) {
    echo h($this->Paginator->next());
} else {
    echo "end";
}?>">

<div>
    <a href=" #" id="prev-link"><img class="paginator-arrow" src="/img/icons/icon-arrow-left.png"></a>
    <span><?=h($this->Paginator->params()['page'] . ' ' . __('of') . ' ' . $this->Paginator->params()['pageCount'])?></span>
    <a href=" #" id="next-link"><img class="paginator-arrow"  src="/img/icons/icon-arrow-right.png"></a>
</div>


<script>
$(document).ready(function() {
    var prev = $('#prev').val();
    if (prev !== "begin") {
        var prev_url = prev.split('href=\"')[1].split('\">')[0].replace(/amp;/g, '');
    } else {
        var prev_url = "#";
    }

    var next = $('#next').val();
    if (next !== "end") {
        var next_url = next.split('href=\"')[1].split('\">')[0].replace(/amp;/g, '');
    } else {
        var next_url = "#";
    }

    $('#prev-link').attr("href", prev_url);
    $('#next-link').attr("href", next_url);
});
</script>