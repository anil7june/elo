<span class="frame-label"><?=h($header_text)?></span>
<div class="dashed-frame">
	<div class="row">
		<div class="col-md-6">
			<?=$this->element('file_upload_button', ['upload_message' => $upload_message, 'file_name' => $file_name, 'file_id' => $file_id])?>
		</div>
		<div class="col-md-6 align-self-center text-center">
			<span class="image-path-txt" id="no-photo-uploaded">
				<?=h($no_file_message)?>
			</span>
			<span class="image-path-txt hide" id="photo-path">
			</span>
			<a href="#" class="hide" id="close-ico"><img class="close-icon" src="/img/icons/icon-close-black.png"/></a>
		</div>
    </div>
</div>

<script>
    $('.dashed-frame').change(function(event) {
		let file_name = event.target.files[0].name;
		$('#no-photo-uploaded').addClass('hide');
		$('#photo-path').text(file_name);
		$('#photo-path').removeClass('hide');
		$('#close-ico').removeClass('hide');
    });
	$('#close-ico').click(function() {
		$("#<?=h($file_id)?>").val("");
		$('#photo-path').addClass('hide');
		$('#close-ico').addClass('hide');
		$('#no-photo-uploaded').removeClass('hide');
	});
</script>