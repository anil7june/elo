<div class="search-input-margin"></div>
<?=$this->element('header_with_back', ['back_url' => $prev_location_url, 'header_text' => __("PRODUCT VIEW"), 'back_link_text' => __('Back to list')])?>
<div class="row product-view-row">
    <div class="col-md-6 my-col no-border">
        <div class="product-photo-container">
            <?=$this->element('product_image_galery', ['product_id' => $product->id])?>
        </div>
        <div class="product-description-wrapper">
            <span class="product-description"><?=h($product->product_version['description'])?></span>
        </div>
    </div>
    <div class="col-md-6 my-col product-details-col">
        <div class="product-details-container">
            <div class="row product-details-row-header">
                <p><?=h(__('Product details'))?></p>
            </div>
        	<?=$this->element('product_table_view')?>
        </div>
    </div>
</div>