<div class="paginator-style">
    <div class="left">
        <?=h($this->Paginator->params()['start'] . ' - ' . $this->Paginator->params()['end'] . __(' from ') . $this->Paginator->params()['count'])?>
    </div>
    <div class="right">
        <?=$this->element('paginator_menu')?>
    </div>
</div>
<div class="table-responsive">
    <table class="table product-table shadow-header-table">
        <thead>
            <tr>
                <th><?=h('ID')?></th>
                <th class="text-center"><?=h(__('Owner'))?></th>
                <th><?=h(__('Name'))?></th>
                <th colspan="2"><?=h(__('Catalogue value'))?></th>
                <th colspan="2"><?=h(__('Value saved'))?></th>
                <th colspan="2"><?=h(__('Total Profit'))?></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <?=$this->Form->control('action_url', ['value' => $this->Url->build(['prefix' => 'admin', 'controller' => 'stocks', 'action' => 'getStockDetails'], true), 'type' => 'hidden'])?>
        <?php foreach ($stocks as $stock): ?>
            <tr id="stock-<?=$stock->id?>-header-row">
                <td><?=h($stock->id)?></td>
                <td class="text-center">
                    <span class="user-name"><?=h($stock->user->first_name . ' ' . $stock->user->last_name)?></span><br>
                    <span class="user-mail"><?=h($stock->user->email)?></span>
                </td>
                <td><?=h($stock->name)?></td>
                <td class="text-right"><?=h($this->ProductFormat->formatNumber($stock['total_catalogue_value'], $user_id))?></td>
                <td class="text-left"><?=h($currencyCode)?></td>
                <td class="text-right"><?=h($this->ProductFormat->formatNumber($stock['total_user_value'], $user_id))?></td>
                <td class="text-left"><?=h($currencyCode)?></td>
                <td class="text-right"><?=h($this->ProductFormat->formatNumber($stock['total_profit_value'], $user_id))?></td>
                <td class="text-left"><?=h($currencyCode)?></td>
                <td>
                    <button class="transparent-btn" data-toggle="modal" data-backdrop="static" data-keyboard="false"
                        data-target="#accept-stock-modal" data-param="<?=h($stock->id)?>">
                        <?=h(__('Accept'))?>
                    </button>
                </td>
                <td>
                    <button class="transparent-btn" data-toggle="modal" data-backdrop="static" data-keyboard="false"
                        data-target="#reject-stock-modal" data-param="<?=h($stock->id)?>">
                        <?=h(__('Reject'))?>
                    </button>
                </td>
                <td>
                    <a href="#" class="toggle-link" id="<?=h($stock->id)?>-arrow-up"><img class="users-stocks-icon" src="/img/icons/icon-arrow-down-users-stocks.png" /></a>
                    <a href="#" class="toggle-link hide" id="<?=h($stock->id)?>-arrow-down"><img class="users-stocks-icon" src="/img/icons/icon-arrow-up-users-stocks.png" /></a>
                </td>
            </tr>
            <tr class="manage-users-stocks-row hide" id="stock-<?=h($stock->id)?>-details">
                <td class="manage-users-stocks-td" colspan="13">
                    <?=$this->element('stock_info_only_table', ['stock_id' => $stock->id])?>
                    <?=$this->element('stock_summary_table', ['stock_id' => $stock->id])?>
                </td>
            </tr>
        <?php endforeach;?>
    </table>
</div>

<?=$this->element('confirm_action_modal_with_param', [
    'modal_id' => 'accept-stock-modal',
    'modal_title' => __("Accept user's  stock"),
    'modal_message' => __('Are you sure you want to accept this stock?'),
    'delete_action_url' => $this->Url->build([
        'prefix' => 'admin',
        'controller' => 'stocks',
        'action' => 'acceptStock',
    ]),
    'confirm_button_text' => __('Accept'),
])?>

<?=$this->element('confirm_action_modal_with_param', [
    'modal_id' => 'reject-stock-modal',
    'modal_title' => __("Reject user's  stock"),
    'modal_message' => __('Are you sure you want to reject this stock?'),
    'delete_action_url' => $this->Url->build([
        'prefix' => 'admin',
        'controller' => 'stocks',
        'action' => 'rejectStock',
    ]),
    'confirm_button_text' => __('Reject'),
])?>

<?=$this->Html->script('tables/stocks_table_manager.js')?>