<span class="frame-label"><?=h($header_text)?></span>
<span>
  <a id="add-more-images" href="#"><?=h(__('Add more images'))?></a>
</span>
<div class="dashed-frame" id="add-file-frame">
  <div class="row" id="file-inputs-data">
      <div class="row">
        <div class="col-md-10">
          <input class="multi-images-input" type="file" name="file_0" id="file-0"/>
        </div>
        <div class="col-md-2">
          <img src="/img/stock_icons/icon-remove-stock.png" class="test-ico image-ico clickable">
        </div>
      </div>
  </div>
</div>


<?=$this->Html->script('element/dotted_add_product_image.js')?>