<p>Hi, <?=$user->name?></p>
<p>This email is to notify you that your account has been deactivated.</p>
<p>For more info, please contact your admin.</p>
<p>Kind regards,<br>Elemental</p>
