<p><?=h(__('This email is to notify you that a new user has requested access to the product catalogue and requires approval. To accept this user, simply go to
manage users in your dashboard and just activate user with given email.'))?></p>
<p>
    <strong><?=h(__('Name') . ': ')?>:</strong> <?=h($user->first_name . ' ' . $user->last_name)?><br>
    <strong><?=h(__('Email') . ': ')?>:</strong> <?=$user->email?>
</p>
<p><?=h(__('Kind regards') . ', ')?><br><?=h(__('Elemental'))?></p>
