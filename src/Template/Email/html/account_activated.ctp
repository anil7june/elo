<p><?=h(__('Hi') . ', ')?><?=$user->name?></p>
<p><?=h(__('This email is to notify you that your account has been made active on the Elemental Web Application.'))?></p>
<p><?=h(__('To login, simply head over to '))?><a href='<?=$this->Url->build('/', true)?>'><?=h($this->Url->build('/', true))?></a><?=h(__(' and sing in with the details you created.'))?></p>
<p><?=h(__('Kind regards') . ', ')?><br><?=h(__('Elemental'))?></p>
