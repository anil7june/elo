<p><?=h(__('Hi'))?><?=$user->name?></p>
<p><?=h(__('As you have requested for reset password instructions, here they are, please follow the URL') . ': ')?> </p>
<p><a href='<?=h($recovery_url)?>'><?=h($recovery_url)?></a> </p>
<p><?=h(__('Kind regards') . ', ')?><br><?=h(__('Elemental'))?></p>
