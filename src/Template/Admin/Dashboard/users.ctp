<?=$this->element('header_with_back',
    [
        'back_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'dashboard', 'action' => 'index']),
        'back_link_text' => __('Back to dashboard'),
        'header_text' => __('MANAGE USERS'),
    ])?>
<div class="search-input-margin"></div>

<div class="row dashboard-row">
    <div class="my-col col-md-4 right-border">
        <div class="row justify-content-center">
            <img class="usr-ico" id="users-list" src="/img/dashboard_icons/users-lists.png"/>
        </div>
        <div class="row justify-content-center">
            <p class="txt-header"><?=h(__('Users List'))?></p>
        </div>
        <div class="row justify-content-center">
            <button class="btn btn-orange-transp" onclick="location.href='<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'users', 'action' => 'index'])?>'"><?=h(__('Manage'))?></button>
        </div>
    </div>
    <div class="my-col col-md-4 right-border">
        <div class="row justify-content-center">
            <img class="usr-ico" id="users-stocks" src="/img/dashboard_icons/users-stocks.png"/>
        </div>
        <div class="row justify-content-center">
            <p class="txt-header"><?=h(__('Users Stocks'))?></p>
        </div>
        <div class="row justify-content-center">
            <button class="btn btn-orange-transp" onclick="location.href='<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'stocks', 'action' => 'index'])?>'"><?=h(__('Manage'))?></button>
        </div>
    </div>
    <?php if ($user_rights_permissions): ?>
    <div class="my-col col-md-4 right-border">
        <div class="row justify-content-center">
            <img class="usr-ico" id="users-rights" src="/img/dashboard_icons/users-rights.png"/>
        </div>
        <div class="row justify-content-center">
            <p class="txt-header"><?=h(__('Users Rights'))?></p>
        </div>
        <div class="row justify-content-center">
            <button class="btn btn-orange-transp" onclick="location.href='<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'roles', 'action' => 'index'])?>'"><?=h(__('Manage'))?></button>
        </div>
    </div>
    <?php endif;?>
</div>
