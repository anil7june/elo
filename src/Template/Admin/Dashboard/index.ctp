<!-- File: src/Template/Admin/Dashboard/index.ctp -->

<div class="row dashboard-row">
  <div class="my-col col-md-3">
    <div class="row justify-content-center">
      <img class="block-ico" src="/img/dashboard_icons/manage-users.png"/>
    </div>
    <div class="row justify-content-center">
      <p class="txt-header"><?=h(__('Users'))?></p>
    </div>
    <div class="row justify-content-center">
      <button class="btn btn-orange-transp" onclick="location.href='<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'dashboard', 'action' => 'users'])?>'"><?=h(__('Manage'))?></button>
    </div>
  </div>
  <div class="my-col col-md-3">
    <div class="row justify-content-center">
      <img class="block-ico" src="/img/dashboard_icons/manage-products.png"/>
    </div>
    <div class="row justify-content-center">
      <p class="txt-header"><?=h(__('Products'))?></p>
    </div>
    <div class="row justify-content-center">
      <button class="btn btn-orange-transp" onclick="location.href='<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'products', 'action' => 'index'])?>'"><?=h(__('Manage'))?></button>
    </div>
  </div>
  <div class="my-col col-md-3">
    <div class="row justify-content-center">
      <img class="block-ico" src="/img/dashboard_icons/manage-currencies.png"/>
    </div>
    <div class="row justify-content-center">
      <p class="txt-header"><?=h(__('Currencies'))?></p>
    </div>
    <div class="row justify-content-center">
      <button class="btn btn-orange-transp" onclick="location.href='<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'currencies', 'action' => 'index'])?>'"><?=h(__('Manage'))?></button>
    </div>
  </div>
  <div class="my-col col-md-3">
    <div class="row justify-content-center">
      <img class="block-ico" src="/img/dashboard_icons/manage-PGM-prices.png"/>
    </div>
    <div class="row justify-content-center">
      <p class="txt-header"><?=h(__('PGM Prices'))?></p>
    </div>
    <div class="row justify-content-center">
      <button class="btn btn-orange-transp" onclick="location.href='<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'PgmPrices', 'action' => 'index'])?>'"><?=h(__('Manage'))?></button>
    </div>
  </div>
</div>
