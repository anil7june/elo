

<h3><?= h(__('Assign test to product')) ?></h3>

<div class="table-responsive">
<table class="greyRows table">
  <?= $this->Form->create('test_form', ['class' => 'form-inline']); ?>
  <tr>
    <th><h5><?= h(__('Assay Provider')) ?></h5></th>
    <th><h5><?= h('Pt') ?></h5></th>
    <th><h5><?= h('Pd') ?></h5></th>
    <th><h5><?= h('Rh') ?></h5></th>
    <th></th>
  </tr>
  <tr>
    <th>
      <?= $this->Form->control('analysis_type_id', [
        'type' => 'select',
        'options' => $analysis_type_list,
        'empty' => 'select one',
        'label' => '']); ?>
    </th>
    <th>
      <?= $this->Form->control('pt', [
        'class' => 'form-control input-lg',
        'label' => ''
      ]); ?>
    </th>
    <th>
      <?= $this->Form->control('pd', [
        'class' => 'form-control input-lg',
        'label' => ''
      ]); ?>
    </th>
    <th>
      <?= $this->Form->control('rh', [
        'class' => 'form-control input-lg',
        'label' => ''
      ]); ?>
    </th>
    <th>
      <?= $this->Form->submit(__('Add Test')); ?>
    </th>
  </tr>
</table>
</div>
