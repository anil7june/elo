<!-- File: src/Template/Dashboard/Users/index.ctp -->

<?=$this->element('header_with_back', ['back_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'dashboard', 'action' => 'users']), 'header_text' => __('USERS LIST'), 'back_link_text' => __('Back')])?>
<?=$this->Form->create('filter', ['type' => 'get'])?>
<div class="row user-list-filter-row">
    <div class="col-md-3">
        <?=$this->Form->control('email', ['label' => '', 'type' => 'email', 'value' => $filterValues['email'], 'placeholder' => __('User E-mail')]);?>
    </div>
    <?php if ($if_user_is_super_admin): ?>
    <div class="col-md-2">
        <?=$this->Form->control('company', ['label' => '', 'text' => 'select', 'value' => $filterValues['company'], 'placeholder' => __('Company Name'), 'options' => $companies_list]);?>
    </div>
    <?php endif;?>
    <div class="col-md-2">
        <?=$this->Form->control('city', ['label' => '', 'type' => 'text', 'value' => $filterValues['city'], 'placeholder' => __('City Name')]);?>
    </div>
    <div class="col-md-2">
        <?=$this->Form->control('country_id', ['label' => '', 'type' => 'select', 'options' => $countries_list, 'empty' => __('Country'), 'value' => $filterValues['country_id']]);?>
    </div>
    <?php if ($if_user_is_super_admin): ?>
    <div class="col-md-2">
        <?=$this->Form->control('role_id', ['type' => 'select', 'options' => $roles_list, 'label' => '', 'empty' => __('User Role'), 'value' => $filterValues['role_id']]);?>
    </div>
    <?php endif;?>
    <div class="col-md-2 ">
        <?=$this->Form->control('user_status_id', ['type' => 'select', 'options' => $statuses_list, 'label' => '', 'empty' => __('User Status'), 'value' => $filterValues['user_status_id']]);?>
    </div>
    <div class="col-md-1">
        <input type="submit" id="user-filter-submit" class="btn submit-btn" value="<?=h(__('Search'))?>"/>
    </div>
</div>
<?=$this->Form->input('filter', ['type' => 'hidden', 'value' => true])?>
<?=$this->Form->end()?>
<div class="search-input-margin"></div>
<?php if (!$empty_results): ?>
<div class="paginator-style">
    <div class="left">
        <?=h($this->Paginator->params()['start'] . ' - ' . $this->Paginator->params()['end'] . __(' from ') . $this->Paginator->params()['count'])?>
    </div>
    <div class="right">
        <?=$this->element('paginator_menu')?>
    </div>
</div>
<?=$this->element('users_search_results')?>
<?php endif;?>
<?php if ($empty_results): ?>
<?=$this->element('search_results_not_found', ['message' => __('No user matches your search criteria.')])?>
<?php endif;?>