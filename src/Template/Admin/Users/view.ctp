<!-- File: src/Template/Dashboard/Users/view.ctp -->

<?=$this->element('header_with_back', ['back_url' => $prev_location, 'header_text' => __('USER') . ' ' . $user->first_name . ' ' . $user->last_name, 'back_link_text' => __('Back to Users List')])?>
<?=$this->Form->create($user, ['id' => 'edit-user-form'])?>
<div class="row user-info-row">
	<div class="col-md-4 my-col">
		<div class="form-container">
			<div class="row header">
				<p><?=h(__('User Details'))?></p>
			</div>
			<div class="row">
				<table id="user-details-table" class="table product-table info-table shadow-header-table user-view-table">
					<tr class="header">
						<td class="bold-content "><?=h(__('Name') . ':')?></td>
						<td class="normal-content text-right"><?=h($user->first_name . ' ' . $user->last_name)?></td>
					</tr>
					<tr>
						<td class="bold-content "><?=h(__('Email') . ':')?></td>
						<td class="normal-content text-right"><?=h($user->email)?></td>
					</tr>
					<tr>
						<td class="bold-content "><?=h(__('Company') . ':')?></td>
						<td class="normal-content text-right"><?=h($user->company)?></td>
					</tr>
					<tr>
						<td class="bold-content "><?=h(__('City') . ':')?></td>
						<td class="normal-content text-right"><?=h($user->city)?></td>
					</tr>
					<tr>
						<td class="no-border bold-content "><?=h(__('Country') . ':')?></td>
						<td class="no-border normal-content text-right"><?=h($user->country->name)?>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="my-col col-md-8">
		<div class="form-container">
			<div class="row header">
				<p><?=h(__('Element Pricing'))?></p>
			</div>
			<div class="row">
				<table class="table product-table info-table shadow-header-table user-view-table" id="pricing-details">
					<tr class="header">
						<td class="border-bottom border-right"></td>
						<td class="border-bottom bold-content text-centered"><?=h(__('Pt'))?></td>
						<td class="border-bottom bold-content text-centered"><?=h(__('Pd'))?></td>
						<td class="border-bottom bold-content text-centered"><?=h(__('Rh'))?></td>
					</tr>
					<tr>
						<td class="border-right border-bottom bold-content text-left"><?=h(_('Price discount') . ':')?></td>
						<td class="border-bottom text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'pt_PD', 'unit' => $this->Units->dolarToz()])?>
						</td>
						<td class="border-bottom text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'pd_PD', 'unit' => $this->Units->dolarToz()])?>
						</td>
						<td class="border-bottom text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'rh_PD', 'unit' => $this->Units->dolarToz()])?>
						</td>
					</tr>
					<tr>
						<td class="border-right border-bottom bold-content text-left"><?=h(_('Recovery rate') . ':')?></td>
						<td class="border-bottom normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'pt_RR', 'unit' => $this->Units->percent()])?>
						</td>
						<td class="border-bottom normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'pd_RR', 'unit' => $this->Units->percent()])?>
						</td>
						<td class="border-bottom normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'rh_RR', 'unit' => $this->Units->percent()])?>
						</td>
					</tr>
					<tr>
						<td class="border-right border-bottom bold-content text-left"><?=h(_('Refining charge') . ':')?></td>
						<td class="border-bottom normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'pt_RC', 'unit' => $this->Units->dolarToz()])?>
						</td>
						<td class="border-bottom normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'pd_RC', 'unit' => $this->Units->dolarToz()])?>
						</td>
						<td class="border-bottom normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'rh_RC', 'unit' => $this->Units->dolarToz()])?>
						</td>
					</tr>
					<tr>
						<td class="border-right border-bottom bold-content text-left"><?=h(__('Leasing charge') . ':')?></td>
						<td class="border-bottom normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'pt_LC', 'unit' => $this->Units->percent()])?>
						</td>
						<td class="border-bottom normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'pd_LC', 'unit' => $this->Units->percent()])?>
						</td>
						<td class="border-bottom normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'rh_LC', 'unit' => $this->Units->percent()])?>
						</td>
					</tr>
					<tr>
						<td class="border-right no-border bold-content text-left"><?=h(__('Leasing days') . ':')?></td>
						<td class="no-border normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'pt_LD', 'unit' => 'days'])?>
						</td>
						<td class="no-border normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'pd_LD', 'unit' => 'days'])?>
						</td>
						<td class="no-border normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'rh_LD', 'unit' => 'days'])?>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="row user-info-row">
	<div class="col-md-4 my-col">
		<div class="form-container">
			<div class="row header">
				<p><?=h(__('System Parameters'))?></p>
			</div>
			<div class="row">
				<table class="table product-table info-table shadow-header-table user-view-table" id="edit-user-details">
					<tr class="header">
						<td class="bold-content text-left"><?=h(__('Role') . ':')?></td>
						<td class="text-right"><?=$this->Form->control('role_id', ['type' => 'select', 'label' => '', 'options' => $roles])?></td>
					</tr>
					<tr>
						<td class="bold-content text-left"><?=h(__('Status') . ':')?></td>
						<td class="text-right"><?=$this->Form->control('user_status_id', ['type' => 'select', 'label' => '', 'options' => $user_statuses])?></td>
					</tr>
					<tr>
						<td class="bold-content text-left"><?=h(__('Amount of Coins') . ':')?></td>
						<td class="text-right"><?=$this->Form->control('coins', ['type' => 'text'])?></td>
					</tr>
					<tr>
						<td class="bold-content no-border text-left"><?=h(__('Allow custom logo') . ':')?></td>
						<td class="text-right no-border"><?=$this->Form->control('logo_allowed', ['type' => 'checkbox', 'value' => 1, 'label' => ['text' => '<p>' . '</p>', 'escape' => false], 'checked' => $user->logo_allowed === 1 ? true : false])?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="my-col col-md-8" id="general-pricing-col">
		<div class="form-container">
			<div class="row header">
				<p><?=h(__('General Pricing'))?></p>
			</div>
			<div class="row">
				<table class="table product-table info-table shadow-header-table user-view-table" id="pricing-terms">
					<tr class="header">
						<td class="border-bottom border-right bold-content text-left"><?=h(__('Treatment charge') . ':')?></td>
						<td class="border-bottom normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'TC', 'unit' => $this->Units->dolarKg()])?>
						</td>
					</tr>
					<tr>
						<td class="border-bottom border-right bold-content text-left"><?=h(__('Discount') . ':')?></td>
						<td class="border-bottom normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'DU_pp', 'unit' => $this->Units->percent()])?>
						</td>
					</tr>
					<tr>
						<td class="no-border border-right bold-content text-left"><?=h(__('Discount (USD)') . ':')?></td>
						<td class="no-border normal-content text-centered">
							<?=$this->element('units/text_input', ['field_name' => 'DU_value', 'unit' => $this->Units->dolar()])?>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="btn-group float-right">
    <button type="button" onclick="location.href='<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'users', 'action' => 'view', $user->id])?>'" id="discard-changes" class="btn white-btn"><?=h(__('Discard the changes'))?></button>
    <input type="submit" class="btn submit-btn" value="<?=h(__('Save changes'))?>"/>
</div>
<?=$this->Form->end()?>

<?=$this->Html->script('admin/users/view.js')?>
