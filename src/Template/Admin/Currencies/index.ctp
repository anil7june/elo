<!-- File: src/Template/Dashboard/Currencies/index.ctp -->

<?=$this->element('header_with_back', ['back_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'dashboard', 'action' => 'index']), 'header_text' => __('CURRENCIES'), 'back_link_text' => __('Back to Dashboard')])?>
<div class="search-input-margin"></div>
<div class="display-3-cols-container">
	<?=$this->Form->create('update-currencies', ['id' => 'update-currencies'])?>
	<table class="table product-table info-table shadow-header-table user-view-table display-3-cols" id="currencies-table">
		<tr class="header">
			<td class="border-bottom border-right"></td>
			<td class="border-bottom text-centered bold-content border-right"><?=h(__('Name'))?></td>
			<td class="border-bottom text-centered bold-content"><?=h(__('Exchange Rate (relative to USD)'))?></td>
		</tr>
		<?php foreach ($rated_currencies as $currency): ?>
    		<tr>
				<td class="border-bottom text-centered bold-content border-right"><?=h($currency->code)?></td>
				<td class="border-bottom text-centered normal-content border-right"><?=h($currency->name)?></td>
				<td class="border-bottom text-centered normal-content rated" id="<?=h($currency->id)?>-col">
					<?=$this->Form->control($currency->id, ['type' => 'text', 'value' => $currency->rate['exchange_rate']])?>
				</td>
    		</tr>
  		<?php endforeach;?>
	</table>
	<div class="search-input-margin"></div>
	<div class="btn-group float-right">
    	<button type="button" onclick="location.href='<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'Currencies', 'action' => 'index'])?>'" id="discard-changes" class="btn white-btn"><?=h(__('Discard the changes'))?></button>
    	<input type="submit" class="btn submit-btn" value="<?=h(__('Save'))?>"/>
	</div>
	<?=$this->Form->end()?>
</div>
<?=$this->Html->script('admin/currencies/index.js')?>