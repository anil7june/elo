<!-- File: src/Template/Admin/Roles/index.ctp -->

<?=$this->element('header_with_back', ['back_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'dashboard', 'action' => 'users']), 'header_text' => __('ROLES'), 'back_link_text' => __('Back')])?>
<div class="roles-table-container">
<table class="table product-table info-table shadow-header-table user-view-table display-3-cols" id="roles-table">
	<?php foreach ($roles as $role): ?>
	<tr class="header">
		<td class="border-right text-left"><?=h($role->name)?></td>
		<td class="text-centered">
			<a class="stock-ico" href="<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'roles', 'action' => 'edit', $role->id])?>">
                <img src="/img/stock_icons/icon-edit.png">
            </a>
		</td>
	</tr>
<?php endforeach;?>
</table>
</div>
