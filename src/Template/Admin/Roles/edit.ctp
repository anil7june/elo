<!-- File: src/Template/Admin/Roles/edit.ctp -->

<?=$this->element('header_with_back', ['back_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'roles', 'action' => 'index']), 'header_text' => __('EDITTING') . ' ' . $role->name, 'back_link_text' => __('Back to Roles')])?>

<?=$this->Form->create('Edit Role Form')?>

<div class="row">
<div class="col-md-6 edit-role">
<table class="table product-table info-table shadow-header-table user-view-table display-3-cols" id="edit-role-table">
<?php foreach ($permission_list_1 as $permission): ?>
    <tr>
      	<td><?=h($permission->name)?></td>
      	<td>
        	<?=$this->Form->control($permission->id, ['type' => 'checkbox', 'value' => $permission->id, 'label' => ['text' => '<p>' . '</p>', 'escape' => false], 'checked' => $role_permissions[$permission->id] === true ? true : false])?>
      	</td>
    </tr>
  <?php endforeach;?>
</table>
</div>
<div class="col-md-6 edit-role">
<table class="table product-table info-table shadow-header-table user-view-table display-3-cols" id="edit-role-table">
<?php foreach ($permission_list_2 as $permission): ?>
    <tr>
      	<td><?=h($permission->name)?></td>
      	<td>
        	<?=$this->Form->control($permission->id, ['type' => 'checkbox', 'value' => $permission->id, 'label' => ['text' => '<p>' . '</p>', 'escape' => false], 'checked' => $role_permissions[$permission->id] === true ? true : false])?>
      	</td>
    </tr>
  <?php endforeach;?>
</table>
</div>
</div>
<div class="float-right">
	<input type="submit" style="margin-right: .3rem; margin-top: .8rem;" class="btn submit-btn" value="<?=h(__('Save the changes'))?>"/>
</div>
<?=$this->Form->end()?>
<div class="nav-bar-margin"></div>
