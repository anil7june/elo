<!-- File: src/Template/Admin/PgmPrices/index.ctp -->
<?=$this->element('header_with_back', ['back_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'dashboard', 'action' => 'index']), 'header_text' => __('PGM PRICES'), 'back_link_text' => __('Back to Dashboard')])?>
<div class="search-input-margin"></div>
<div class="display-3-cols-container">
	<?=$this->Form->create('pgm_prices', ['url' => ['action' => 'updateDiscounts'], 'id' => 'update-pgm-prices-form'])?>
	<table class="table product-table info-table shadow-header-table user-view-table display-3-cols" id="pgm-prices-table">
		<tr class="header">
			<td class="border-bottom border-right"></td>
			<td class="border-bottom text-centered bold-content border-right"><?=h(__('Price'))?></td>
			<td class="border-bottom text-centered bold-content"><?=h(__('Discount'))?></td>
		</tr>
			<?php foreach ($elements as $element): ?>
    		<tr>
					<td class="border-bottom text-centered bold-content border-right"><?=h($element)?></td>
							<td class="border-bottom text-centered normal-content border-right">
								<?=$this->element('units/text_input', ['field_name' => $element . '_price', 'unit' => $this->Units->dolarToz(), 'other_props' => ['value' => $prices[$element], 'label' => ' ']])?></td>
							<td class="border-bottom text-centered normal-content">
								<?=$this->element('units/text_input', ['field_name' => $element, 'unit' => $this->Units->dolarToz(), 'other_props' => ['value' => $discounts[$element], 'label' => ' ']])?></td>
    		</tr>
  		<?php endforeach;?>
	</table>
	<div class="search-input-margin"></div>
	<div class="btn-group float-right">
    	<button type="button" onclick="location.href='<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'PgmPrices', 'action' => 'index'])?>'" id="discard-changes" class="btn white-btn"><?=h(__('Discard the changes'))?></button>
    	<input type="submit" class="btn submit-btn" value="<?=h(__('Save'))?>"/>
	</div>
	<?=$this->Form->end()?>
</div>

<?=$this->Html->script('admin/pgm_prices/index.js')?>