<!-- File: src/Template/Dashboard/Coins/index.ctp -->
<?php echo $this->Html->script('projectScripts/coin_balances_form'); ?>

<h1><?= h(__('Coins Balance')) ?></h1>

<?= $this->Form->create('Search Filter', ['type' => 'get']); ?>
<div class="table-responsive">
<table class="table">
  <thead>
    <th width="3%"></th>
    <th width="15%"><h5><?= h(__('User Name')) ?></h5></th>
    <th width="15%"><h5><?= h(__('Email Address')) ?></h5></th>
    <th width="15%"><h5><?= h(__('Country')) ?></h5></th>
    <th width="5%"></th>
  </thead>
  <tr>
    <td></td>
    <td>
      <?= $this->Form->control('user_id', [
        'type' => 'select',
        'options' => $users_list,
        'label' => ' ',
        'empty' => __('Choose User Name'),
        'style' => 'width:250px;',
        'value' => $filterValues['user_id']
      ]); ?>
    </td>
    <td>
      <?= $this->Form->control('email', [
        'label' => '',
        'type' => 'email',
        'style' => 'width:250px;',
        'value' => $filterValues['email']
      ]); ?>
    </td>
    <td>
      <?= $this->Form->control('country_id', [
        'label' => '',
        'type' => 'select',
        'options' => $countries_list,
        'empty' => __('Choose Country'),
        'style' => 'width:250px;',
        'value' => $filterValues['country_id']
      ]); ?>
    </td>
    <td><?= $this->Form->submit(__('Filter'), ['name' => 'filter']) ?></td>
  </tr>
</table>
<?= $this->Form->end(); ?>
</div>

  <ul class="myPagination myPaginationLeft">
    <?= $this->element("paginator_menu");?>
  </ul>
  <ul class="myPagination myPaginationRight">
    <li><?= h(__('Results per page'))?></li>
    <li><?= $this->element('paginator_limit_control', ['limits' => [25, 50, 100], 'hidden' => $filterValues])?></li>
  </ul>

  <div class="table-responsive">
<table class="greyRows adminCoinTable table">
  <thead>
  <tr>
    <th width="3%"></th>
    <th width="15%"><h5><?= h(__('User ID')) ?></h5></th>
    <th width="15%"><h5><?= h(__('User Name')) ?></h5></th>
    <th width="20%"><h5><?= h(__('Email Address')) ?></h5></th>
    <th width="15%"><h5><?= h(__('Country')) ?></h5></th>
    <th width="10%"><h5><?= h(__('Coins')) ?></h5></th>
    <th width="15%"></th>
  </tr>
</thead>

  <?php foreach ($users as $user): ?>
    <tr class="admin_coins_row">
      <td></td>
      <td><?= h($user->id) ?></td>
      <td><?= h($user->name) ?></td>
      <td><?= h($user->email) ?></td>
      <td><?= h($user->country->name) ?></td>
      <td>
        <?= $this->Form->control('coins_' . $user->id, ['label' => '', 'type' => 'number', 'value' => $user->coins]) ?>
      </td>
      <td>
        <input type="button" class="btn btn-default smallButton" value=<?= __('Update');?> onclick="sendPostData(
        '<?= h('coins-' . $user->id) ?>',
        '<?= $this->Url->build([
          'prefix' => 'admin',
          'controller' => 'coins',
          'action' => 'editCoinBalance',
          $user->id
          ]) ?>'
        )" >
      </td>
    </tr>
  <?php endforeach; ?>
</table>
</div>
