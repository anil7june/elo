<?=$this->element('header', ['header_text' => __('CLOSED STOCKS')])?>

<?php if ($stocks->count() == 0): ?>
<?=$this->element('search_results_not_found', ['message' => __('No stock awaits your approval')])?>
<?php else: ?>
<?=$this->element('stocks_to_accept_table')?>
<?php endif;?>
<script>
    $('.toggle-link').each(function() {
        $(this).click(function() {
            let stock_id = stockId($(this));
            let action_url = $('#action-url').val();
            if(!dataInserted(stock_id)) {
                insertData(action_url, stock_id);
            }
            toggleStockInfoDOM(stock_id);
        });
    });

</script>