<!-- File: src/Template/Dashboard/Stocks/index.ctp -->

<?=$this->element('header_with_back', ['back_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'dashboard', 'action' => 'users']), 'header_text' => __('USERS STOCKS'), 'back_link_text' => __('Back')])?>
<?=$this->Form->create('search_filter', ['type' => 'get', 'id' => 'search-filter'])?>
<div class="row users-stocks-filter-row">
    <div class="col-md-3">
        <?=$this->Form->control('owner', ['type' => 'text', 'label' => '', 'placeholder' => 'Enter an Owner'])?>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <input id="date-from" name="date_from" placeholder="<?=h(__('Date from'))?>" name="date-from"/>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <input class="date" id="date-to" name="date_to" placeholder="<?=h(__('Date to'))?>" name="date-to"/>
        </div>
    </div>
    <div class="col-md-2">
        <?=$this->Form->control('stock_status_id', ['type' => 'select', 'label' => '', 'options' => $stockStatuses, 'empty' => __('Status'), 'value' => $filterValues['stock_status_id']])?>
    </div>
    <div class="col-md-2">
        <input type="submit" id="users-stocks-filter-submit" class="btn submit-btn" value="<?=h(__('Search'))?>"/>
    </div>
</div>
<?=$this->Form->end()?>
<?php if ($stocks->count() > 0): ?>
    <?=$this->element('stocks_search_results')?>
<?php else: ?>
    <div class="search-input-margin">
    <?=$this->element('search_results_not_found', ['message' => __('No stock meets your search criteria.')])?>
<?php endif;?>

<?=$this->Html->script('admin/stocks/index.js')?>