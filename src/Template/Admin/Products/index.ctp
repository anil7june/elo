<!-- File: src/Template/Admin/Products/index.ctp -->
<?=$this->element('header_with_back', ['back_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'dashboard', 'action' => 'index']), 'header_text' => __('MANAGE PRODUCTS'), 'back_link_text' => __('Back to Dashboard')])?>
<div class="btn-group my-btn-group">
  	<a data-toggle="modal" href="#import-data-modal">
    	<button class="common-btn"><?=h(__('Import From CSV'))?></button>
  	</a>
  	<a href="#">
    	<button class="common-btn" id="export-to-csv">
      		<?=h(__('Export To Csv'))?>
    	</button>
  	</a>
  	<a href="<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'products', 'action' => 'add'])?>">
    	<button class="common-btn"><?=h(__('Add New Product'))?></button>
	</a>
  	<div class="dropdown">
        <button class="dropdown-btn btn-white dropdown-toggle" id="dropdown-select-stock" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <?=h(__('Add new...'))?><img src="/img/stock_icons/icon-arrow-down.png">
        </button>
  		<div class="dropdown-menu" aria-labelledby="dropdown-add-new-parameter">
			<?php foreach ($parameters as $param): ?>
				<a class="dropdown-item" data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#create-<?=h($param)?>-modal"><?=ucwords(str_replace('-', ' ', $param))?></a>
			<?php endforeach;?>
		</div>
  	</div>
</div>

<?php foreach ($parameters as $param): ?>
	<?=$this->element('create_product_param_modal', [
    'param' => $param,
    'modal_id' => 'create-' . $param . '-modal',
    'modal_title' => _('Add new') . ' ' . __(ucwords(str_replace('-', ' ', $param))),
    'confirm_button_text' => __('Save'),
    'action_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'products', 'action' => 'createNew' . $this->ProductParams->modelName($param)]),
])?>
<?php endforeach;?>

<?php
echo $this->Modal->create(__('Import Data From CSV'), [
    'id' => 'import-data-modal',
    'close' => false,
]);
echo $this->Form->create('import-from-csv-modal-form', [
    'type' => 'file',
    'url' => [
        'prefix' => 'admin',
        'controller' => 'products',
        'action' => 'importFromCsv',
    ],
]);
echo $this->Form->control('csv_file', [
    'type' => 'file',
    'label' => __('Upload File'),
]);
echo $this->Form->submit(__('Upload Data'));
echo $this->Form->end();
echo $this->Modal->end();
?>

<div class="search-input-margin"></div>
<?=$this->element('search_input', [$search_data])?>
<?php if ($search_performed && $products->count() > 0): ?>
  <?=$this->element('admin_search_results', [$products, $currency_code, $content, $prices, $product_statuses_list, $product_status_search])?>
<?php endif;?>

<?php if ($search_performed && $products->count() === 0): ?>
  <?=$this->element('search_results_not_found', ['message' => __('It seems, that what you are looking for does not exist in our database.')])?>
<?php endif;?>

<?=$this->Html->script('admin/products/index')?>