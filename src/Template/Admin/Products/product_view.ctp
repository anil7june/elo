<!-- File: src/Template/Catalog/Products/createProduct.ctp -->
<?=$this->element('product_view')?>

<div class="row product-view-row">
    <div class="col-md-12 my-col">
        <div class="product-details-container">
            <div class="row tests-row-header">
                <p><?=h(__('Tests'))?></p>
			</div>
			<div class="table-responsive">
				<table class="table product-table info-table shadow-header-table user-view-table" id="tests-table">
					<tr class="header">
						<td class="border-bottom border-right bold-content text-centered"><?=h(__('Platinum') . ' (' . $this->Units->ppm() . ')')?></td>
						<td class="border-bottom border-right bold-content text-centered"><?=h(__('Palladium') . ' (' . $this->Units->ppm() . ')')?></td>
						<td class="border-bottom border-right bold-content text-centered"><?=h(__('Rhodium') . ' (' . $this->Units->ppm() . ')')?></td>
						<td class="border-bottom border-right bold-content text-centered"><?=h(__('Assay Provider') . ' (' . $this->Units->ppm() . ')')?></td>
						<td class="border-bottom bold-content text-centered"><?=h(__('Status'))?></td>
					</tr>
					<?php foreach ($tests as $test): ?>
						<tr>
							<td class="border-bottom border-right normal-content text-centered"><?=h($test->pt)?></td>
							<td class="border-bottom border-right normal-content text-centered"><?=h($test->pd)?></td>
							<td class="border-bottom border-right normal-content text-centered"><?=h($test->rh)?></td>
							<td class="border-bottom border-right normal-content text-centered"><?=h($test->analysis_type['name'])?></td>
							<td class="border-bottom normal-content text-centered"><?=h($testStatus[$test->id])?></td>
						</tr>
					<?php endforeach;?>
				</table>
			</div>
        </div>
    </div>
</div>
