<?=$this->element('header_with_back', ['back_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'products', 'action' => 'index']), 'header_text' => __('ADD NEW PRODUCT'), 'back_link_text' => __('Back to Products')])?>

<div class="product-form-wrapper">
    <?=$this->element('product_form', ['submit_text' => __('Add Product')])?>
    <?=$this->element('tests_list', ['submit_url' => ''])?>
</div>

<?=$this->Html->script('admin/products/add.js')?>


