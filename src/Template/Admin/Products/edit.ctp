<!-- File: src/Template/Catalog/Products/createProduct.ctp -->

<?=$this->element('header_with_back', ['back_url' => $prev_location_url, 'header_text' => __("EDIT PRODUCT"), 'back_link_text' => __('Back to list')])?>

<div class="product-form-wrapper">
	<?=$this->element('product_form', ['submit_text' => __('Update Product')])?>
	<?=$this->element('tests_list')?>
</div>

<?=$this->element('confirm_action_modal_with_param', ['modal_id' => 'delete-test-modal', 'modal_title' => __('Are you sure you want to delete this test?'), 'delete_action_url' => $this->Url->build(['prefix' => 'admin', 'controller' => 'tests', 'action' => 'removeTest']), 'confirm_button_text' => __('Delete')])?>
<?=$this->Html->script('admin/products/edit.js')?>

