<div class="container justify-content-center align-items-center" id="login-container">
    <div class="logo-container">
        <img src="/img/elemental-logo.png" class="logo">
    </div>
    <div class="login-container">
        <div class="row justify-content-center align-items-center">
            <div class="login-header fgt-pass">
                <?=h(__('Forgot password?'))?>
            </div>
            <div class="col-md-12 form-box">
                <form class="form" action="" method="post">
                    <?=$this->Form->control('email', ['type' => 'text', 'placeholder' => 'Enter email', 'label' => 'We\'ll send a recovery link to'])?>
                    <div class="form-group">
                        <input type="submit" name="submit" class="btn btn-forgot-password"
                            value="<?=h(__('Send recovery link'))?>">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="below-form">
        <div class="text-small">
            <?=h(__('Return to '))?><a
                href="<?=$this->Url->build(['prefix' => false, 'controller' => 'users', 'action' => 'login'])?>"><?=__('log in')?></a>
        </div>
    </div>
</div>