<!-- src/Template/Users/add.ctp -->
<div class="container">
    <div class="logo-container register-logo">
        <img src="/img/elemental-logo.png" class="logo">
    </div>
    <div class="register-container">
        <div class="row">
            <div class="form-header form-text">
                <?=h(__('Sign up for new account'))?>
            </div>
        </div>
        <?=$this->Form->create($user, ['type' => 'file'])?>
        <div class="form-container">
            <div class="logo-col">
                <div class="logo-wrapper">
                    <div id="logo-text">
                        <?=h(__('Company Logo  '))?>
                    </div>
                </div>
                <div class="text-wrapper" id="row-1">
                    <div class="text-row">
                        <?=h(__("If you'd like the company's logo to be displayed in the navigation bar, please add it."))?>
                    </div>
                </div>
                <div class="text-wrapper" id="row-2">
                    <div class="text-row fade-text">
                        <?=h(__("LOGO PREFERENCES:"))?>
                        <br>
                        <?=h(__("*.png file with transparent background"))?>
                    </div>
                </div>
                <img src="" style="display:none;" id="logo-img" />
                <div class="file-input logo">
                    <span>
                        <img id="left-margin" src="/img/icons/upload-logo-icon.png"><?=h(__('Upload your logo'))?>
                    </span>
                    <input type="file" name="company_logo" id="company-logo">
                </div>

            </div>
            <div class="my-form-col">
                <div class="split-form-row">
                    <div>
                        <?=$this->Form->control('first_name', ['label' => __('First Name'), 'type' => 'text'])?>
                    </div>
                    <div>
                        <?=$this->Form->control('last_name', ['label' => __('Last Name'), 'type' => 'text'])?>
                    </div>
                </div>
                <div>
                    <?=$this->Form->control('company', ['label' => __('Company'), 'empty' => __('Select your company'), 'type' => 'text', 'placeholder' => __('Enter your company name')])?>
                </div>
                <div>
                    <?=$this->Form->control('country_id', ['type' => 'select', 'empty' => __('Select your country'), 'label' => ''])?>
                </div>
                <div>
                    <?=$this->Form->control('email', ['type' => 'text', 'label' => __('Email address')])?>
                </div>
                <div>
                    <?=$this->Form->control('phone', ['type' => 'text', 'label' => __('Cell phone')])?>
                </div>
                <div class="white-row">
                </div>
            </div>
            <div class="my-form-col">
                <div>
                    <?=$this->Form->control('password', ['type' => 'password', 'label' => __('Password')])?>
                </div>
                <div>
                    <?=$this->Form->control('password_2', ['type' => 'password', 'label' => __('Confirm Password')])?>
                </div>
                <div>
                    <?=$this->Form->control('comments', ['type' => 'textarea', 'label' => __('Comments')])?>
                </div>
                <div>
                    <?=$this->Form->control('supervisor_email', ['type' => 'email', 'label' => __('Supervisor email')])?>
                </div>
                <div class="split-form-row submit-row">
                    <div id="disclaimer">
                        <?=$this->Form->control('disclaimer_id', ['type' => 'checkbox', 'value' => $disclaimer->id, 'label' => ['text' => '<p>' . __('I accept the ') . '<a href="' . $this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'showDisclaimer', $disclaimer->id, $language_id]) . '" target="_blank">' . __('Terms of use') . '</a></p>', 'escape' => false]])?>
                    </div>
                    <div>
                        <?=$this->Form->control('language_id', ['type' => 'hidden', 'value' => $language_id])?>
                        <input type="submit" name="submit" class="btn btn-register" value="<?=h(__('Sign up'))?>">
                    </div>

                </div>
            </div>
        </div>
        <?=$this->Form->end()?>
    </div>


    <div class="below-form">
        <div class="text-small">
            <?=h(__('Already have an account? '))?><a
                href="<?=$this->Url->build(['prefix' => false, 'controller' => 'users', 'action' => 'login'])?>"><?=h(__('Log in'))?></a>
        </div>
    </div>

    <script>
    $('.logo-col').change(function(event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        $("#logo-img").fadeIn("fast").attr('src', URL.createObjectURL(event.target.files[0]));
        $("#logo-text").hide();
        $(".logo-wrapper").addClass("transparent");
    });
    </script>