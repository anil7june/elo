<div class="container justify-content-center align-items-center" id="login-container">
    <div class="logo-container">
        <img src="/img/elemental-logo.png" class="logo">
    </div>
    <div class="login-container">
        <div class="row justify-content-center align-items-center">
            <div class="login-header fgt-pass">
                <?=h(__('Forgot password?'))?>
            </div>
            <div class="regular-message"><?=h(__('We sent a recovery link to your email address:'))?></div>
            <div class="bold-message"><?=h($email)?></div>
        </div>
    </div>
    <div class="below-form">
        <div class="text-small">
            <?=h(__('Return to '))?><a
                href="<?=$this->Url->build(['prefix' => false, 'controller' => 'users', 'action' => 'login'])?>"><?=__('log in')?></a>
        </div>
    </div>
</div>