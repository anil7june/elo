<!-- File: src/Template/Users/login.ctp -->



<div class="container justify-content-center align-items-center" id="login-container">
    <div class="logo-container">
        <img src="/img/elemental-logo.png" class="logo">
    </div>
    <div class="login-container">
        <div class="row justify-content-center align-items-center">
            <div class="login-header">
                <?=h(__('Log in to your account'))?>
            </div>
            <div class="col-md-12 form-box">
                <form class="form" action="" method="post">
                    <div class="form-group">
                        <input type="text" name="email" placeholder="<?=h(__('Email'))?>" id="username"
                            class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" placeholder="<?=h(__('Password'))?>" id="password"
                            class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" class="btn btn-login" value="<?=h(__('Log in'))?>">
                    </div>
                </form>
                <div class="form-text">
                    <a
                        href="<?=$this->Url->build(['prefix' => false, 'controller' => 'users', 'action' => 'forgotPassword'])?>"><?=h(__('Forgot password?'))?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="below-form">
        <div class="text-small">
            <?=h(__('Need an account? '))?><a
                href="<?=$this->Url->build(['prefix' => false, 'controller' => 'users', 'action' => 'registerUser'])?>"><?=h(__('Sign up'))?></a>
        </div>
    </div>
</div>