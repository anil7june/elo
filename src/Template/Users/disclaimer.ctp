<div class="disclaimer-container">
    <h1><?=h(__('Accept new terms of use'))?></h1>
    <?=$disclaimer_content->content?>
    <?=$this->Form->create()?>
    <div class="col-md-11">
    </div>
    <div class="col-md-1">
        <?=$this->Form->submit(__('Accept'))?>
    </div>
    <?=$this->Form->end()?>
</div>