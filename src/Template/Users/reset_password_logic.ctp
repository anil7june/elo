<div class="container justify-content-center align-items-center" id="login-container">
    <div class="logo-container">
        <img src="/img/elemental-logo.png" class="logo">
    </div>
    <div class="login-container">
        <div class="row justify-content-center align-items-center">
            <div class="login-header fgt-pass">
                <?=h(__('Reset your password'))?>
            </div>
            <div class="col-md-12 form-box">
                <?=$this->Form->create('reset-password-form', ['id' => 'reset-password-form'])?>
                    <?=$this->Form->control('password', ['type' => 'password', 'placeholder' => __('Enter new password'), 'label' => __('Password'), 'value' => ''])?>
                    <?=$this->Form->control('password_2', ['type' => 'password', 'placeholder' => __('Confirm new password'), 'label' => __('Confirm Password')])?>
                    <div>
                        <input type="submit" name="submit" class="btn btn-forgot-password"
                            value="<?=h(__('Reset password'))?>">
                    </div>
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script([
    'forms/validation_message_controller.js',
    'forms/form_field_validator.js',
    'forms/form_validator.js',
])?>

<script>
    $(document).ready(function () {
        $('#reset-password-form').submit(function () {
            return validateForm($(this), {
                'password': {
                    id: 'password',
                    condition: 'validPassword',
                    allowEmpty: false,
                    message: '<?=h(__('Password must be at least 8 characters long, at most 15 characters, contains only letters and digits: at least one digit, one uppercase letter and one lowercase letter'))?>'
                },
                'password-2': {
                    id: 'password-2',
                    condition: 'validPassword',
                    allowEmpty: false,
                    message: '<?=h(__('Password must be at least 8 characters long, at most 15 characters, contains only letters and digits: at least one digit, one uppercase letter and one lowercase letter'))?>'
                },
            });
        });
    });
</script>