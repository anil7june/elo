<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'Elemental';
?>
<!DOCTYPE html>
<html>

<head>
    <?=$this->Html->charset()?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?=$cakeDescription?>:
        <?=$this->fetch('title')?>
    </title>
    <?=$this->Html->meta('icon')?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />


    <link href='//fonts.googleapis.com/css?family=Open+Sans:600' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
    <?=$this->Html->css('base.css')?>
    <?=$this->Html->css('style.css')?>
    <?=$this->Html->css('common.css')?>


    <?=$this->fetch('meta')?>
    <?=$this->fetch('css')?>
    <?=$this->fetch('script')?>
</head>

<body>

    <div class="d-md-block">
        <nav class="navbar navbar-expand-md navbar-light fixed-top">
            <div class="nav-item mobile-logo mobile">
                <a href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'products', 'action' => 'index'])?>" class="navbar-brand"><img src="<?=h($logo_path)?>" class="img-logo"></a>
            </div>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#collapse_target">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class ="collapse navbar-collapse mobile" id="collapse_target">
                <div>
                <ul class="navbar-nav grey-bckg">
                    <li>
                        <a class="navbar-brand" href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'products', 'action' => 'search'], true)?>"><?=h(__('Search'))?></a>
                    </li>
                    <li>
                        <a class="navbar-brand" href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'index'], true)?>"><?=h(__('Stocks'))?></a>
                    </li>
                    <?php if ($dashboard_permission): ?>
                        <li>
                            <a class="navbar-brand" href="<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'dashboard', 'action' => 'index'])?>"><?=h(__('Manage'))?></a>
                        </li>
                    <?php endif;?>
                </ul></div>
                <div class="white">
                    <ul class="navbar-nav">
                        <li>
                            <div>
                                <img src="/img/icons/icon-language.png" class="ico-img mobile-img"/><?=h(__('Language') . ': ' . $user_language_name)?>
                            </div>
                            <div>
                                <button class="navbar-toggler small" type="button" data-toggle="collapse" data-target="#language-collapse" aria-expanded="false" aria-controls="language-collapse">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </div>
                        </li>
                        <div class="collapse collapse-inside" id="language-collapse">
                            <ul class="collapse-list">
                            <?php foreach ($languages as $language): ?>
                            <li>
                                <a
                                    href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'languages', 'action' => 'updateLanguage', $language->id])?>">
                                    <?=h($language->name)?>
                                </a>
                            </li>
                            <?php endforeach;?>
                            </ul>
                        </div>
                        <li>
                            <div><img src="/img/icons/icon-currency.png" class="ico-img mobile-img"/><?=h(__('Currency') . ': ' . $user_currency_code)?></div>
                            <div><button class="navbar-toggler small" type="button" data-toggle="collapse" data-target="#currencies-collapse" aria-expanded="false" aria-controls="currencies-collapse">
                                <span class="navbar-toggler-icon"></span>
                            </button></div>
                        </li>
                        <div class="collapse collapse-inside" id="currencies-collapse">
                            <ul class="collapse-list">
                                <?php foreach ($currencies as $currency): ?>
                                    <li>
                                        <a href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'currencies', 'action' => 'updateCurrency', $currency->id])?>">
                                            <?=h($currency->code)?><small><?=h(__('Exchange rate: ') . $currency->rate['exchange_rate'])?></small>
                                        </a>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </ul>
                </div>
                <div class="white">
                    <ul class="navbar-nav">
                        <li>
                            <a href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'account', 'action' => 'index'], true)?>"><img class="ico-img mobile-img"src="/img/icons/icon-account.png" /><?=h(__('My account'))?></a>
                        </li>
                        <li>
                            <a href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'contact', 'action' => 'index'], true)?>"><img class="ico-img mobile-img"src="/img/icons/icon-contact.png" /><?=h(__('Contact us'))?></a>
                        </li>
                        <li>
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'users', 'action' => 'logout'])?>"><img class="ico-img mobile-img" src="/img/icons/icon-logout.png" /><?=h(__('Log out'))?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="collapse navbar-collapse desktop">
                <ul class="navbar-nav nav-fill mx-auto">
                    <li class="nav-item nav-bar-logo">
                        <a href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'products', 'action' => 'index'])?>"
                            class="navbar-brand"><img src="<?=h($logo_path)?>" class="img-logo"></a>
                    </li>
                    <li class="nav-item ml-2" id="search-link">
                        <a class="nav-link"
                            href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'products', 'action' => 'search'], true)?>"><?=h(__('Search'))?></a>
                    </li>
                    <li class="nav-item ml-2" id="stocks-link">
                        <a class="nav-link"
                            href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'index'], true)?>"><?=h(__('Stocks'))?></a>
                    </li>
                    <?php if ($dashboard_permission): ?>
                    <li class="nav-item ml-2" id="dashboard-link">
                        <a class="nav-link"
                            href="<?=$this->Url->build(['prefix' => 'admin', 'controller' => 'dashboard', 'action' => 'index'])?>"><?=h(__('Manage'))?></a>
                    </li>
                    <?php endif;?>
                </ul>
                <ul class="navbar-nav nav-fill mx-auto">
                    <li class="nav-item dropdown ico-dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown"
                            data-target="dropdown_languages_target">
                            <img src="/img/icons/icon-language.png" class="ico-img img-fluid" />
                            <img class="ml-1" src="/img/icons/icon-arrow-down.png" class="img-fluid" />
                        </a>

                        <div class="dropdown-menu" role="menu" aria-labbeledby="dropdown_languages_target">
                            <?php foreach ($languages as $language): ?>
                            <a class="dropdown-item"
                                href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'languages', 'action' => 'updateLanguage', $language->id])?>">
                                <?php if ($language->id === $user_language_id): ?><img src="/img/icons/icon-checked.png"
                                    class="img-fluid" /><?php else: ?><img src="/img/icons/space-icon.png"
                                    class="img-fluid" />
                                <?php endif;?>
                                <?=h($language->name)?>
                            </a>
                            <?php endforeach;?>
                        </div>
                    </li>
                    <li class="nav-item dropdown ico-dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown"
                            data-target="dropdown_currencies_target">
                            <img src="/img/icons/icon-currency.png" class="ico-img img-fluid" />
                            <img class="ml-1 img-fluid" src="/img/icons/icon-arrow-down.png" />
                        </a>
                        <div class="dropdown-menu" role="menu" aria-labbeledby="dropdown_currencies_target">
                            <?php foreach ($currencies as $currency): ?>
                            <a class="dropdown-item"
                                href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'currencies', 'action' => 'updateCurrency', $currency->id])?>">
                                <?php if ($currency->id === $user_currency_id): ?><img src="/img/icons/icon-checked.png"
                                    class="img-fluid" /><?php else: ?><img src="/img/icons/space-icon.png"
                                    class="img-fluid" />
                                <?php endif;?>
                                <?=h($currency->code)?>
                                <small><?=h(__('Exchange rate: ') . $currency->rate['exchange_rate'])?></small>
                            </a>
                            <?php endforeach;?>
                        </div>
                    </li>
                    <li class="nav-item line">
                        <img src="/img/icons/line-nav-bar.png" class="img-fluid" />
                    </li>
                    <li class="nav-item">
                        <a class="nav-link right-link"
                            href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'account', 'action' => 'index'], true)?>">
                            <img class="link-icon img-fluid"
                                src="/img/icons/icon-account.png" /><?=h(__('My account'))?>
                        </a>
                    </li>
                    <li class="nav-item line">
                        <img src="/img/icons/line-nav-bar.png" class="img-fluid" />
                    </li>
                    <li class="nav-item">
                        <a class="nav-link right-link"
                            href="<?=$this->Url->build(['prefix' => 'catalog', 'controller' => 'contact', 'action' => 'index'], true)?>">
                            <img class="link-icon img-fluid"
                                src="/img/icons/icon-contact.png" /><?=h(__('Contact us'))?>
                        </a>
                    </li>
                    <li class="nav-item line">
                        <img src="/img/icons/line-nav-bar.png" class="img-fluid" />
                    </li>
                    <li class="nav-item">
                        <a class="nav-link right-link"
                            href="<?=$this->Url->build(['prefix' => false, 'controller' => 'users', 'action' => 'logout'])?>">
                            <img class="link-icon img-fluid" src="/img/icons/icon-logout.png" /><?=h(__('Log out'))?>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="nav-bar-margin">
    </div>
    <?=$this->Flash->render()?>
    <div class="flash-margin">
    </div>
    <div class="content">
        <div class="metal-prices-wrapper">
            <div class="metal-prices">
                <?php foreach ($metal_prices as $metal => $price): ?>
                    <div class="metal-price key"><?=h($metal . ": ")?></div>
                    <div class="metal-price value"><?=h($this->ProductFormat->formatNumber($price, $user_id))?><?php echo ' ' . $this->Units->getCurrencySymbol($user_currency_code) . '/toz' ?></div>
                <?php endforeach;?>
            </div>
        </div>
        <?=$this->fetch('content')?>
        <div class="margin-bottom">
        </div>
    </div>

</body>

<script>
$(document).ready(function() {
    var url = window.location.toString();
    console.log(url);
    if(url.includes("/search")) {
        $('#search-link').addClass("active");
    } else if(url.includes("/catalog/stocks")) {
        $('#stocks-link').addClass("active");
    } else if(url.includes("/admin")) {
        $('#dashboard-link').addClass("active");
    }

});
</script>

</html>