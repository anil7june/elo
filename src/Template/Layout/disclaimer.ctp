<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'Elemental';
?>
<!DOCTYPE html>
<html>

<head>
    <?=$this->Html->charset()?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?=$cakeDescription?>:
        <?=$this->fetch('title')?>
    </title>
    <?=$this->Html->meta('icon')?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />


    <link href='//fonts.googleapis.com/css?family=Open+Sans:600' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
    <?=$this->Html->css('base.css')?>
    <?=$this->Html->css('style.css')?>
    <?=$this->Html->css('common.css')?>

    <?=$this->fetch('meta')?>
    <?=$this->fetch('css')?>
    <?=$this->fetch('script')?>
</head>

<body>
    <div class="d-md-block">
        <nav class="navbar navbar-expand-md navbar-light fixed-top">
            <div class="order-1">
                <ul class="navbar-nav nav-fill">
                    <li class="nav-item nav-bar-logo disclaimer-logo">
                        <img src="/img/elemental-logo.png" class="img-logo"></a>
                    </li>
                </ul>
            </div>
            <div class="order-2 w-100 mobile-no-order"></div>
            <div class="order-3 justify-content-end">
                <ul class="navbar-nav nav-fill mr-4">
                    <li class="nav-item dropdown ico-dropdown disclaimer-lang">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown"
                            data-target="dropdown_languages_target">
                            <img src="/img/icons/icon-language.png" class="ico-img img-fluid" id="disclaimer-lang-ico" />
                            <img src="/img/icons/icon-arrow-down.png" class="ico-img img-fluid ml-1" id="disclaimer-arrow-down-ico" />
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labbeledby="dropdown_languages_target">
                            <?php foreach ($language_list as $language): ?>
                            <li>
                            <a class="dropdown-item"
                                href="<?=$this->Url->build(['prefix' => false, 'controller' => 'users', 'action' => 'showDisclaimer', $disclaimer_id, $language->id])?>">
                                <?=h($language->name)?>
                            </a>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="nav-bar-margin"></div>
    <?=$this->Flash->render()?>
    <div class="flash-margin">
    </div>
    <div class="content">
        <?=$this->fetch('content')?>
        <div class="margin-bottom">
        </div>
    </div>

</body>