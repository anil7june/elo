<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?= $this->Html->css(['bootstrap/bootstrap.css']); ?>
    <?= $this->Html->css(WWW_ROOT . '/css/base.css') ?>
    <?= $this->Html->css(WWW_ROOT . '/css/style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
</head>
<body>
	<?= $this->Flash->render() ?>
	<div class="container clearfix">
			<?= $this->fetch('content') ?>
	</div>
	<footer>
	</footer>
</body>
</html>
