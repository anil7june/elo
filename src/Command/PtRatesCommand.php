<?php
namespace App\Command;

use App\Utility\PgmPriceUpdater;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;

class PtRatesCommand extends Command
{
    public function initialize()
    {
        parent::initialize();
    }

    public function execute(Arguments $args, ConsoleIO $io)
    {
        $price_updater = new PgmPriceUpdater();
        $price_updater->updatePtPrice();
    }
}
