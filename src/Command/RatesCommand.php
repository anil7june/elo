<?php
namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Http\Client;

class RatesCommand extends Command {

  public function initialize() {
    parent::initialize();
    $this->loadModel('Currencies');
    $this->loadModel('ExchangeRateVersions');
  }

  public function execute(Arguments $args, ConsoleIO $io) {

    $pt_value = $this->getPtPrice();
    $pd_value = $this->getPdPrice();
    $io->out("Pt value: " . $pt_value);
    $io->out("Pd value: " . $pd_value);
  }

  private function getPtPrice() {
    $http = new Client();
    $value = $http->get('https://www.bulliondesk.com/fmdatacache/?sym=XPT&fld=B&zz=319897461801&__=1552396627106&ts=24-Apr-1992+13%3A17:04');
    $value = $value->getStringBody();
    die();
    return htmlentities($value);
  }

  private function getPdPrice() {
    $value = file_get_contents('https://www.bulliondesk.com/fmdatacache/?sym=XPD&fld=B&zz=319897461801&__=1552396627106&ts=24-Apr-1992+13%3A17:04');
    return htmlentities($value);
  }

  private function getRhPrice() {

  }
}
