<?php
namespace App\Command;

use App\Controller\Component\RatesComponent;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Controller\ComponentRegistry;

class ExchangeRatesCommand extends Command
{

    private $ratesComponent;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Currencies');
        $this->ratesComponent = new RatesComponent(new ComponentRegistry());
    }

    public function execute(Arguments $args, ConsoleIO $io)
    {
        $this->ratesComponent->updateExchangeRatesFromNBP($this->Currencies->getAllCodesArray());
    }

}