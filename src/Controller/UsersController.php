<?php
// src/Controller/UsersController

namespace App\Controller;

use App\Controller\AppController;
use App\Utility\EmailFactory;
use App\Utility\FileHandler;
use App\Utility\RandomTokenGenerator;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function beforeFilter(Event $event)
    {

        parent::beforeFilter($event);
        $this->Auth->allow(
            [
                'registerUser',
                'logout',
                'showDisclaimer',
                'forgotPassword',
                'forgotPasswordSuccess',
                'resetPassword',
                'resetPasswordLogic',
            ]);
    }

    public function login()
    {

        $this->layout = "login";

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            $pass = $this->request->getData('password');
            $email = $this->request->getData('email');
            if ($user) {
                $active_status_id = TableRegistry::get('UserStatuses')->findByName('active')->first()->id;
                if ($user['user_status_id'] == $active_status_id) {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Flash->error(__('This user is not active. Please contact your admin.'));
                }
            }
            $this->Flash->error(__('Invalid username or password.'));
        }
    }

    public function disclaimer()
    {

        $this->layout = "disclaimer";

        $user = $this->Users->get($this->Auth->user('id'));

        $disclaimers = TableRegistry::get('Disclaimers');
        $disclaimer = $disclaimers->getCurrentDisclaimer()->first();
        if ($user->disclaimer_id == $disclaimer->id) {
            return $this->redirect(['prefix' => 'catalog', 'controller' => 'Products', 'action' => 'index']);
        } else {
            $disclaimer_contents = TableRegistry::get('DisclaimerContents');
            $disclaimer_content = $disclaimer_contents->getContent($disclaimer->id, $user->language_id)->first();

            $this->set(compact('user'));
            $this->set(compact('disclaimer_content'));
            if ($this->request->is(['post'])) {
                $user->disclaimer_id = $disclaimer->id;
                $this->Users->save($user);
                return $this->redirect(['prefix' => 'catalog', 'controller' => 'Products', 'action' => 'index']);
            }
        }

    }

    public function acceptNewTerms($user_id, $disclaimer_id)
    {

        $user = $this->Users->get($user_id);
        $user->disclaimer_id = $disclaimer_id;
        $this->Users->save($user);

        return $this->redirect(['prefix' => false, 'controller' => 'users', 'action' => 'login']);

    }

    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    public function registerUser()
    {
        $this->layout = "login";

        $this->set([
            'countries' => $this->Users->Countries->find('list'),
            'disclaimer' => $this->Users->Disclaimers->getCurrentDisclaimer()->first(),
            'language_id' => $this->Users->Languages->getDefaultLanguageId(),
        ]);

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if ($this->registerUserLogic($data, $user)) {
                $this->Flash->success(__('Registration complete. Your account is inactive, soon you should receive an activation email.'));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__('Error: email sending error, please contact your tech support'));
            }
        }
        $this->set('user', $user);
    }

    public function registerUserLogic($data, $user)
    {
        $user = $this->Users->patchEntity($user, $data);
        if (!$user->errors()) {

            $file = $this->request->getData('company_logo');
            $user->company_logo = FileHandler::saveFile($file, 'company_logo');
            $user->show_logo_in_header = false;

            $user->role = TableRegistry::get('Roles')->findByName('User')->first();
            $user->user_status_id = TableRegistry::get('UserStatuses')->findByName('pending')->first()->id;
            $user->created = Time::now();
            $user->language_id = $this->Users->Languages->getDefaultLanguageId();
            $user->currency_id = $this->Users->Currencies->findByCode('USD')->first()->id;
            $user->disclaimer_id = $this->Users->Disclaimers->getCurrentDisclaimer()->first()->id;
            if ($this->Users->save($user)) {
                return $this->sendNotificationEmailToSuperAdmins($user);
            }
        }
        return false;
    }

    private function sendNotificationEmailToSuperAdmins($user)
    {
        $super_admins = $this->Users->find()
            ->where([
                'id' => $this->Users->Roles->getSuperAdminRoleId(),
            ]);
        foreach ($super_admins as $super_admin) {
            if (!$this->sendNotificationEmailToSuperAdmin($user, $super_admin)) {
                return false;
            }
        }
        return true;
    }

    private function sendNotificationEmailToSuperAdmin($user, $super_admin)
    {
        $email = EmailFactory::createUserRegistrationRequestAdminNotificationEmail($user);
        try {
            $email
                ->to($super_admin->email)
                ->send();
        } catch (Exception $e) {
            $this->Users->delete($user);
            return false;
        }
        return true;

    }

    public function forgotPassword()
    {
        $this->layout = "login";

        if ($this->request->is('post')) {
            $email = $this->request->getData('email');
            $user = $this->Users->findByEmail($email)->first();
            if (!$user) {
                $this->Flash->error(__('Error: there is no user with given email address'));
            } else {
                $new_token = $this->generateRecoveryToken();
                if ($new_token !== '') {
                    $token_created = $this->createNewTokenForUser($new_token, $user);
                    if ($token_created) {
                        $this->sendRecoveryEmail($user, $new_token);
                        return $this->redirect(['prefix' => false, 'controller' => 'users', 'action' => 'forgotPasswordSuccess', $user->email]);
                    }
                } else {
                    $this->Flash->error(__('Error: unable to send an email'));
                }
            }
        }
    }

    private function generateRecoveryToken()
    {
        $tokens = TableRegistry::get('Tokens');
        $new_token = RandomTokenGenerator::generateToken();
        while ($tokens->tokenExists($new_token)) {
            $new_token = RandomTokenGenerator::generateToken();
        }
        return $new_token;

    }

    private function createNewTokenForUser($token_str, $user)
    {
        $tokens = TableRegistry::get('Tokens');
        $new_token = $tokens->newEntity([
            'token' => $token_str,
            'created' => Time::now(),
        ]);
        if ($tokens->save($new_token)) {
            $user->token_id = $new_token->id;
            $this->Users->save($user);
            return true;
        } else {
            return false;
        }
    }

    private function sendRecoveryEmail($user, $token)
    {
        $recovery_url = $this->generateRecoveryUrl($token);
        $email = EmailFactory::createRecoveryEmail($user, $recovery_url);
        try {
            $email
                ->to($user->email)
                ->send();
        } catch (Exception $e) {
            die(__('Error: email sending error. Please contact to your tech support'));
        }
    }

    private function generateRecoveryUrl($token)
    {
        return Router::url([
            'prefix' => false,
            'controller' => 'users',
            'action' => 'resetPassword',
            '?' => [
                'pass_reset_token' => $token,
            ],
        ], true);
    }

    public function forgotPasswordSuccess($email)
    {
        $this->layout = "login";
        $this->set(compact('email'));
    }

    public function resetPassword()
    {
        $this->autoRender = false;

        if ($this->request->is('get')) {
            $token = $this->request->getQuery('pass_reset_token');
            return $this->redirect(['prefix' => false, 'controller' => 'users', 'action' => 'resetPasswordLogic', $token]);
        }
    }

    public function resetPasswordLogic($token)
    {
        $this->layout = "login";

        $this->checkTokenParam($token);

        $token_id = TableRegistry::get('Tokens')->find()
            ->where(['token' => $token])
            ->first()
            ->id;
        $user = $this->Users->findByTokenId($token_id)->first();

        if ($this->request->is(['put', 'post'])) {
            $data = $this->request->getData();
            if ($data['password'] !== $data['password_2']) {
                $this->Flash->error(__('Passwords did not match'));
            } else {
                $user = $this->Users->patchEntity($user, $data);
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Your password has been changed'));
                    $this->redirect(['prefix' => false, 'controller' => 'users', 'action' => 'login']);
                } else {
                    $this->Flash->error(__('Error: unable to save your new password'));
                }
            }
        }
    }

    private function checkTokenParam($token)
    {
        $tokens = TableRegistry::get('Tokens');
        if (!$tokens->existsAndIsValid($token)) {
            die(__('It seems you are trying to do something wicked...'));
        }
    }

    public function showDisclaimer($disclaimer_id, $language_id)
    {
        $this->layout = "disclaimer";

        $languages = TableRegistry::get('Languages');
        $language_list = $languages->find();
        $this->set(compact('language_list', 'language_id'));

        $disclaimers = TableRegistry::get('Disclaimers');
        $disclaimer_update_date = $disclaimers->get($disclaimer_id)->created;
        $disclaimer_update_date = Time::parse($disclaimer_update_date)->nice();

        $disclaimer_contents = TableRegistry::get('DisclaimerContents');
        $disclaimer_content = $disclaimer_contents->getContent($disclaimer_id, $language_id)->first();

        $this->set(compact('disclaimer_content', 'disclaimer_id', 'disclaimer_update_date'));
    }

}
