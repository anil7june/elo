<?php
// src/Controller/Admin/ProductsController.php

namespace App\Controller\Admin;

use App\Controller\AppController;
use App\Utility\CSVParser;
use App\Utility\FileHandler;
use App\Utility\ProductSearch;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class ProductsController extends AppController
{

    public $paginate = [
        'limit' => 25,
    ];

    public function initialize()
    {

        parent::initialize();
        $this->loadComponent('Prices');
        $this->loadComponent('Paginator');
    }

    public function index()
    {
        $parameters = ['brand', 'analysis-type', 'model'];
        $makes_list = TableRegistry::get('Makes')->find('list');
        $this->set(compact('parameters', 'makes_list'));
        ProductSearch::search($this);
    }

    private function createNewParameter($table_name, $data, $success_msg, $error_msg)
    {
        $data = $this->request->getData();
        if (!$this->formDataCorrect($data)) {
            $this->Flash->error(__("Input error: form should not contain empty fields"));
        } else {
            $table = TableRegistry::get($table_name);
            $data = $this->processData($data);
            $new_entity = $table->patchEntity($table->newEntity(), $data);
            if ($table->save($new_entity)) {
                $this->Flash->success($success_msg);
            } else {
                $this->Flash->error($error_msg);
            }
        }
    }

    private function processData($data)
    {
        $result = [];
        foreach ($data as $name => $value) {
            $start_index = strpos($name, '_') + 1;
            $new_name = substr($name, $start_index);
            $result[$new_name] = $value;
        }
        return $result;
    }

    private function formDataCorrect(array $data)
    {
        foreach ($data as $key => $value) {
            if ($value == '') {
                return false;
            }
        }
        return true;
    }

    public function createNewMake()
    {
        if ($this->request->is('post')) {
            $this->createNewParameter('Makes', $this->request->getData(), __('New Brand has been added'), __('Error: unable to add new Brand'));
            return $this->redirect($this->referer());
        }
    }

    public function createNewCatMake()
    {
        if ($this->request->is('post')) {
            $this->createNewParameter('CatMakes', $this->request->getData(), __('New Model has been added'), __('Error: unable to add new Model'));
            return $this->redirect($this->referer());
        }
    }

    public function createNewAnalysisType()
    {
        if ($this->request->is('post')) {
            $this->createNewParameter('AnalysisTypes', $this->request->getData(), __('New AnalysisType has been added'), __('Error: unable to add new AnalysisType'));
            return $this->redirect($this->referer());
        }
    }

    public function edit($product_id)
    {
        $product = $this->loadProductEntityToEdit($product_id);
        $product_version = $this->createNewProductVersionEntityForProductEntity($product);

        $this->setProductFormView();
        $this->setTestView($product_id);
        $this->setImagesView($product_id);
        $this->set([
            'product' => $product,
            'prev_location_url' => Router::url(['prefix' => 'admin', 'controller' => 'products', 'action' => 'index']),
            'mode' => 'edit',
        ]);

        if ($this->request->is(['put', 'post'])) {
            $data = $this->request->getData();
            $this->createImagesForProduct($product->id, $this->getImagesData($data));
            $product_version = $this->Products->ProductVersions->patchEntity($product_version, $this->request->getData());
            if ($this->updateProductData($product, $product_version, $data)) {
                $this->Flash->success(__('The product has been updated'));
                return $this->redirect(['controller' => 'products', 'action' => 'edit', $product->id]);
            } else {
                $this->Flash->error('Error: unable to add new product');
            }
        }
    }

    public function deleteImagesFromProduct()
    {
        $this->autoRender = false;
        $this->response->type('json');

        if ($this->request->isAjax()) {
            $data = $this->request->getData();
            $images_to_remove = $this->getImagesToRemove($data);
            $product_id = $data['product_id'];
            if (!$this->removeImages($images_to_remove, $product_id)) {
                $result = $this->errorRedirectJson(__('Error: unable to remove images, please contact your tech support.'), "edit", $product_id);
            } else {
                $result = $this->successRedirectJson(__('Images have been removed.'), "edit", $product_id);
            }
            $this->response->body($result);
        }
    }

    private function getImagesToRemove($data)
    {
        $result = [];
        foreach ($data as $name => $id) {
            if (strpos($name, "image_") === 0) {
                if ($id != 0) {
                    $result[] = $id;
                }
            }
        }
        return $result;
    }

    private function removeImages($images_to_remove, $product_id)
    {
        $images = TableRegistry::get('Images');
        $result = true;
        foreach ($images_to_remove as $image_id) {
            $result = $result && FileHandler::removeImage($images->getFileName($image_id));
            $result = $result && $images->removeImage($image_id);
        }
        $result = $result && $images->bindImageToProduct($product_id);
        return $result;
    }

    private function setImagesView($product_id)
    {
        $this->set([
            'images' => TableRegistry::get('Images')->getImagesForProduct($product_id),
        ]);
    }

    private function updateProductData($product, $product_version, $data)
    {
        $product_version->version = $product->version + 1;
        if (!$this->Products->ProductVersions->save($product_version)) {
            return false;
        }
        $product->version += 1;
        $product->product_status_id = $data['product_status_id'];
        if (!$this->Products->save($product)) {
            return false;
        }
        return true;
    }

    private function loadProductEntityToEdit($product_id)
    {

        $find_conditions = [
            'Products.id' => $product_id,
        ];

        $select_conditions = [
            'make.id',
            'cat_make.id',
            'product_version.description',
            'product_version.additional_ref_data',
            'Products.product_status_id',
            'Products.version',
            'product_version.cat_code',
            'product_version.ecr_code',
            'product_version.id',
        ];

        return $this->Products
            ->findByConditions($find_conditions, $select_conditions)
            ->first();
    }

    private function createNewProductVersionEntityForProductEntity($product)
    {
        $product_versions = TableRegistry::get('ProductVersions');
        $product_version = $product_versions->newEntity([
            'product_id' => $product->id,
        ]);
        return $product_version;
    }

    public function delete($product_id)
    {
        $product = $this->Products->get($product_id);
        $product->deleted = true;
        $this->Products->save($product);
        $this->Flash->success(__('Product has been deleted'));
        return $this->redirect(['prefix' => 'admin', 'controller' => 'products', 'action' => 'index']);
    }

    public function add()
    {
        $this->setProductFormView();
        $this->setCreateNewTestModalView();

        $this->set([
            'mode' => 'create',
        ]);
    }

    private function errorRedirectJson($message, $action, $action_arg = "_")
    {
        return json_encode([
            "result" => $message,
            "redirect_url" => $this->getRedirectUrl($message, "error", $action, $action_arg),
        ]);
    }

    private function successRedirectJson($message, $action, $action_arg = "_")
    {
        return json_encode([
            "result" => $message,
            "redirect_url" => $this->getRedirectUrl($message, "success", $action, $action_arg),
        ]);

    }

    public function addRequestHandler()
    {
        $this->autoRender = false;
        $this->response->type('json');

        if ($this->request->isAjax()) {
            $data = $this->request->getData();
            $product_data = json_decode($data['product'], true);
            $test_data = json_decode($data['tests'], true);
            $images_data = $this->getImagesData($data);
            $new_product_id = $this->createNewProduct($product_data);
            if ($new_product_id == -1) {
                $result = $this->errorRedirectJson(__("Error: unable to add new product."), "add");
            } else if (!$this->addTestsToProduct($new_product_id, $test_data)) {
                $result = $this->errorRedirectJson(__("Error: unable add all tests, contact your tech support."), "add");
            } else if (!$this->createImagesForProduct($new_product_id, $images_data)) {
                $result = $this->errorRedirectJson(__("Error: unable to save images, contact your test support."), "add");
            } else {
                $result = $this->successRedirectJson(__("Product created successfuly"), "add");
            }
            $this->response->body($result);
        }
    }

    private function getImagesData($data)
    {
        $result = [];
        foreach ($data as $key => $value) {
            if (strpos($key, "file_") === 0) {
                if ($data[$key]['tmp_name']) {
                    $result[$count] = $data[$key];
                }
            }
            $count++;
        }
        return $result;
    }

    public function redirectToActionWithMessage($action, $message, $type, $action_arg)
    {
        if ($type == "success") {
            $this->Flash->success($message);
        } else if ($type == "error") {
            $this->Flash->error($message);
        }
        return $this->redirect(array_merge(['prefix' => 'admin', 'controler' => 'products', 'action' => $action, $action_arg]));
    }

    private function getRedirectUrl($message, $type, $action, $action_arg)
    {
        $arg = ($action_arg === "_") ? "" : $action_arg;
        return Router::url([
            'prefix' => 'admin',
            'controler' => 'products',
            'action' => 'redirectToActionWithMessage',
            $action,
            $message,
            $type,
            $action_arg,
        ]);
    }

    private function addTestsToProduct($product_id, $data)
    {
        $tests = TableRegistry::get('Tests');
        $analysis_types = TableRegistry::get('AnalysisTypes');

        foreach ($data as $test_data) {
            $test = $tests->newEntity([
                'product_id' => $product_id,
                'analysis_type_id' => $analysis_types->findByName($test_data['analysis_type_name'])
                    ->first()->id,
                'pt' => $test_data['pt'],
                'pd' => $test_data['pd'],
                'rh' => $test_data['rh'],
            ]);
            if (!$tests->save($test)) {
                $this->deleteAllTestsAddedSoFar($product_id);
                return false;
            } else if ($test_data['status'] == "Applied") {
                $product = $this->Products->get($product_id);
                $product->test_id = $test->id;
                $this->Products->save($product);
            }
        }
        return true;
    }

    private function createNewProduct($product_data)
    {
        $product = $this->createProductObject($product_data);
        $product_version = $this->createProductVersionObject($product_data);

        $save_result = $this->saveProductAndProductVersion($product, $product_version);
        return $save_result;
    }

    private function createProductObject($product_data)
    {
        return $this->Products->newEntity([
            'version' => 1,
            'product_status_id' => $product_data['product_status_id'],
            'deleted' => false,
        ]);
    }

    private function createProductVersionObject($product_data)
    {
        $product_versions = TableRegistry::get('ProductVersions');
        $product_version = $product_versions->newEntity([
            'version' => 1,
        ]);
        $product_version = $product_versions->patchEntity($product_version, $product_data);

        return $product_version;
    }

    private function createImagesForProduct($product_id, $images_data)
    {
        $images = TableRegistry::get('Images');
        $result = true;
        if ($images_data !== []) {
            foreach ($images_data as $key => $image_data) {
                $new_file_name = FileHandler::saveImage($image_data);
                if ($new_file_name == '') {
                    return false;
                }
                $result = $result && $images->saveNewImage($new_file_name, $product_id);
            }
            $result = $result && $images->bindImageToProduct($product_id);
        } else {
            $result = $result && $images->addDefaultImageToProduct($product_id);
        }
        return $result;
    }

    private function saveProductAndProductVersion($product, $product_version)
    {
        $product_versions = TableRegistry::get('ProductVersions');
        if ($this->Products->save($product)) {
            $product_version->product_id = $product->id;
            if ($product_versions->save($product_version)) {
                $result = $product->id;
            } else {
                $result = -1;
            }
        } else {
            $result = -1;
        }
        return $result;
    }

    public function importFromCsv()
    {
        if ($this->request->is(['post'])) {
            $file_name = $this->request->getData('csv_file');
            if ($file_name) {
                $tmp_file_name = FileHandler::saveFile($file_name, 'csv');
                $error = CSVParser::parseCSV($tmp_file_name);
                if ($error) {
                    $this->Flash->error($error);
                } else {
                    $import_result = CSVParser::importProducts($tmp_file_name);
                    if ($import_result === "Success") {
                        $this->Flash->success(__('All data from your CSV file has been imported'));
                    } else {
                        $this->Flash->error($import_result);
                    }
                }
                FileHandler::removeFile($tmp_file_name, 'csv');
            } else {
                $this->Flash->error('You have to select CSV file.');
            }
        }
        return $this->redirect(['prefix' => 'admin', 'controller' => 'products', 'action' => 'index']);
    }

    public function exportToCsv()
    {

        if ($this->request->is('Ajax')) {
            $advanced_search_form_data = $this->request->getData();
            $data = $this->getCsvData($advanced_search_form_data);
            $_header = CSVParser::getColumnsNameTableForImport();
            $this->export($data, $_header);
        }
    }

    public function productView($product_id)
    {
        ProductSearch::setProductViewData($product_id, $this->Auth->user('id'), $this);
        $this->setTestView($product_id);
    }

    private function getCsvData($advanced_search_form_data)
    {

        $user_id = $this->Auth->user('id');

        $query = ProductSearch::performAdvancedSearchAction($this, $advanced_search_form_data, 'gross_value', $user_id);

        $query->select([
            'product_version.cat_code',
            'product_version.additional_ref_data',
            'product_version.description',
            'product_version.weight',
            'test.pt',
            'test.pd',
            'test.rh',
            'cat_make.name',
            'analysis_type.name',
        ]);

        $result = [];
        foreach ($query as $product) {
            $result[] = [
                $product->make['name'],
                $product->product_version['cat_code'],
                $product->product_version['additional_ref_data'],
                $produt->product_version['description'],
                $product->product_version['weight'],
                $product->test['pt'],
                $product->test['pd'],
                $product->test['rh'],
                $product->cat_make['name'],
                $product->analysis_type['name'],
            ];
        }
        return $result;
    }

    private function export($data, $_header)
    {

        $_serialize = 'data';

        $this->setResponse($this->getResponse()->withDownload('Products.csv'));
        $this->viewBuilder()->setClassName('CsvView.Csv');
        $this->set(compact('data', '_serialize', '_header'));
    }

    private function setProductFormView()
    {
        $makes_list = TableRegistry::get('Makes')->find('list');
        $cat_makes_list = TableRegistry::get('CatMakes')->find('list');
        $product_statuses_list = TableRegistry::get('ProductStatuses')->find('list');

        $this->set(compact('makes_list', 'cat_makes_list', 'product_statuses_list'));
    }

    private function setTestView($product_id)
    {
        $tests = TableRegistry::get('Tests')->find('all')
            ->select(['Tests.pt', 'Tests.rh', 'Tests.pd', 'AnalysisTypes.name', 'Tests.id'])
            ->contain(['AnalysisTypes', 'Products'])
            ->where([
                'product_id' => $product_id,
                'Tests.deleted' => false,
            ]);

        $product = $this->Products->get($product_id);
        $testStatus = [];

        foreach ($tests as $test) {
            if ($test->id === $product->test_id) {
                $testStatus[$test->id] = 'Applied';
            } else {
                $testStatus[$test->id] = 'Disabled';
            }
        }
        $this->setCreateNewTestModalView();
        $this->set(compact('testStatus', 'tests'));
    }

    private function setCreateNewTestModalView()
    {
        $analysis_types_list = TableRegistry::get('AnalysisTypes')->find('list');
        $this->set(compact('analysis_types_list'));

    }
}
