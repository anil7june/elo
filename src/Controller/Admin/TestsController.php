<?php
// src/Controller/Admin/TestsController.php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class TestsController extends AppController
{

    public function initialize()
    {
        parent::initialize();

    }

    public function addTestToProduct($product_id)
    {
        $analysis_types = TableRegistry::get('AnalysisTypes');
        $analysis_type_list = $analysis_types->find('list');
        $this->set(compact('analysis_type_list'));

        $test = $this->Tests->newEntity();
        if ($this->request->is('post')) {
            $test = $this->Tests->patchEntity($test, $this->request->getData());
            if ($this->Tests->save($test)) {
                $this->linkTestToProduct($test, $product_id);
                $this->Flash->success(__('Test has been added'));
            } else {
                $this->Flash->error(__('Unable to add new test'));
            }
            return $this->redirect(['prefix' => 'admin', 'controller' => 'products', 'action' => 'edit', $product_id]);
        }
        $this->set('test', $test);
    }

    public function activateTest($test_id)
    {

        $test = $this->Tests->get($test_id);

        $products = TableRegistry::get('Products');
        $product = $products->get($test->product_id);

        $product->test_id = $test->id;
        $products->save($product);

        return $this->redirect(['prefix' => 'admin', 'controller' => 'products', 'action' => 'edit', $product->id]);
    }

    public function disactivateTest($test_id)
    {

        $test = $this->Tests->get($test_id);

        $products = TableRegistry::get('Products');
        $product = $products->get($test->product_id);

        if ($product->test_id === $test->id) {
            $product->test_id = 0;
            $products->save($product);
        }

        return $this->redirect(['prefix' => 'admin', 'controller' => 'products', 'action' => 'edit', $product->id]);
    }

    public function removeTest($test_id)
    {
        $test = $this->Tests->get($test_id);

        $products = TableRegistry::get('Products');
        $product = $products->get($test->product_id);

        if ($product->test_id == $test_id) {
            $product->test_id = 0;
            $products->save($product);
        }
        $test->deleted = 1;
        if ($this->Tests->save($test)) {
            $this->Flash->success(__('Test has been deleted'));
            return $this->redirect(['prefix' => 'admin', 'controller' => 'products', 'action' => 'edit', $product->id]);
        } else {
            $this->Flash->error(__('Error: unable to remove this test'));
        }
    }

    private function linkTestToProduct($test, $product_id)
    {

        $productsTable = TableRegistry::get('Products');
        $product = $productsTable->get($product_id);

        $test->product_id = $product->id;
        $this->Tests->save($test);
    }
}
