<?php
// src/Controller/Admin/CoinsController

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class CoinsController extends AppController
{
    private $request_params = ["user_id", "email", "country_id", "filter"];

    public $paginate = [
        'limit' => 25,
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index()
    {

        $this->setView();
        $this->setSearchFormView();
        if ($this->request->is(['get']) && $this->request->getQuery('filter')) {
            $data = $this->request->getQueryParams();
            $conditions = $this->createConditionsFromFormData($data);

            $filterValues = $data;
            $this->setView($conditions);
            $this->set(compact('filterValues'));
        }
    }

    public function editCoinBalance($user_id)
    {

        $this->autoRender = false;

        if ($this->request->is(['Ajax'])) {
            $users = TableRegistry::get('Users');
            $user = $users->get($user_id);
            $user->coins = $this->request->getData('coins');
            $users->save($user);
        }
    }

    private function setView(array $conditions = [])
    {

        $usersTable = TableRegistry::get('Users');

        $users = $usersTable->find('all')
            ->select([
                'Users.id',
                'Users.first_name',
                'Users.last_name',
                'Users.email',
                'Users.coins',
                'Countries.name',
            ])
            ->contain(['Countries'])
            ->where($conditions);

        $this->paginate($users);
        $this->set(compact('users'));
    }

    private function setSearchFormView()
    {

        $usersTable = TableRegistry::get('Users');

        $countries = TableRegistry::get('Countries');
        $countries_list = $countries->find('list');

        $this->set(compact('countries_list'));
        $this->set(compact('users_list'));
    }

    private function createConditionsFromFormData($form_data)
    {

        $conditions = [];

        if ($form_data['user_id']) {
            $conditions['Users.id'] = $form_data['user_id'];
        }
        if ($form_data['email']) {
            $conditions['Users.email'] = $form_data['email'];
        }
        if ($form_data['country_id']) {
            $conditions['Users.country_id'] = $form_data['country_id'];
        }

        return $conditions;
    }
}