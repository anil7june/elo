<?php
// src/Controller/Admin/CurrenciesController

namespace App\Controller\Admin;

use App\Controller\AppController;

class CurrenciesController extends AppController
{
    public function initialize()
    {

        $this->loadComponent('Rates');
        parent::initialize();
    }

    public function index()
    {
        $this->setView();
        if ($this->request->is('post')) {
            $form_data = $this->request->getData();
            $this->processFormData($form_data);
        }
    }

    public function getNonBasedCurrenciesIds()
    {
        $this->autoRender = false;
        $this->response->type('json');

        if ($this->request->isAjax()) {
            $result = [
                'ids' => $this->Currencies->find()
                    ->where(['based' => 0])
                    ->select(['id'])
                    ->toArray(),
            ];
            $this->response->body(json_encode($result));
        }
    }

    private function processFormData($form_data)
    {
        if ($this->checkFormData($form_data)) {
            $this->handleCorrectFormData($form_data);
        } else {
            $this->displayErrorMessage();
        }
    }

    private function handleCorrectFormData($form_data)
    {
        if ($this->updateRates($form_data)) {
            $this->handleSuccess();
        } else {
            $this->displayErrorMessage();
        }
    }

    private function handleSuccess()
    {
        $this->Flash->success(__('Currency rates has been updated'));
        $this->redirect(['prefix' => 'admin', 'controller' => 'currencies', 'action' => 'index']);
    }

    private function displayErrorMessage()
    {
        $this->Flash->error(__('Incorrect data input. 3 decimail places required.'));
    }

    private function updateRates($form_data)
    {
        $result = true;
        $rated_currencies = $this->getRatedCurrenciesQuery();
        foreach ($rated_currencies as $currency) {
            $result = $result && $this->Currencies->updateCurrencyRate($currency->code, $form_data[$currency->id]);
        }
        return $result;
    }

    private function checkFormData($form_data)
    {
        $result = true;
        foreach ($form_data as $id => $exchange_rate_value) {
            $result = $this->validateExchangeRateValue($exchange_rate_value) && $result;
        }
        return $result;
    }

    private function validateExchangeRateValue($value)
    {
        return preg_match("/^[0-9]+\.[0-9]{3}$/", $value);
    }

    private function setView()
    {

        $rated_currencies = $this->getRatedCurrenciesQuery();
        $base_currency = $this->getBaseCurrencyQuery();

        $this->set(compact('rated_currencies', 'base_currency'));
    }

    private function getRatedCurrenciesQuery()
    {
        return $this->Currencies->joinWithExchangeRates([
            'Currencies.based' => 0,
        ]);
    }

    private function getBaseCurrencyQuery()
    {
        $result = $this->Currencies->joinWithExchangeRates([
            'Currencies.based' => 1,
        ]);

        return $result->first();
    }
}
