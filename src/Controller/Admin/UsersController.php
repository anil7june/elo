<?php
// src/Controller/Admin/UsersController

namespace App\Controller\Admin;

use App\Controller\AppController;
use App\Utility\EmailFactory;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class UsersController extends AppController
{
    public $paginate = [
        'limit' => 25,
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index()
    {
        $admin_id = $this->Auth->user('id');
        $this->setSearchInitialView($admin_id);

        if ($this->searchRequested()) {
            $data = $this->request->getQueryParams();
            $this->setFilterValues($data);
            $conditions = $this->createConditionsFromFormData($data, $admin_id);
            $this->handleSearchAction($conditions);
        }
    }

    private function searchRequested()
    {
        return $this->request->is(['get']) && $this->request->getQueryParams('filter');
    }

    private function setFilterValues($data)
    {
        $filterValues = $data;
        $this->set(compact('filterValues'));
    }

    private function handleSearchAction($conditions)
    {
        $admin_id = $this->Auth->user('id');

        if (count($conditions) == 0) {
            $this->Flash->error(__('Please specify at least one search parameter'));
            $empty_results = false;
        } else {
            $users = $this->performSearch($conditions, $admin_id);
            $empty_results = ($users->count() == 0);
            $this->paginate($users);
        }

        $this->set(compact('users', 'empty_results'));
    }

    public function view($user_id)
    {
        $user = $this->Users
            ->getAllUsersQuery([
                'Users.id' => $user_id,
            ])
            ->first();

        if ($this->Users->Roles->ifUserIsSuperAdmin($this->Auth->user('id'))) {
            $roles = $this->Users->Roles->find('list');

        } else {
            $roles = $this->Users->Roles->find('list')->where([
                'id' => $this->Users->Roles->getIdOfUserRole($user_id),
            ]);
        }

        $prev_location = Router::url(['prefix' => 'admin', 'controller' => 'users', 'action' => 'index'], true);
        $user_statuses = TableRegistry::get('UserStatuses')->find('list');

        if ($this->request->is(['put', 'post'])) {
            $data = $this->request->getData();
            $user_status_changed = $data['user_status_id'] != $user->user_status->id;
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $success = $this->Users->save($user);
            if ($success) {
                if ($user_status_changed) {
                    $this->handleUserStatus($user, $data['user_status_id']);
                }
                $this->Flash->success(__('User has been updated'));
            } else {
                $this->Flash->error(__('Error: unable to update user'));
            }
        }
        $this->set(compact('user', 'prev_location', 'roles', 'user_statuses', 'leasing_days_list'));
    }

    private function handleUserStatus($user, $new_status_id)
    {
        $active_status_id = TableRegistry::get('UserStatuses')->getActiveStatus()->id;
        if ($new_status_id == $active_status_id) {
            $email = EmailFactory::createUserAccountActivationNotificationEmail($user);
            try {
                $email
                    ->to($user->email)
                    ->send();
            } catch (Exception $e) {
                die(__('Error: email sending error, please contact your tech support'));
            }
        }
    }

    private function performSearch(array $conditions = [], $admin_id)
    {
        if ($conditions != []) {
            $users = $this->getUsersQueryForAdmin($conditions);
        } else {
            $users = $this->Users->findById(-1);
        }

        return $users;
    }

    private function setSearchInitialView($admin_id)
    {

        $this->set([
            'countries_list' => $this->Users->Countries->find('list'),
            'statuses_list' => $this->Users->UserStatuses->find('list'),
            'roles_list' => $this->Users->Roles->find('list'),
            'if_user_is_super_admin' => $this->Users->Roles->ifUserIsSuperAdmin($admin_id),
        ]);

        $users = $this->getUsersQueryForAdmin($admin_id);
        $this->paginate($users);
        $this->set(compact('users'));
    }

    private function getUsersQueryForAdmin($admin_id, $conditions = [])
    {
        $conditions = $this->addAdminRestrictionToConditions($conditions, $admin_id);
        $users = $this->Users->getAllUsersQuery($conditions);

        return $users;
    }

    private function addAdminRestrictionToConditions($conditions, $admin_id)
    {
        if (!$this->Users->Roles->ifUserIsSuperAdmin($admin_id)) {
            $conditions['Users.role_id !='] = $this->Users->Roles->getSuperAdminRoleId();
        }
        return $conditions;
    }

    private function createConditionsFromFormData($form_data, $admin_id)
    {
        $form_fields = [
            'user_status_id',
            'email',
            'country_id',
            'role_id',
            'company',
            'city',
        ];

        $conditions = [];
        foreach ($form_fields as $field_name) {
            if ($form_data[$field_name]) {
                $conditions['Users.' . $field_name] = $form_data[$field_name];
            }
        }
        return $this->addAdminRestrictionToConditions($conditions, $admin_id);

    }

    private function prepareArrayForQuerySearch($arr): array
    {
        $result = [];

        $result["user_id"] = $arr["user_id"];
        $result["email"] = $arr["email"];
        $result["country_id"] = $arr["country_id"];
        $result["filter"] = $arr["filter"];

        return $result;
    }

    private function setLogoPermission($user_id, $value)
    {
        $user = $this->Users->get($user_id);
        $user->logo_allowed = $value;
        $user->show_logo_in_header = true;
        return $this->Users->save($user);
    }

    public function disableUserLogo($user_id)
    {
        $success = $this->setLogoPermission($user_id, 0);
        if ($success) {
            $this->Flash->success(__('Logo premission disabled'));
        } else {
            $this->Flash->error(__('Error: unable to change logo premission'));
        }
        return $this->redirect(['prefix' => 'admin', 'controller' => 'users', 'action' => 'edit', $user_id]);

    }

    public function enableUserLogo($user_id)
    {
        $success = $this->setLogoPermission($user_id, 1);
        if ($success) {
            $this->Flash->success(__('Logo premission granted'));
        } else {
            $this->Flash->error(__('Error: unable to change logo premission'));
        }
        return $this->redirect(['prefix' => 'admin', 'controller' => 'users', 'action' => 'edit', $user_id]);

    }

}
