<?php

// src/Controller/Catalog/StocksController.php
namespace App\Controller\Admin;

use App\Controller\AppController;
use App\Utility\Converter;
use App\Utility\Formater;
use App\Utility\SQLFormulasGenerator;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class StocksController extends AppController
{

    public $paginate = [
        'limit' => 25,
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index()
    {
        $conditions = [];
        $this->setSearchFormView();
        if ($this->request->is(['get']) && $this->request->getQueryParams('search_filter')) {
            $data = $this->request->getQueryParams();
            $conditions = $this->createConditionsFromFormData($data);
        }
        $stocks = $this->getStocksData($conditions);
        $currencyCode = Converter::getCurrencyCode($this->Auth->user('id'));
        $this->set(compact('currencyCode', 'stocks'));
        $this->paginate($stocks);

    }

    public function stocksToAccept()
    {
        $conditions = [
            'stock_status_id' => $this->Stocks->StockStatuses->getClosedStatusId(),
        ];
        $stocks = $this->getStocksData($conditions);
        $currencyCode = Converter::getCurrencyCode($this->Auth->user('id'));
        $this->set(compact('currencyCode', 'stocks'));
        $this->paginate($stocks);
    }

    public function acceptStock($stock_id)
    {
        $accepted_status_id = $this->Stocks->StockStatuses->getAcceptedStatusId();
        if ($this->stockIsNotClosed($stock_id)) {
            $this->Flash->error(__('It seems, user has cancelled his/her stock after you loaded this page'));
            return $this->redirect(['prefix' => 'admin', 'controller' => 'stocks', 'action' => 'stocksToAccept']);
        }
        if ($this->Stocks->changeStockStatus($stock_id, $accepted_status_id)) {
            $this->Flash->success(__('Stock has been accepted'));
            return $this->redirect(['prefix' => 'admin', 'controller' => 'stocks', 'action' => 'stocksToAccept']);
        } else {
            $this->Flash->error(__('Error: unable to accept the stock'));
        }
    }

    public function rejectStock($stock_id)
    {
        $rejected_status_id = $this->Stocks->StockStatuses->getRejectedStatusId();
        if ($this->stockIsNotClosed($stock_id)) {
            $this->Flash->error(__('It seems, user has cancelled his/her stock after you loaded this page'));
            return $this->redirect(['prefix' => 'admin', 'controller' => 'stocks', 'action' => 'stocksToAccept']);
        }
        if ($this->Stocks->changeStockStatus($stock_id, $rejected_status_id)) {
            $this->Flash->success(__('Stock has been rejected'));
            return $this->redirect(['prefix' => 'admin', 'controller' => 'stocks', 'action' => 'stocksToAccept']);
        } else {
            $this->Flash->error(__('Error: unable to reject the stock'));
        }
    }

    private function stockIsNotClosed($stock_id)
    {
        $stock = $this->Stocks->get($stock_id);
        return $stock->stock_status_id !== $this->Stocks->StockStatuses->getClosedStatusId();
    }

    public function getStockDetails()
    {
        $this->autoRender = false;
        $this->response->type('json');

        if ($this->request->is(['Ajax'])) {
            $json = $this->loadStockDetails($this->request->getData('stock_id'), $this->Auth->user('id'));
            $this->response->body($json);
        }
    }

    private function loadStockDetails($stock_id, $user_id)
    {
        $currencies = TableRegistry::get('Currencies');

        $data = [
            'stock_products' => [
                'products_list' => $this->formatStockProductData($this->Stocks->getStockProductsQuery($stock_id, $user_id)->toArray(), $user_id),
                'info' => [
                    'currency_code' => $currencies->getCurrencyCodeForUser($user_id),
                ],
            ],
            'stock_summary' => $this->Stocks->getKeyValueStockSummaryData($this->Stocks->getFullStockQuery($stock_id, $user_id)->first(), $user_id),
        ];

        return json_encode($data, true);
    }

    private function formatStockProductData($products, $user_id)
    {
        foreach ($products as $product) {
            $this->formatStockProduct($product, $user_id);
        }

        return $products;
    }

    private function formatStockProduct(&$product, $user_id)
    {
        $prices = [
            'catalogue_value',
            'user_value',
            'profit',
        ];
        foreach ($prices as $price) {
            $product[$price] = Formater::formatNumber($product[$price], $user_id);
        }
    }

    private function setSearchFormView()
    {

        $users = TableRegistry::get('Users')->find('list');
        $stockStatuses = TableRegistry::get('StockStatuses')->find('list');

        $this->set(compact('users'));
        $this->set(compact('stockStatuses'));
    }

    private function getStocksData(array $conditions = [])
    {
        $user_id = $this->Auth->user('id');
        return $this->Stocks->getQueryForManageUsersStock($user_id, $conditions);
    }

    private function createConditionsFromFormData(array $form_data)
    {

        $conditions = [];

        if ($form_data['owner']) {
            $conditions['AND'] = $this->generateOwnerSQLExpression($form_data['owner']);
        }
        if ($form_data['stock_status_id']) {
            $conditions['Stocks.stock_status_id'] = $form_data['stock_status_id'];
        }
        if ($form_data['date_from']) {
            $date_from = Time::parse($form_data['date_from'] . '00:00:00')->i18nFormat('yyyy-MM-dd HH:mm:ss');
            $conditions['Stocks.created >= '] = $date_from;
        }
        if ($form_data['date_to']) {
            $date_to = Time::parse($form_data['date_to'] . '23:59:59')->i18nFormat('yyyy-MM-dd HH:mm:ss');
            $conditions['Stocks.created <= '] = $date_to;
        }

        return $conditions;
    }

    private function generateOwnerSQLExpression($owner_input)
    {
        $owner_parameters = [
            'Users.first_name',
            'Users.last_name',
        ];

        $key_words = SQLFormulasGenerator::splitInputIntoEscapedSqlWords($owner_input);
        return SQLFormulasGenerator::everyWordLikeAtLeastOneParameterExpression($key_words, $owner_parameters);
    }

    private function getStockProductsCompactData($stock_id)
    {

        $stock = $this->Stocks->get($stock_id);
        $stock_products = $this->Stocks->getAllStockProductsFromStock($stock->id);
        $stock_products_data = [];
        $user_id = $this->Auth->user('id');

        foreach ($stock_products as $stock_product) {
            $catalogueValue = $stock_product->catalogue_value;
            $userValue = $stock_product->user_value;

            $stock_product_data = [];

            $imagePath;
            if ($stock_product->product_version->image) {
                $imagePath = "/uploads/cat_images/" . $stock_product->product_version->image;
            } else {
                $imagePath = "/img/default_product_image.jpg";
            }

            $stock_product_data['imagePath'] = $imagePath;
            $stock_product_data['id'] = $stock_product->id;
            $stock_product_data['code'] = $stock_product->product_version->cat_code;
            $stock_product_data['no_of_pcs'] = $stock_product->no_of_pcs;
            $stock_product_data['cat_value'] = Converter::format(Converter::convert($catalogueValue, $user_id), $user_id);
            $stock_product_data['price'] = Converter::format(Converter::convert($userValue, $user_id), $user_id);
            $stock_product_data['profit'] = Converter::format(Converter::convert($catalogueValue - $userValue, $user_id), $user_id);
            $stock_products_data[] = $stock_product_data;
        }

        return $stock_products_data;
    }

}
