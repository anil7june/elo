<?php
// src/Controller/Admin/PgmPricesController.php

namespace App\Controller\Admin;

use App\Controller\AppController;

class PgmPricesController extends AppController
{

    public function initialize()
    {
        $this->loadComponent('Rates');
        parent::initialize();
    }

    public function index()
    {
        $this->setView();
    }

    public function updateDiscounts()
    {
        if ($this->request->is(['post'])) {
            $elements = $this->PgmPrices->find();
            $data = $this->request->getData();
            foreach ($elements as $element) {
                $this->PgmPrices->setElementDiscount($element->name, $data[$element->name]);
                $this->PgmPrices->setElementPrice($element->full_name, $data[$element->name . '_price']);
            }
            return $this->redirect(['prefix' => 'admin', 'controller' => 'pgm_prices', 'action' => 'index']);
        }
    }

    private function setView()
    {
        $pgmPrices = $this->PgmPrices->find();
        $prices = [];
        $discounts = [];
        $elements = [];
        foreach ($pgmPrices as $price) {
            $prices[$price->name] = $this->PgmPrices->getElementPrice($price->name);
            $discounts[$price->name] = $this->PgmPrices->getElementDiscount($price->name);
            $elements[] = $price->name;
        }

        $this->set(compact('elements'));
        $this->set(compact('prices'));
        $this->set(compact('discounts'));
    }
}
