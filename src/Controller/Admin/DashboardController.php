<?php
// src/Controller/Admin/DashboardController

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class DashboardController extends AppController
{

    public function index()
    {

    }

    public function users()
    {
        $user_id = $this->Auth->user('id');
        $this->set([
            'user_rights_permissions' => TableRegistry::get('Roles')->hasGranted($user_id, 'admin_roles_index'),
        ]);
    }

}
