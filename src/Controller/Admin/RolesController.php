<?php
// src/Controller/Admin/RolesController
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class RolesController extends AppController
{

    public function initialize()
    {

        parent::initialize();
    }

    public function index()
    {

        $roles = $this->Roles->find();
        $this->set(compact('roles'));

    }

    public function edit($role_id)
    {

        $role = $this->Roles->get($role_id);
        $permission_list = $this->Roles->Permissions->find()->toArray();
        $len = count($permission_list);

        $permission_list_1 = array_slice($permission_list, 0, $len / 2);
        $permission_list_2 = array_slice($permission_list, $len / 2);

        $relations = $this->Roles->PermissionsRoles->find()
            ->where(['role_id' => $role_id]);

        $role_permissions = [];
        foreach ($relations as $relation) {
            $role_permissions[$relation->permission_id] = true;
        }

        $this->set(compact('role', 'role_permissions', 'permission_list_1', 'permission_list_2'));

        if ($this->request->is(['post'])) {
            $new_relations = $this->request->getData();
            foreach ($new_relations as $permission_id => $granted) {
                if (\array_key_exists($permission_id, $role_permissions) && $granted == 0) {
                    $this->deletePermission($role_id, $permission_id);
                } else if (!\array_key_exists($permission_id, $role_permissions) && $granted != 0) {
                    $this->grantPermission($role_id, $permission_id);
                }
            }
            $this->Flash->success(__('Role') . ' ' . $role->name . ' ' . __('has been updated'));
            return $this->redirect(['prefix' => 'admin', 'controller' => 'roles', 'action' => 'edit', $role->id]);
        }

    }

    private function grantPermission($role_id, $permission_id)
    {

        $relations = TableRegistry::get('PermissionsRoles');

        $already_granted = $relations->find()
            ->where([
                'role_id' => $role_id,
                'permission_id' => $permission_id,
            ])->count();

        if (!$already_granted) {
            $relation = $relations->newEntity([
                'role_id' => $role_id,
                'permission_id' => $permission_id,
            ]);
            $relations->save($relation);
        }
    }

    private function deletePermission($role_id, $permission_id)
    {

        $relations = TableRegistry::get('PermissionsRoles');
        $relation = $relations->find()
            ->where([
                'role_id' => $role_id,
                'permission_id' => $permission_id,
            ])
            ->first();

        if ($relation) {
            $relations->delete($relation);
        }
    }

}
