<?php

namespace App\Controller;

use App\Controller\Catalog\ProductsController;
use App\Controller\Catalog\StocksController;
use App\Utility\Formater;
use App\Utility\ProductSearch;
use App\Utility\RequestHelper;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Controller\Controller;
use Cake\Filesystem\File;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AjaxController extends AppController
{
    public function initialize()
    {

        parent::initialize();
    }

    private static $auth_allowed_methods = [
        'API_login',
        'API_get_registration_form_data',
        'API_get_disclaimer_content',
        'API_register_user',
    ];

    public function ajax()
    {

        $this->autoRender = false;
        $this->response->type('json');

        if ($this->request) {
            $request_helper = new RequestHelper($this->request);
            $params = json_decode($request_helper->getParam('params'), true);

            $methods = $this->get_API_methods();
            $method = $params['action_requested'];
            if (!in_array($method, $methods)) {
                $json = $this->json_invalid_method();
            } else {
                $json = self::invoke_api_method($params);
            }
            $this->response->body($json);
        }
    }

    private function invoke_api_method($params)
    {
        $method = $params['action_requested'];
        if (!in_array($method, self::$auth_allowed_methods)) {
            if ($params['session_id'] !== $this->request->session()->id()) {
                return $this->json_auth_error();
            } else {
                $params['user_id'] = $this->Auth->user('id');
            }
        }
        return $this->{$method}($params);
    }

    private function get_API_methods()
    {
        $methods = get_class_methods(new AjaxController());
        $result = [];
        foreach ($methods as $method) {
            if (substr($method, 0, 4) === "API_") {
                $result[] = $method;
            }
        }
        return $result;
    }

    private function API_check_session_id($params)
    {
        return $this->json_success();
    }

    private function API_login($params)
    {
        $email = $params['email'];
        $password = $params['password'];

        $users = TableRegistry::get('Users');
        $user = $users->findByEmail($email)->first();
        if ($user && (new DefaultPasswordHasher())->check($password, $user->password)) {
            $this->Auth->setUser($user);
            return json_encode([
                'session_id' => $this->request->session()->id(),
                'result' => 'success',
            ]);
        } else {
            return $this->json_auth_error();
        }
    }

    private function API_logout($params)
    {
        $session = $this->request->getSession();
        $session->destroy();
        return $this->json_success();
    }

    private function API_search_results($params)
    {
        $user_id = $params['user_id'];
        $search_input = $params['search_input'];
        if (trim($search_input) !== '') {
            $products_ctrl = new ProductsController();
            $query = ProductSearch::performSearchAction($products_ctrl, $search_input, $user_id);
            $data = $this->queryResultsToJSON($query);
        } else {
            $data = json_encode([], true);
        }
        return $data;
    }

    private function API_get_content_amounts($params)
    {
        $content_amounts = TableRegistry::get('ContentAmounts');
        $query = $content_amounts->find('all');
        if ($content_amounts) {
            return $this->queryResultsToJSON($query);
        } else {
            return $this->json_error();
        }
    }

    private function API_get_default_content_amount_and_no_of_pcs($params)
    {
        $content_amounts = TableRegistry::get('ContentAmounts');
        $default_content_amount = $content_amounts->findByName('100%')->first();
        if (!$default_content_amount) {
            return $this->json_error();
        }
        $default_no_of_pcs = 1;
        return json_encode([
            'default_content_amount_id' => $default_content_amount->id,
            'default_no_of_pcs' => $default_no_of_pcs,
        ], true);
    }

    private function API_stock_list_search($params)
    {
        $user_id = $params['user_id'];
        $stock_id = $params['stock_id'];
        $search_input = $params['search_input'];
        if (trim($search_input) !== '') {
            $products_ctrl = new ProductsController();
            $query = ProductSearch::performSearchAction($products_ctrl, $search_input, $user_id, $stock_id);
            $data = $this->queryResultsToJSON($query);
        } else {
            $data = json_encode([], true);
        }
        return $data;
    }

    private function API_product_view($params)
    {
        $product_id = $params['product_id'];
        $user_id = $params['user_id'];
        $products = TableRegistry::get('Products');
        $query = $products->findByConditions([
            'Products.id' => $product_id,
        ]);
        $query = $products->addProductPricesToQuery($query, $user_id);
        $data = $this->singleEntityToJSON($query);
        return $data;
    }

    private function API_stocks_view($params)
    {
        $user_id = $params['user_id'];
        $stocks = TableRegistry::get('Stocks');
        $query = $stocks->getAllUserStocks($user_id);
        $data = $this->queryResultsToJSON($query);
        return $data;
    }

    private function API_stock_details($params)
    {
        $stock_id = $params['stock_id'];
        $user_id = $params['user_id'];
        $stocks_table = TableRegistry::get('Stocks');
        $products_table = TableRegistry::get('Products');
        $stock_statuses_table = TableRegistry::get('StockStatuses');
        $stock = $stocks_table->get($stock_id);
        $stock_products = $stocks_table->getAllStockProductsFromStock($stock_id);
        $stock_products = $stock_products->toArray();
        $exchange_rate = $products_table->getUserExchangeRate($user_id);
        $products_total = 0;
        $price_total = 0;
        foreach ($stock_products as $product) {
            $product['catalogue_value'] = Formater::formatNumber($product['catalogue_value'] * $exchange_rate, $user_id);
            $product['user_value'] = Formater::formatNumber($product['user_value'] * $exchange_rate, $user_id);
            $price_total += $product['catalogue_value'];
        }
        $array = [
            'id' => $stock->id,
            'name' => $stock->name,
            'editable' => ($stock_statuses_table->getOpenStatusId() == $stock->stock_status_id),
            'button_action_name' => $stock_statuses_table->getButtonAction($stock->stock_status_id),
            'status' => [
                'id' => $stock->stock_status_id,
                'name' => $stock_statuses_table->get($stock->stock_status_id)->name,
            ],
            'products' => $stock_products,
            'price_total' => $price_total,
        ];
        $data = json_encode($array, true);
        return $data;
    }

    private function API_get_stock_summary_data($params)
    {
        $stocks_table = TableRegistry::get('Stocks');
        $stock = $stocks_table->getFullStockQuery($params['stock_id'], $params['user_id'])->first();
        return json_encode($stocks_table->getKeyValueStockSummaryData($stock, $params['user_id']), true);

    }

    private function API_perform_stock_status_change_action($params)
    {
        $stock_id = $params['stock_id'];
        $stock = TableRegistry::get('Stocks')->get($stock_id);
        $stock_statuses = TableRegistry::get('StockStatuses');
        $stock_ctrl = new StocksController();
        if ($stock->stock_status_id == $stock_statuses->getOpenStatusId()) {
            if ($stock_ctrl->closeLogic($stock->id)) {
                return $this->json_success();
            } else {
                return $this->json_error();
            }
        } else if ($stock->stock_status_id == $stock_statuses->getClosedStatusId()) {
            if ($stock_ctrl->cancelLogic($stock->id)) {
                return $this->json_success();
            } else {
                return $this->json_error();
            }
        } else if ($stock->stock_status_id == $stock_statuses->getCancelledStatusId()) {
            if ($stock_ctrl->reopenLogic($stock->id)) {
                return $this->json_success();
            } else {
                return $this->json_error();
            }
        } else {
            return $this->json_error();
        }
    }

    private function API_stock_product_detail($params)
    {
        $query = TableRegistry::get('StockProducts')->getStockProductDetailData($params['stock_product_id']);
        return $this->singleEntityToJSON($query);
    }

    private function API_recalculate_stock($params)
    {
        $stock_id = $params['stock_id'];
        $user_id = $params['user_id'];
        $stocks_ctrl = new StocksController();
        $result = $stocks_ctrl->refresh_logic($stock_id, $user_id);
        if ($result) {
            return $this->json_success();
        } else {
            return $this->json_error();
        }
    }

    private function API_remove_stock($params)
    {
        $stock_id = $params['stock_id'];
        $stocks_table = TableRegistry::get('Stocks');
        $stock = $stocks_table->findById($stock_id)->first();
        if (!$stock) {
            return $this->json_error();
        }
        $success = $stocks_table->delete($stock);
        if ($success) {
            return $this->json_success();
        } else {
            return $this->json_error();
        }

    }

    private function API_remove_stock_product($params)
    {
        $stock_product_id = $params['stock_product_id'];
        $stock_products = TableRegistry::get('StockProducts');
        $stock_product = $stock_products->findById($stock_product_id)->first();
        if (!$stock_product) {
            return $this->json_error();
        }
        $success = $stock_products->delete($stock_product);
        if ($success) {
            return $this->json_success();
        } else {
            return $this->json_error();
        }
    }

    private function API_edit_stock_product($params)
    {
        $data = [
            'id' => $params['stock_product_id'],
            'no_of_pcs' => $params['no_of_pcs'],
            'content_amount_id' => $params['content_amount_id'],
        ];

        $ctrl = new StocksController();
        $success = $ctrl->editStockProductLogic($data);
        if ($success) {
            return $this->json_success();
        } else {
            return $this->json_error();
        }
    }

    private function API_create_new_stock($params)
    {
        $data = [
            'name' => $params['name'],
            'margin' => $params['margin'],
            'USD_margin' => $params['USD_margin'],
        ];
        $ctrl = new StocksController();
        $new_stock_id = $ctrl->createStockLogic($params['user_id'], $data);
        if ($new_stock_id) {
            return json_encode(['stock_id' => $new_stock_id], true);
        } else {
            return $this->json_error();
        }
    }

    private function API_add_product_to_stock($params)
    {
        $stock_id = $params['stock_id'];
        $product_id = $params['product_id'];
        $user_id = $params['user_id'];
        $data = $params['data'];
        $data['stock_id'] = $stock_id;

        $products_ctrl = new ProductsController();
        $result = json_decode($products_ctrl->addProductToStockLogic($user_id, $stock_id, $product_id, $data), true);
        if ($result['success']) {
            return $this->json_success();
        } else {
            return $this->json_error();
        }

    }

    private function API_get_registration_form_data($params)
    {
        return json_encode([
            'countries' => TableRegistry::get('Countries')->find()->select(['id', 'name'])->toArray(),
            'companies' => TableRegistry::get('Companies')->find()->select(['id', 'name'])->toArray(),
        ], true);
    }

    private function API_get_disclaimer_content($params)
    {
        $disclaimer_id = TableRegistry::get('Disclaimers')->getCurrentDisclaimer()->first()->id;
        $language_id = TableRegistry::get('Languages')->getDefaultLanguageId();
        if (array_key_exists("language_id", $params)) {
            $language_id = $params["language_id"];
        }
        return json_encode([
            'disclaimer' => TableRegistry::get('DisclaimerContents')->getFormattedContent($disclaimer_id, $language_id),
            'languages' => TableRegistry::get('Languages')->find()->select(['id', 'name'])->toArray(),
            'language_id' => $language_id,
            'language_name' => TableRegistry::get('Languages')->get($language_id)->name,
        ], true);
    }

    private function API_register_user($params)
    {
        $users_ctrl = new UsersController();
        $user = TableRegistry::get('Users')->newEntity();
        $success = $users_ctrl->registerUserLogic($params['form_data'], $user);
        if ($success) {
            return $this->json_success();
        } else {
            return $this->json_error();
        }
    }

    private function API_user_settings_view($params)
    {
        $user_id = $params['user_id'];
        $user = TableRegistry::get('Users')->get($user_id);

        $currencies_table = TableRegistry::get('Currencies');
        $languages_table = TableRegistry::get('Languages');

        $currencies = $currencies_table->find()->select(['id', 'code'])->toArray();
        $languages = $languages_table->find()->select(['id', 'name'])->toArray();

        $result = [
            'user_language_id' => $user->language_id,
            'user_currency_id' => $user->currency_id,
            'languages' => $languages,
            'currencies' => $currencies,
        ];

        return json_encode($result, true);
    }

    private function API_change_user_settings($params)
    {
        $user_id = $params['user_id'];
        $users = TableRegistry::get('Users');
        $user = $users->get($user_id);

        if ($params['currency_id']) {
            $user->currency_id = $params['currency_id'];
        }
        if ($params['language_id']) {
            $user->language_id = $params['language_id'];
        }

        if ($users->save($user)) {
            return $this->json_success();
        } else {
            return $this->json_error();
        }
    }

    private function json_success()
    {
        return \json_encode(['result' => 'success'], true);
    }

    private function json_error()
    {
        return json_encode(['result' => 'error'], true);
    }

    private function json_invalid_method()
    {
        return json_encode(['result' => 'invalid_method'], true);
    }

    private function json_auth_error()
    {
        return json_encode(['result' => 'auth_error'], true);
    }

    private function queryResultsToJSON($query)
    {
        return json_encode($query->toArray(), true);
    }

    private function singleEntityToJSON($query)
    {
        return json_encode($query->first()->toArray(), true);
    }

    private function debug($variable)
    {

        $log_file = new File(WWW_ROOT . DS . 'API_logs' . DS . 'logs', false);
        $log_file->open('w', false);
        $log_file->write('Log input: ');
        $log_file->write($this->varDumpToString($variable), 'w');
        $log_file->close();
    }

    private function varDumpToString($var)
    {
        ob_start();
        var_dump($var);
        $result = ob_get_clean();
        return $result;
    }

}
