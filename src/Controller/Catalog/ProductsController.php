<?php
// src/Controller/Catalog/ProductsController.php
namespace App\Controller\Catalog;

use App\Controller\AppController;
use App\Utility\Converter;
use App\Utility\Formater;
use App\Utility\Prices;
use App\Utility\ProductSearch;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class ProductsController extends AppController
{

    private $requestParams = ['make_id', 'type_id', 'cat_code', 'ecr_code',
        'min_price', 'max_price', 'min_weight', 'max_weight', 'advanced_search_button'];

    public $paginate = [
        'limit' => 25,
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function liveSearch()
    {
        $this->autoRender = false;
        $this->response->type('json');

        if ($this->request->is('Ajax')) {
            $input = $this->request->getData('input');
            $live_search = true;

            $search_keys = ProductSearch::get_search_keys($input);
            $query = $this->Products
                ->findByKeyWords($search_keys, $live_search);
            $query = $query->where(['Products.deleted' => false]);
            if ($query->count() > 0) {
                $json = json_encode(array_merge($query->toArray(), ['empty' => 'false']), true);
            } else {
                $json = json_encode(array_merge(['no sugestions'], ['empty' => 'true']), true);
            }
            $this->response->body($json);
        }
    }

    public function index()
    {
        $user_id = $this->Auth->user('id');
        $roles = TableRegistry::get('Roles');
        if ($roles->ifUserIsAdminOrSuperAdmin($user_id)) {
            return $this->redirect(['prefix' => 'admin', 'controller' => 'stocks', 'action' => 'stocksToAccept']);
        } else if ($roles->getIdOfUserRole($user_id) == $roles->getUserRoleId()) {
            return $this->redirect(['prefix' => 'catalog', 'controller' => 'products', 'action' => 'search']);
        } else {
            die();
        }
    }

    public function search()
    {
        ProductSearch::search($this);
    }

    public function productView($product_id)
    {
        ProductSearch::setProductViewData($product_id, $this->Auth->user('id'), $this);
    }

    public function stockList($stock_id)
    {
        $this->handlePossibleUnauthorizedStockAccess($this->Auth->user('id'), $stock_id, $this->referer());
        ProductSearch::stockListSearch($this, $stock_id);
    }

    public function itemNotFound($stock_id)
    {
        $this->handlePossibleUnauthorizedStockAccess($this->Auth->user('id'), $stock_id, $this->referer());
        $this->setItemNotFoundViewVariables($stock_id);
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data_created_with_no_errors = $this->createPreproductWithProductVersion($data, $stock_id);
            if ($data_created_with_no_errors) {
                $this->redirectToDonePage($data, $stock_id);
            } else {
                $this->Flash->error(__('Error: unable to create custom product'));
            }
        }
    }

    public function preproductCreated($preproduct_code)
    {
        $prev_location = Router::url(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'index']);
        $this->set(compact('prev_location', 'preproduct_code'));
    }

    private function redirectToDonePage($data, $stock_id)
    {
        $this->sendRequestToAdmin($data);
        return $this->redirect(['prefix' => 'catalog', 'controller' => 'products', 'action' => 'preproductCreated', $data['cat_code']]);
    }

    private function setItemNotFoundViewVariables($stock_id)
    {
        $makes = TableRegistry::get('Makes');
        $default_make_id = $makes->findByName('Unknown')->first()->id;
        $makes_list = $makes->find('list');
        $cat_makes_list = TableRegistry::get('CatMakes')->find('list');
        $prev_location = Router::url(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'index']);

        $this->set(compact('makes_list', 'stock_id', 'prev_location', 'default_make_id', 'cat_makes_list'));
    }

    private function createPreproductWithProductVersion($data, $stock_id)
    {
        $stock_products = TableRegistry::get('StockProducts');
        $product_versions = TableRegistry::get('ProductVersions');

        $product_version = $this->createProductVersionForPreproduct($data);
        $preproduct = $this->createPreproduct($stock_id, $data);

        if ($product_versions->save($product_version)) {
            $preproduct->product_version_id = $product_version->id;
            return $stock_products->save($preproduct);
        }
        return false;
    }

    private function createProductVersionForPreproduct($data)
    {
        $product_versions = TableRegistry::get('ProductVersions');
        $product_version = $product_versions->newEntity();

        $product_version = $product_versions->patchEntity($product_version, [
            'make_id' => $data['make_id'],
            'cat_code' => $data['cat_code'],
            'cat_make_id' => $data['cat_make_id'],
        ]);

        return $product_version;
    }

    public function getProductImagePaths()
    {
        $this->autoRender = false;
        $this->response->type('json');

        if ($this->request->isAjax()) {
            $product_id = $this->request->getData('product_id');
            $result = json_encode([
                'result' => 'OK',
                'image_paths' => $this->getImagePathsForProduct($product_id),
            ]);
            $this->response->body($result);
        }
    }

    public function getProductImagePathsToDisplay()
    {
        $this->autoRender = false;
        $this->response->type('json');

        if ($this->request->isAjax()) {
            $product_id = $this->request->getData('product_id');
            $image_paths = $this->getImagePathsForProduct($product_id);
            if ($image_paths == []) {
                $default_image = TableRegistry::get('Images')->getDefaultImage()->first();
                $image_paths = [Formater::imagePath($default_image->file_name)];
            }
            $result = json_encode([
                'result' => 'OK',
                'image_paths' => $image_paths,
            ]);
            $this->response->body($result);
        }

    }

    private function getImagePathsForProduct($product_id)
    {
        $images = TableRegistry::get('Images')->getImagesForProduct($product_id);
        $result = [];
        foreach ($images as $image) {
            $result[] = Formater::imagePath($image->file_name);
        }
        return $result;
    }

    private function createPreproduct($stock_id, $data)
    {
        $stock_products = TableRegistry::get('StockProducts');
        $preproduct = $stock_products->newEntity();

        $preproduct = $stock_products->patchEntity($preproduct, [
            'preproduct' => true,
            'stock_id' => $stock_id,
            'catalogue_value' => $data['value'],
            'user_value' => $data['value'],
            'no_of_pcs' => 1,
            'content_amount_id' => TableRegistry::get('ContentAmounts')->findByName('100%')->first()->id,
        ]);

        return $preproduct;
    }

    private function sendRequestToAdmin($data)
    {
        if ($data['contact_admin'] == 1) {
            ; //send request here
        }
    }

    private function isUserAStockOwner($user_id, $stock_id)
    {
        $stock = TableRegistry::get('Stocks')->get($stock_id);
        return $user_id === $stock->user_id;
    }

    private function handlePossibleUnauthorizedStockAccess($user_id, $stock_id, $unauthorized_redirect)
    {
        if (!$this->isUserAStockOwner($user_id, $stock_id)) {
            $this->Flash->error(__('Error: you are trying to access another users stock!'));
            return $this->redirect($unauthorized_redirect);
        }
    }

    public function addProductToStock($product_id, $stock_id)
    {

        $this->autoRender = false;
        $this->response->type('json');

        if ($this->request->is(['Ajax'])) {
            $data = $this->request->getData();
            $user_id = $this->Auth->user('id');
            $json = self::addProductToStockLogic($user_id, $stock_id, $product_id, $data);
            $this->response->body($json);
        }
    }

    /* data = [product_version_id, content_amount_id, no_of_pcs] */
    public function addProductToStockLogic($user_id, $stock_id, $product_id, $data)
    {
        $stocks = TableRegistry::get('Stocks');
        $stock = $stocks->get($stock_id);

        $stock_products = TableRegistry::get('StockProducts');
        $stock_product = $stock_products->newEntity();

        $exchange_rate = TableRegistry::get('Currencies')->getExchangeRateForUser($user_id);

        $product_data = $this->Products->getProductFullDataQuery($product_id, $user_id);
        $product_data = $this->Products->addUserPricesToQuery($product_data, $stock_id, $user_id);
        $product_data = $product_data->first();

        if (!$this->productVersionInStock($data['product_version_id'], $stock_id, $data['content_amount_id'], $user_id)) {
            $stock_product = $stock_products->patchEntity($stock_product, $data);
            $stock_product->catalogue_value = Converter::convertToBase($exchange_rate, $product_data->catalogue_value);
            $stock_product->user_value = Converter::convertToBase($exchange_rate, $product_data->user_value);
            $saved = $stock_products->save($stock_product);
            if ($saved) {
                $json = json_encode(['success' => true, 'error_message' => ''], true);
            } else {
                $json = json_encode(['success' => false, 'error_message' => 'addProductToStock error: save stock product error'], true);
            }
        } else {
            $incremented = $this->incrementStockProductNoOfPcs($data['no_of_pcs'], $data['product_version_id'], $stock_id);
            if ($incremented) {
                $json = json_encode(['success' => true, 'error_message' => '']);
            } else {
                $json = json_encode(['success' => false, 'error_message' => 'addProductToStock error: increase stock product error']);
            }
        }
        return $json;
    }

    private function setTickerBoxesView()
    {

    }

    private function productVersionInStock($product_version_id, $stock_id, $content_amount_id, $user_id)
    {

        $product_id = $this->Products->ProductVersions->get($product_version_id)->product_id;

        $stock_products = TableRegistry::get('StockProducts');
        $query = $stock_products->find()
            ->where([
                'product_version_id' => $product_version_id,
                'stock_id' => $stock_id,
            ]);
        if ($query->count() == 0) {
            return false;
        }

        $result = false;
        foreach ($query as $stock_product) {
            $current_catalogue_value = Prices::catalogueValue($product_id, $user_id);
            $result = $currenc_catalogue_value == $stock_product->catalogue_value || $content_amount_id == $stock_product->content_amount_id;
        }
        return $result;
    }

    private function incrementStockProductNoOfPcs($by, $product_version_id, $stock_id)
    {

        $stock_products = TableRegistry::get('StockProducts');
        $product = $stock_products->find()
            ->where([
                'stock_id' => $stock_id,
                'product_version_id' => $product_version_id,
            ])
            ->first();
        $product->no_of_pcs += $by;
        return $stock_products->save($product);
    }

}
