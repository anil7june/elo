<?php
// src/Controller/Catalog/CurrenciesController

namespace App\Controller\Catalog;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class LanguagesController extends AppController
{

    public function initialize()
    {

        parent::initialize();
    }

    public function updateLanguage($language_id)
    {

        $users = TableRegistry::get('Users');
        $user = $users->get($this->Auth->user('id'));

        $user->language_id = $language_id;
        $saved = $users->save($user);
        if (!$saved) {
            $this->Flash->error(__('Error: unable to change your language'));
        }
        return $this->redirect($this->referer());
    }

}