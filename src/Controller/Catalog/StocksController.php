<?php
// src/Controller/Catalog/StocksController.php

namespace App\Controller\Catalog;

use App\Controller\AppController;
use App\Utility\Converter;
use App\Utility\ProductSearch;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\View\ViewBuilder;

class StocksController extends AppController
{
    public $paginate = [
        'limit' => 25,
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');

        Configure::write('CakePdf', [
            'engine' => 'CakePdf.WkHtmlToPdf',
            'binary' => '/usr/local/bin/wkhtmltopdf',
            'margin' => [
                'bottom' => 15,
                'left' => 10,
                'right' => 10,
                'top' => 10,
            ],
            'orientation' => 'landscape',
            'download' => true,
        ]);
    }

    public function index()
    {
        $this->setView();

        $active_stock_id = $this->Stocks->getRecentStock($this->Auth->user('id'))->id;
        if ($active_stock_id) {
            ProductSearch::stockListSearch($this, $active_stock_id);
        }
    }

    public function summary($stock_id)
    {
        $user_id = $this->Auth->user('id');
        $this->handlePossibleStockOwnershipViolation($user_id, $stock_id);

        $stock = $this->Stocks->getFullStockQuery($stock_id, $user_id)->first();
        $stock_summary_data = $this->Stocks->getKeyValueStockSummaryData($stock, $user_id);
        if (!TableRegistry::get('Roles')->hasGranted($user_id, 'see_value_to_hedge')) {
            unset($stock_summary_data[3]);
        }
        $back_url = Router::url(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'index']);
        $this->set(compact('stock', 'stock_summary_data', 'back_url'));
    }

    public function closeLogic($stock_id)
    {
        return $this->modifyStockStatusAction(
            $stock_id,
            [
                $this->Stocks->StockStatuses->getOpenStatusId(),
            ],
            $this->Auth->user('id'),
            $this->Stocks->StockStatuses->getClosedStatusId()
        );
    }

    public function close($stock_id)
    {
        if ($this->closeLogic($stock_id)) {
            $this->Flash->success(__('Your stock has been closed'));
            return $this->redirect(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'index']);
        } else {
            $this->Flash->error(__('Error: unable to close your stock'));
        }
    }

    public function cancelLogic($stock_id)
    {
        return $this->modifyStockStatusAction(
            $stock_id,
            [
                $this->Stocks->StockStatuses->getClosedStatusId(),
            ],
            $this->Auth->user('id'),
            $this->Stocks->StockStatuses->getCancelledStatusId()
        );
    }

    public function cancel($stock_id)
    {
        if ($this->cancelLogic($stock_id)) {
            $this->Flash->success(__('Your stock has been cancelled'));
            return $this->redirect(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'index']);
        } else {
            $this->Flash->error(__('Error: unable to cancel your stock'));
        }
    }

    public function reopenLogic($stock_id)
    {
        return $this->modifyStockStatusAction(
            $stock_id,
            [
                $this->Stocks->StockStatuses->getCancelledStatusId(),
                $this->Stocks->StockStatuses->getRejectedStatusId(),

            ],
            $this->Auth->user('id'),
            $this->Stocks->StockStatuses->getOpenStatusId()
        );
    }

    public function reopen($stock_id)
    {
        if ($this->reopenLogic($stock_id)) {
            $this->Flash->success(__('Your stock has been reopened'));
            return $this->redirect(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'index']);
        } else {
            $this->Flash->error(__('Error: unable to reopen your stock'));
        }
    }

    private function modifyStockStatusAction($stock_id, $possible_statuses, $user_id, $status_id)
    {
        $this->handlePossibleStockOwnershipViolation($user_id, $stock_id);

        if (!in_array($this->Stocks->get($stock_id)->stock_status_id, $possible_statuses)) {
            die('You can not perform this action on this stock');
        } else if ($this->Stocks->changeStockStatus($stock_id, $status_id)) {
            return true;
        } else {
            return false;
        }
    }

    private function setView()
    {
        $user_id = $this->Auth->user('id');
        $this->setAllUserStocksListView($user_id);
        $this->setActiveStockView($user_id);
        $this->setParametersData($user_id);
    }

    private function setParametersData($user_id)
    {
        $this->set([
            'content_amounts_list' => TableRegistry::get('ContentAmounts')->find('list'),
            'stock_statuses' => TableRegistry::get('StockStatuses')->find('list'),
            'user_id' => $this->Auth->user('id'),
            'currency_code' => TableRegistry::get('Currencies')->getCurrencyCodeForUser($user_id),
        ]);
    }

    private function setActiveStockView($user_id)
    {
        $active_stock = $this->Stocks->getRecentStock($user_id);

        if ($active_stock) {
            $this->setStockProductsView($active_stock);
            $this->setStockDataView($active_stock->id, $user_id);
        } else {
            $this->setStockUndefined();
        }

    }

    private function setStockDataView($stock_id, $user_id)
    {
        $query = $this->Stocks->getFullStockQuery($stock_id, $user_id);
        $stock_statuses = TableRegistry::get('StockStatuses');
        $stock = $this->Stocks->get($stock_id);

        $this->set([
            'stock' => $query->first(),
            'stock_open' => $stock->stock_status_id === $stock_statuses->getOpenStatusId(),
            'stock_closed' => $stock->stock_status_id === $stock_statuses->getClosedStatusId(),
            'stock_cancelled' => $stock->stock_status_id === $stock_statuses->getCancelledStatusId(),
            'stock_rejected' => $stock->stock_status_id === $stock_statuses->getRejectedStatusId(),
        ]);
    }

    private function setValuesToHedge($query, $user_id, $stock_id)
    {
        if ($this->userCanSeeValueToHedge($user_id)) {
            return $this->Stocks->addValuesToHedgeToFullStockQuery($query, $user_id);
        }
        return $query;
    }

    private function userCanSeeValueToHedge($user_id)
    {
        $roles = TableRegistry::get('Roles');
        return ($roles->hasGranted($user_id, 'see_value_to_hedge'));
    }

    private function setAllUserStocksListView($user_id)
    {
        $this->set([
            'stocks' => $this->Stocks->getAllUserStocks($user_id)->toList(),
        ]);
    }

    private function setStockUndefined()
    {
        $this->set([
            'stock_undefined' => true,
        ]);
    }

    private function setStockProductsView($stock)
    {
        $user_id = $this->Auth->User('id');
        $stock_products = $this->Stocks->getStockProductsQuery($stock->id, $user_id);
        $this->set(compact('stock_products'));
    }

    private function getStockImages($stockProducts)
    {

        $images = [];
        $default_image = '/img/default_product_image.jpg';
        foreach ($stockProducts as $stockProduct) {
            $images[$stockProduct->id] = $default_image;
        }

        return $images;
    }

    public function select($stock_id)
    {
        $user_id = $this->Auth->user('id');
        $this->handlePossibleStockOwnershipViolation($user_id, $stock_id);
        $users_table = TableRegistry::get('Users');
        $user = $users_table->get($user_id);
        $user->recent_stock_id = $stock_id;
        $users_table->save($user);
        return $this->redirect(['controller' => 'stocks', 'action' => 'index']);
    }

    public function create()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['USD_margin'] = Converter::convertToBaseFromUserCurrency($this->Auth->user('id'), $data['USD_margin']);
            if ($this->createStockLogic($this->Auth->user('id'), $data)) {
                return $this->redirect(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'index']);
            }
        }
    }

    public function createStockLogic($user_id, $data)
    {

        $stock_statuses = TableRegistry::get('StockStatuses');
        $default_stock_status_id = $stock_statuses->findByName('Open')->first()->id;
        $users_table = TableRegistry::get('Users');
        $user = $users_table->get($user_id);

        $new_stock = $this->Stocks->newEntity();

        $data['created'] = Time::now();
        $new_stock = $this->Stocks->patchEntity($new_stock, $data);
        $new_stock->stock_status_id = $default_stock_status_id;
        $new_stock->user_id = $user->id;

        if ($this->Stocks->save($new_stock)) {
            $user->recent_stock_id = $new_stock->id;
            $users_table->save($user);
            $this->Stocks->StockPgmPricesVersion->savePgmPricesForStock($new_stock->id);
            return $new_stock->id;
        }
        return false;
    }

    private function validStockName($name, $user_id)
    {

        $result = $this->Stocks->find()
            ->where([
                'user_id' => $user_id,
                'name' => $name,
            ]);

        return $result->count() === 0;
    }

    public function export($stock_id)
    {

        $this->viewBuilder()->options([
            'pdfConfig' => [
                'download' => 'true',
                'filename' => 'stock_' . $stock_id,
            ],
        ]);
        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->setView();
    }

    public function editStockProduct()
    {

        $this->autoRender = false;

        if ($this->request->is('Ajax')) {
            $data = $this->request->getData();
            $this->editStockProductLogic($data);
        }
    }

    private function editStockProductLogic($data)
    {
        $stock_products = TableRegistry::get('StockProducts');
        $stock_product = $stock_products->get($data['id']);
        $stock_product = $stock_products->patchEntity($stock_product, $data);
        if ($data['price'] != '') {
            $stock_product->catalogue_value = Converter::convertToBase(TableRegistry::get('Currencies')->getExchangeRateForUser($this->Auth->user('id')), $data['price']);
            $stock_product->user_value = $this->calculate_new_user_value($stock_product->stock_id, $data['price']);
        }
        $duplicate = $stock_products->find()
            ->where([
                'id <>' => $stock_product->id,
                'stock_id' => $stock_product->stock_id,
                'product_version_id' => $stock_product->product_version_id,
                'content_amount_id' => $stock_product->content_amount_id,
            ])
            ->first();
        if ($duplicate) {
            $stock_product->no_of_pcs = $stock_product->no_of_pcs + $duplicate->no_of_pcs;
            $stock_products->delete($duplicate);
        }
        return $stock_products->save($stock_product);
    }

    private function calculate_new_user_value($stock_id, $price)
    {
        $stock = $this->Stocks->findById($stock_id)->first();
        return $price * ((100.00 - $stock->margin) / 100.00) - $stock->USD_margin;
    }

    public function removeStockProduct($stock_product_id)
    {
        $stockProducts = TableRegistry::get('StockProducts');
        $stockProduct = $stockProducts->get($stock_product_id);

        if (!self::checkIfUserOwnsStockProduct($this->Auth->user('id'), $stock_product_id)) {
            $this->Flash->error(__('Error: you have no permission to perform this action.'));
        } else {
            if ($stockProduct->preproduct) {
                $deleted = $this->deletePreproduct($stockProduct);
            } else {
                $deleted = $stockProducts->delete($stockProduct);
            }
            if ($deleted) {
                $this->Flash->success(__('Product has been deleted from your stock'));
            } else {
                $this->Flash->error(__('Error: unable to remove the product'));
            }
        }
        return $this->redirect($this->referer());
    }

    public function decrementStockProduct($stock_product_id)
    {
        if (!self::checkIfUserOwnsStockProduct($this->Auth->user('id'), $stock_product_id)) {
            $this->Flash->error(__('Error: you have no permission to perform this action.'));
        } else {
            $stockProducts = $this->Stocks->StockProducts;
            $stockProduct = $stockProducts->get($stock_product_id);
            $stockProduct->no_of_pcs--;
            $stockProducts->save($stockProduct);
        }
        return $this->redirect(['controller' => 'stocks', 'action' => 'index']);

    }

    public function incrementStockProduct($stock_product_id)
    {
        if (!self::checkIfUserOwnsStockProduct($this->Auth->user('id'), $stock_product_id)) {
            $this->Flash->error(__('Error: you have no permission to perform this action.'));
        } else {
            $stockProducts = $this->Stocks->StockProducts;
            $stockProduct = $stockProducts->get($stock_product_id);
            $stockProduct->no_of_pcs++;
            $stockProducts->save($stockProduct);
        }
        return $this->redirect(['controller' => 'stocks', 'action' => 'index']);

    }

    public function refresh($stock_id)
    {
        $user_id = $this->Auth->user('id');
        $this->handlePossibleStockOwnershipViolation($user_id, $stock_id);

        $success = $this->refresh_logic($stock_id, $user_id);
        if ($success) {
            $this->Flash->success(__('Stock has been recalculated'));
        } else {
            $this->Flash->error(__('Error: unable to refresh your stock'));
        }

        return $this->redirect(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'index']);
    }

    public function refresh_logic($stock_id, $user_id)
    {
        $this->Stocks->StockPgmPricesVersion->updatePgmPricesForStock($stock_id);
        $products = $this->Stocks->StockProducts->find()
            ->where([
                'stock_id' => $stock_id,
                'preproduct' => false,
            ]);

        $result = true;
        foreach ($products as $product) {
            $result = $this->recalculate($product, $stock_id, $user_id) && $result;
        }
        $result = $this->deleteAllPreproductsFromStock($stock_id) && $result;
        return $result;
    }

    private function recalculate($stock_product, $stock_id, $user_id)
    {
        $product_id = $this->Stocks->StockProducts->getProductId($stock_product->id);
        $products = TableRegistry::get('Products');

        $product_data = $products->getProductFullDataQuery($product_id, $user_id);
        $product_data = $products->addUserPricesToQuery($product_data, $stock_id, $user_id);
        $product_data = $product_data->first();

        $exchange_rate = TableRegistry::get('Currencies')->getExchangeRateForUser($user_id);

        //this prices are in the user's currency already, so it is important to convert them back to USD
        $stock_product->catalogue_value = Converter::convertToBase($exchange_rate, $product_data->catalogue_value);
        $stock_product->user_value = Converter::convertToBase($exchange_rate, $product_data->user_value);

        return $this->Stocks->StockProducts->save($stock_product);
    }

    private function deleteAllPreproductsFromStock($stock_id)
    {
        $preproducts = $this->Stocks->StockProducts->find()
            ->where([
                'stock_id' => $stock_id,
                'preproduct' => true,
            ]);
        $result = true;
        foreach ($preproducts as $preproduct) {
            $result = $result && $this->deletePreproduct($preproduct);
        }
        return $result;
    }

    private function deletePreproduct($preproduct)
    {
        $product_versions = TableRegistry::get('ProductVersions');
        $product_version = $product_versions->get($preproduct->product_version_id);

        return $product_versions->delete($product_version) && $this->Stocks->StockProducts->delete($preproduct);
    }

    public function removeProductFromStock($stock_product_id)
    {

        if (!self::checkIfUserOwnsStockProduct($user_id, $stock_product_id)) {
            $this->Flash->error(__('Error: you have no permission to perform this action.'));
        } else {
            $stock_product = $this->Stocks->StockProducts->get($stock_product_id);
            if ($stock_product->preproduct) {
                $deleted = $this->deletePreproduct($stock_product);
            } else {
                $deleted = $this->Stocks->StockProducts->delete($stock_product);
            }
            if ($deleted) {
                $this->Flash->success(__('Product deleted from stock'));
            } else {
                $this->Flash->error(__('Unable to delete product'));
            }
        }
        return $this->redirect(['controller' => 'stocks', 'action' => 'index']);
    }

    public function removeStock($stock_id)
    {
        $user_id = $this->Auth->user('id');
        $this->handlePossibleStockOwnershipViolation($user_id, $stock_id);

        $stock = $this->Stocks->get($stock_id);
        if ($stock) {
            $this->Stocks->StockProducts->deleteAllStockProductsFromStock($stock->id);
            $deleted = $this->Stocks->delete($stock);
            if ($deleted) {
                $this->Stocks->StockPgmPricesVersion->deleteAllStockRows($stock_id);
                $this->Flash->success(__('Stock has been deleted'));
            } else {
                $this->Flash->error(__('Unable to delete your stock'));
            }
        }

        return $this->redirect(['prefix' => 'catalog', 'controller' => 'stocks', 'action' => 'index']);
    }

    private function setStockStatuses()
    {
        $stockStatusesList = TableRegistry::get('StockStatuses')->find('all');
        $stockStatuses = [];
        foreach ($stockStatusesList as $stockStatus) {
            $stockStatuses[] = __($stockStatus->name);
        }
        $this->set(compact('stockStatuses'));
    }

    private function handlePossibleStockOwnershipViolation($user_id, $stock_id)
    {
        if (!$this->checkIfUserOwnsStock($user_id, $stock_id)) {
            die(__('Error: you are trying to access stock that is not yours'));
        }
    }

    private function checkIfUserOwnsStock($user_id, $stock_id)
    {
        $result = $this->Stocks->find()
            ->where([
                'id' => $stock_id,
                'user_id' => $user_id,
            ])
            ->first();
        if ($result) {
            return true;
        }
        return false;
    }

    private function checkIfUserOwnsStockProduct($user_id, $stock_product_id)
    {
        $stock_id = $this->Stocks->StockProducts->get($stock_product_id)->stock_id;
        return self::checkIfUserOwnsStock($user_id, $stock_id);
    }

}
