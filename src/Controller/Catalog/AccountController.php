<?php
namespace App\Controller\Catalog;

use App\Controller\AppController;
use App\Utility\FileHandler;
use Cake\ORM\TableRegistry;

class AccountController extends AppController
{

    public function initialize()
    {

        parent::initialize();
    }

    public function index()
    {
        $users = TableRegistry::get('Users');
        $user = $users->get($this->Auth->User('id'));

        $countries = TableRegistry::get('Countries')->find('list');
        $this->set(compact('user', 'countries'));

        if ($this->request->is(['put', 'post'])) {
            $data = $this->request->getData();
            if (!$this->checkDataForUniquness($data, $this->Auth->user('id'), 'email')) {
                $this->Flash->error(__('Other user is using this email'));
                return;
            }
            if (!$this->checkDataForUniquness($data, $this->Auth->user('id'), 'phone')) {
                $this->Flash->error(__('Other user is using this phone number'));
                return;
            }
            $data = $this->ifEmptyPasswordUnsetIt($data); //this is very important line!!!
            $user = $users->patchEntity($user, $data);
            $user->company = $data['company'];
            $file = $data['company_logo_file'];
            $new_logo = FileHandler::saveFile($file, 'company_logo');
            if ($new_logo) {
                $user->company_logo = $new_logo;
            }
            if ($users->save($user)) {
                $this->Flash->success(__('Your account info has been updated'));
                return $this->redirect(['prefix' => 'catalog', 'controller' => 'account', 'action' => 'index']);
            }
        }
    }

    private function checkDataForUniquness($data, $user_id, $param)
    {
        $found = TableRegistry::get('Users')
            ->find()
            ->where([
                $param => $data[$param],
            ])
            ->first();
        // var_dump($data[$param]);
        // die();
        if ($found) {
            return $found->id == $user_id;
        }
        return true;
    }

    //checks if the password is empty, if it is, just ignore it!
    private function ifEmptyPasswordUnsetIt($data)
    {
        $result = $data;
        $password = $data['password'];
        if (!trim($password)) {
            $result = array_diff($data, ['password' => $password]);
        }
        return $result;
    }

}
