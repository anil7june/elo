<?php
namespace App\Controller\Catalog;

use App\Controller\AppController;

class ContactController extends AppController
{
    public function initialize()
    {

        parent::initialize();
    }

    public function index()
    {
        if ($this->request->is('post')) {
            $message_sent = $this->sendMessage($this->request->getData());
            if ($message_sent) {
                return $this->redirect(['prefix' => 'catalog', 'controller' => 'contact', 'action' => 'messageSent']);
            }
        }
    }

    private function sendMessage($data)
    {
        return true; //hardcodding for now
    }

    public function messageSent()
    {

    }
}
