<?php
// src/Controller/Catalog/CurrenciesController

namespace App\Controller\Catalog;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class CurrenciesController extends AppController
{

    public function initialize()
    {

        parent::initialize();

    }

    public function updateCurrency($currency_id)
    {
        $this->autoRender = false;

        $users = TableRegistry::get('Users');
        $user = $users->get($this->Auth->user('id'));
        $user->currency_id = $currency_id;
        $saved = $users->save($user);
        if (!$saved) {
            $this->Flash->error(__('Error: unable to change your currency.'));
        } else {
            $this->Flash->success(__('Currency has been changed'));
        }

        return $this->redirect($this->referer());
    }
}