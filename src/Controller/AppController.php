<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\I18n\I18n;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */

    public function initialize()
    {

        parent::initialize();

        $this->loadComponent('Prices');
        $this->loadComponent('Flash');
        $this->loadComponent('Rates');
        $this->loadComponent('Auth', [
            'authorize' => 'Controller',
            'loginAction' => [
                'prefix' => false,
                'controller' => 'Users',
                'action' => 'login',
            ],
            'loginRedirect' => [
                'prefix' => false,
                'controller' => 'Users',
                'action' => 'disclaimer',
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
            ],
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password',
                    ],
                ],
            ],
        ]);
    }

    public function beforeFilter(Event $event)
    {

        $this->Auth->allow(['ajax']);
        $this->set('Auth', $this->Auth);

        if ($this->request->params['action'] == "home") {
            $this->Auth->config('authError', false);
        }

        $roles = TableRegistry::get('Roles');

        $dashboard_permission = false;
        $showOwnLogo = false;
        $logoChecked = false;
        $coins;

        if ($this->Auth->user()) {
            $user_id = $this->Auth->user('id');
            $dashboard_permission = $roles->hasGranted($user_id, 'admin_dashboard_index');
            $this->setLocale($this->Auth->user('id'));

            $user = TableRegistry::get('Users')->get($user_id);
            $coins = $user->coins;

            $this->setCurrenciesView();
            $this->setLanguagesView();
            $this->setMetalPricesView();
            $this->setLogoView();
        }
        $this->set(compact('coins', 'user_id', 'dashboard_permission'));
    }

    private function setLogoView()
    {
        $user = TableRegistry::get('Users')->get($this->Auth->user('id'));
        if ($user->show_logo_in_header && $user->logo_allowed && $user->company_logo) {
            $logo_path = '/uploads/company_logo/' . $user->company_logo;
        } else {
            $logo_path = '/img/elemental-logo.png';
        }

        $this->set(compact('logo_path'));
    }

    private function setCurrenciesView()
    {
        $currencies_table = TableRegistry::get('Currencies');
        $user_currency_id = TableRegistry::get('Users')->get($this->Auth->user('id'))->currency_id;
        $user_currency_code = $currencies_table->get($user_currency_id)->code;
        $currencies = $currencies_table->joinWithExchangeRates();
        $this->set(compact('currencies', 'user_currency_id', 'user_currency_code'));
    }

    private function setLanguagesView()
    {
        $languages_table = TableRegistry::get('Languages');
        $languages = $languages_table->find()
            ->select(['id', 'name']);
        $user_language_id = TableRegistry::get('Users')->get($this->Auth->user('id'))->language_id;
        $user_language_name = $languages_table->get($user_language_id)->name;
        $this->set(compact('languages', 'user_language_id', 'user_language_name'));
    }

    private function setMetalPricesView()
    {
        $exchange_rate = TableRegistry::get('Currencies')->getExchangeRateForUser($this->Auth->user('id'));
        $pgm_prices = TableRegistry::get('PgmPrices');
        $metal_prices = [
            'Pt' => $pgm_prices->getElementPrice('Pt') * $exchange_rate,
            'Pd' => $pgm_prices->getElementPrice('Pd') * $exchange_rate,
            'Rh' => $pgm_prices->getElementPrice('Rh') * $exchange_rate,
        ];
        $this->set(compact('metal_prices'));
    }

    public function isAuthorized($user)
    {

        $roles = TableRegistry::get('Roles');
        if ($this->Auth->user()) {
            $user_id = $this->Auth->user('id');
        }

        $permission = \strtolower($this->request->param('controller')) . '_' . $this->request->param('action');
        if ($this->request->param('prefix')) {
            $permission = $this->request->param('prefix') . '_' . $permission;
        }

        return $roles->hasGranted($user_id, $permission);

    }

    protected function isAuthError()
    {
        return true;
    }

    private function getVersion()
    {

        $file = new File(WWW_ROOT . DS . 'version.txt', true, 0644);
        $result = $file->read();
        $file->close();

        return $result;
    }

    private function setLocale($user_id)
    {

        $locale = TableRegistry::get("Users")->find()
            ->select(['Users.id', 'languages.code'])
            ->join(['languages' => [
                'table' => 'languages',
                'type' => 'LEFT',
                'conditions' => 'Users.language_id = languages.id',
            ]])
            ->where(['Users.id' => $user_id])
            ->first()->languages['code'];

        I18n::setLocale($locale);
    }

}
