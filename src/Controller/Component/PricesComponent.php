<?php
// src/Controller/Component/PricesComponent.php
namespace App\Controller\Component;

use Cake\Controller\Component;
use App\Utility\UserPricingData;
use App\Utility\Converter;
use App\Utility\ProductPricingData;
use Cake\ORM\TableRegistry;

class PricesComponent extends Component {

  public function getProductPrices($products, $type_of_price, $user_id) {

		if($type_of_price == 'gross_value') {
			return $this->getGrossValues($products, $user_id);
		} else if($type_of_price == 'catalogue_value') {
			return $this->getCatalogueValues($products, $user_id);
		} else if($type_of_price == 'user_value') {
			return $this->getUserValues($products, $user_id);
		}
	}

	private function getGrossValues($products, $user_id) {

		$result = [];
		foreach ($products as $product) {
			$result[$product->id] = Converter::convert($this->Prices->grossValue($product->id), $user_id);
		}

		return $result;
	}

	private function getCatalogueValues($products, $user_id) {

		$active_user_id = $this->Auth->user('id');
		$result = [];
		foreach ($products as $product) {
			$result[$product->id] = Converter::convert($this->Prices->catalogueValue($product->id, $user_id), $user_id);
		}
		return $result;
	}

	private function getUserValues($products, $user_id) {

		$result = [];
		foreach ($products as $product) {
			$catalogue_value = $this->Prices->catalogueValue($product->id, $user_id);
			$result[$product->id] = Converter::convert($this->Prices->userValue($catalogue_value, $user_id), $user_id);
		}
		return $result;
	}

  public function catalogueValue($product_id, $user_id) {

		$productData = new ProductPricingData();
		$productData->setProductData($product_id);

		$userData = new UserPricingData();
		$userData->setUserData($user_id);

    $users = TableRegistry::get('Users');
    $user = $users->get($user_id);

		$catalogueValue = $this->calculateCatalogueValue($productData, $userData);

		return (float)\number_format($catalogueValue, 2);
	}

	public function grossValue($product_id) {

		$productData = new ProductPricingData();
		$productData->setProductData($product_id);


		$grossValue = $this->calculateGrossValue($productData);

		return (float)\number_format($grossValue, 2);

	}


  public function userValue($catalogueValue, $margin) {

    $result = $catalogueValue * (1.0 - 0.01 * $margin);
    return (float)\number_format($result);
  }


  private $_gramOunce = 32.1507;

  private function calculateGrossValue($productData) {

    $elements = $productData->_elements;

    $content = $productData->_content;
    $weight = $productData->_weight;
    $prices = $productData->_prices;

    $result = 0;
    foreach ($elements as $element) {
      $result += ($weight*$content[$element]*$prices[$element]*$this->_gramOunce)/(10**6);
    }
    return $result;
  }


  private function calculateCatalogueValue($productData, $userData) {

    $elements = $userData->_elements;

    $RR = $userData->_RR;
    $PD = $userData->_PD;
    $RC = $userData->_RC;
    $TC = $userData->_TC;
    $DU_pp = $userData->_DU_pp;
    $DU_value = $userData->_DU_value;

    $content = $productData->_content;
    $weight = $productData->_weight;
    $prices = $productData->_prices;
    $PAD = $productData->_discounts;

    $result = 0;
    foreach ($elements as $element) {
      $iteration = (float)( $RR[$element]*$weight*$content[$element]*$this->_gramOunce*
        ($prices[$element] - $PD[$element] - $PAD[$element]) ) / (10**6)
        - ( $weight*$content[$element]*$RR[$element]*$RC[$element] ) / (10**6);
      $result += $iteration;
    }
    \var_dump($prices[$element], $PD[$element], $PAD[$element]);
    // $result -= ( $TC*$weight + ($DU_value*(100.0 - $DU_pp))/100.0 );

    return $result;
  }

}
