<?php

// src/Controller/Component/RatesComponent.php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

require_once APP . 'Utility/simple_html_dom.php';

class RatesComponent extends Component
{

    public function updateExchangeRates(array $currencyCodes)
    {

        $currencies = TableRegistry::get('Currencies');

        $csv = file_get_contents('http://www.floatrates.com/daily/usd.xml');
        $rows = explode("\n", $csv);

        foreach ($rows as $row) {

            $parse = preg_match('/<title>1 USD = ([0-9]*.[0-9]*) ([A-Z]*)<\/title>$/', $row, $matches);
            if ($parse && in_array($matches[2], $currencyCodes)) {
                $currencyCode = $matches[2];
                $exchangeRate = $matches[1];
                $currencies->updateCurrencyRate($currencyCode, $exchangeRate);
            }
        }

    }

    public function updateExchangeRatesFromNBP(array $currencyCodes)
    {

        $response = \file_get_contents('http://api.nbp.pl/api/exchangerates/tables/A/?format=json');
        $response_json = \json_decode($response);

        $currencies = TableRegistry::get('Currencies');
        $currenciesArray = $response_json[0]->rates;

        $usdRate = $this->getCurrencyRateFromNBPResponse("USD", $response_json)->mid;
        $currencies->updateCurrencyRate('PLN', $usdRate);

        foreach ($currenciesArray as $c) {
            if (\in_array($c->code, $currencyCodes) && $c->code !== "USD") {
                $tempRate = $c->mid;
                $rateToSave = $usdRate / $tempRate;
                $currencies->updateCurrencyRate($c->code, $rateToSave);
            }
        }
    }

    public function updateElementPrices(array $elementFullNames)
    {

        $html = \file_get_html('http://www.platinum.matthey.com/');
        $pgmPrices = TableRegistry::get('PgmPrices');

        foreach ($html->find('li') as $element0) {
            $is_find = false;
            foreach ($element0->find('a[class=chart-trigger]') as $element) {
                $is_find = true;
            }

            if ($is_find) {
                $parse = preg_match('/> *([a-zA-z]{3,12})<\/a> {0,3}<span> {0,20}([0-9]{1,6}) {0,3}<\/span>/', $element0, $matches);
                if ($parse && in_array($matches[1], $elementFullNames)) {
                    $elementFullName = $matches[1];
                    $elementPrice = $matches[2];
                    $pgmPrices->setElementPrice($elementFullName, $elementPrice);
                }
            }
        }
    }

    private function getCurrencyRateFromNBPResponse($currencyCode, $json_response)
    {
        $currencyArray = $json_response[0]->rates;

        foreach ($currencyArray as $c) {
            if ($c->code == $currencyCode) {
                return $c;
            }

        }
    }

}
