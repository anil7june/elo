<?php
// src/Model/Table/MakesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class MakesTable extends Table
{

    public function initialize(array $config)
    {
        $this->hasMany('ProductVersions');
        $this->hasMany('Preproducts');
        $this->hasMany('CatMakes');
    }

    public function getAllModelsList($make_id)
    {
        return $this->CatMakes->find('list')
            ->where([
                'make_id' => $make_id,
            ]);
    }
}
