<?php
// src/Modet/Table/UserStatusesTable.php

namespace App\Model\Table;

use Cake\ORM\Table;

class UserStatusesTable extends Table
{

    public function initialize(array $config)
    {
        $this->hasMany('Users');
    }

    public function getActiveStatus()
    {
        return $this->findByName('active')->first();
    }
}
