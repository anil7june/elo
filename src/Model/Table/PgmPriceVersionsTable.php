<?php
// src/Model/Table/PgmPriceVersionsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PgmPriceVersionsTable extends Table {

  public function initialize(array $config) {

    $this-> belongsTo('PgmPrices');
  }
}
