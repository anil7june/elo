<?php
// src/Model/Table/RolesTable
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class RolesTable extends Table
{

    public function initialize(array $config)
    {
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Permissions', [
            'joinTable' => 'permissions_roles',
            'through' => 'PermissionsRoles',
        ]);

        $this->hasMany('PermissionsRoles');
        $this->addBehavior('Timestamp');
    }

    public function hasGranted($user_id, $permission_name)
    {
        $users = TableRegistry::get('Users');
        $user = $users->get($user_id);

        $permission = $this->Permissions->findByName($permission_name)->first();
        $role = $this->get($user->role_id);

        $relation = $this->PermissionsRoles->find()
            ->where([
                'role_id' => $role->id,
                'permission_id' => $permission->id,
            ]);

        return $relation->count() > 0;
    }

    public function getIdOfUserRole($user_id)
    {
        return TableRegistry::get('Users')->get($user_id)->role_id;
    }

    public function getUserRoleId()
    {
        return $this->findByName('User')->first()->id;
    }

    public function getAdminRoleId()
    {
        return $this->findByName('Admin')->first()->id;
    }

    public function getSuperAdminRoleId()
    {
        return $this->findByName('SuperAdmin')->first()->id;
    }

    public function ifUserIsSuperAdmin($user_id)
    {
        $role_id = $this->getIdOfUserRole($user_id);
        return $role_id == $this->getSuperAdminRoleId();
    }

    public function ifUserIsAdminOrSuperAdmin($user_id)
    {
        $role_id = $this->getIdOfUserRole($user_id);
        return $role_id == $this->getAdminRoleId() || $role_id == $this->getSuperAdminRoleId();
    }
}
