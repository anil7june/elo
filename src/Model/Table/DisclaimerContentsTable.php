<?php
// src/Model/Table/DisclaimerContentsTable.php
namespace App\Model\Table;

use Cake\I18n\Time;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class DisclaimerContentsTable extends Table
{

    public function initialize(array $config)
    {
        $this->belongsTo('Disclaimers');
        $this->belongsTo('Languages');
    }

    public function getContent($disclaimer_id, $language_id)
    {

        $query = $this->find()
            ->where([
                'disclaimer_id' => $disclaimer_id,
                'language_id' => $language_id,
            ]);

        return $query;
    }

    public function getFormattedContent($disclaimer_id, $language_id)
    {

        $content_str = $this->getContent($disclaimer_id, $language_id)->first()->content;
        $header_and_content = explode("</h1>", explode("<h1>", $content_str)[1]);

        $result = [
            'header' => $header_and_content[0],
            'content' => [],
            'last_updated' => Time::parse(TableRegistry::get('Disclaimers')->get($disclaimer_id)->created)->nice(),
        ];

        $content = explode('<h3 style="text-align:left">', $header_and_content[1]);
        foreach ($content as $input) {
            if (strpos($input, "</h3>") !== false) {
                $result['content'][] = $this->split_parargaph($input);
            }
        }
        return $result;
    }

    private function split_parargaph($input)
    {
        $split = explode("</h3>", $input);

        $result = [
            'header' => $split[0],
            'content' => [],
        ];
        $content = explode("<p>", $split[1]);
        foreach ($content as $input) {
            $formated_input = trim(explode("</p>", $input)[0]);
            if ($formated_input !== '') {
                $result['content'][] = $formated_input;
            }
        }
        return $result;
    }

}
