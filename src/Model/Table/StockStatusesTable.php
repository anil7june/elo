<?php
// src/Model/Table/StockStatusesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class StockStatusesTable extends Table
{

    public function initialize(array $config)
    {

        $this->hasMany('Stocks');
    }

    public function getOpenStatusId()
    {
        return $this->findByName('Open')->first()->id;
    }

    public function getClosedStatusId()
    {
        return $this->findByName('Closed')->first()->id;
    }

    public function getRejectedStatusId()
    {
        return $this->findByName('Rejected')->first()->id;
    }

    public function getAcceptedStatusId()
    {
        return $this->findByName('Accepted')->first()->id;
    }

    public function getCancelledStatusId()
    {
        return $this->findByName('Cancelled')->first()->id;
    }

    public function getButtonAction($status_id) {
        if($status_id == $this->getOpenStatusId()) {
          return 'Close';
        } else if ($status_id == $this->getClosedStatusId()) {
          return 'Cancel';
        } else if($status_id == $this->getCancelledStatusId()) {
          return 'Reopen';
        } else {
          return '';
        }
    }

}
