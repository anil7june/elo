<?php
// src/Model/Table/ExchangeRateVersionsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ExchangeRateVersionsTable extends Table {

  public function initialize(array $config) {

    $this->belongsTo('Currencies');
  }
}
