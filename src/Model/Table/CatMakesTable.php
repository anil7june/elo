<?php
// src/Model/Table/CatMakesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class CatMakesTable extends Table
{

    public function initialize(array $config)
    {
        $this->hasMany('ProductVersions');
        $this->belongsTo('Makes');
    }
}
