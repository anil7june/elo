<?php
// src/Model/Table/CompaniesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class CompaniesTable extends Table
{
    public function initialize(array $config)
    {
        $this->hasMany('Users');
    }
}
