<?php
// src/Model/Table/ImagesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class ImagesTable extends Table
{

    public function initialize(array $config)
    {
        $this->belongsTo('Products');
    }

    public function saveNewImage($file_name, $product_id)
    {
        $new_image = $this->newEntity([
            'product_id' => $product_id,
            'file_name' => $file_name,
        ]);
        return $this->save($new_image);
    }

    public function addDefaultImageToProduct($product_id)
    {
        $products = TableRegistry::get('Products');
        $product = $products->get($product_id);
        $product->image_id = $this->getDefaultImageId();
        return $products->save($product);

    }

    public function getDefaultImageId()
    {
        return $this->getDefaultImage()->first()->id;
    }

    public function getDefaultImage()
    {
        return $this->find()
            ->where(['is_default' => 1]);
    }

    public function bindImageToProduct($product_id)
    {
        $products = TableRegistry::get('Products');

        $product = $products->get($product_id);
        $image = $this->find()
            ->where(['product_id' => $product_id])
            ->first();

        if ($image) {
            $product->image_id = $image->id;
        } else {
            $product->image_id = $this->getDefaultImageId();
        }

        return $products->save($product);
    }

    public function removeImage($image_id)
    {
        $image_to_remove = $this->get($image_id);
        return $this->delete($image_to_remove);
    }

    public function getFileName($image_id)
    {
        return $this->get($image_id)->file_name;
    }

    public function getImagesForProduct($product_id)
    {
        return $this->find()
            ->where([
                'product_id' => $product_id,
            ]);
    }
}
