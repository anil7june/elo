<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class UsersTable extends Table
{

    public function initialize(array $config)
    {
        $this->belongsTo('Roles');
        $this->belongsTo('Countries');
        $this->belongsTo('Disclaimers');
        $this->belongsTo('Currencies');
        $this->belongsTo('Languages');
        $this->belongsTo('UserStatuses');
        $this->belongsTo('Companies');

        $this->hasMany('Stocks');
        $this->hasOne('Tokens');
    }

    public function validationDefault(Validator $validator)
    {

        $validator = new Validator();

        $validator
            ->add('email', [
                'notBlank' => [
                    'rule' => 'notBlank',
                    'message' => __('Please enter your email address'),
                ],
                'unique' => [
                    'rule' => function ($value) {
                        $find = $this->findByEmail($value)->toArray();
                        return count($find) === 0;
                    },
                    'message' => __('This email already exists'),
                    'on' => function ($context) {
                        return !$context['data']['id'];
                    },
                ],
                'validFormat' => [
                    'rule' => 'email',
                    'message' => __('Email must be valid'),
                ],
            ])
            ->add('supervisor_email', [
                'notBlank' => [
                    'rule' => 'notBlank',
                    'message' => __('Please enter your supervisor email'),
                ],
                'unique' => [
                    'rule' => function ($value) {
                        $find = $this->findByEmail($value)->first();
                        if (!$find) {
                            return false;
                        }
                        return ($find->role_id == $this->Roles->getSuperAdminRoleId());

                    },
                    'message' => __('There is no supervisor with email you have given'),
                    'on' => function ($context) {
                        return !$context['data']['id'];
                    },
                ],
                'validFormat' => [
                    'rule' => 'email',
                    'message' => __('Email must be valid'),
                ],
            ])
            ->add('phone', [
                'notBlank' => [
                    'rule' => 'notBlank',
                    'message' => __('Please enter phone number'),
                ],
                'unique' => [
                    'rule' => function ($value) {
                        $find = $this->findByPhone($value)->toArray();
                        return count($find) === 0;
                    },
                    'message' => __('This phone number already exists'),
                    'on' => function ($context) {
                        return !$context['data']['id'];
                    },
                ],
            ])
            ->regex('password', "~(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])^[a-zA-Z0-9]{8,15}$~", __('Password must be at least 8 characters long, at most 15 characters, contains only letters and digits: at least one digit, one uppercase letter and one lowercase letter'))
            ->regex('phone', '/^\+?\d+$/', __('Phone number should be a non-spaced sequence of digits with potential + sign at the beginning'))
            ->add('confirm_password',
                'compareWith', [
                    'rule' => ['compareWith', 'password'],
                    'message' => __('Passwords do not match'),
                ])
            ->add('disclaimer_id', 'custom', [
                'rule' => function ($value, $context) {
                    return $value != 0;
                },
                'message' => __('Please, accept terms of use'),
            ])
            ->notEmpty('first_name', __('Please enter your first name'))
            ->notEmpty('last_name', __('Please enter your last name'))
            ->notEmpty('company', __('Please enter your company name'))
            ->notEmpty('city', __('Please enter city you live in'))
            ->notEmpty('country_id', __('Please choose your country'))
            ->notEmpty('supervisor_is', __('This field can not be empty'))
            ->notEmpty('password', __('Password can not be empty'))
            ->sameAs('password', 'password_2', __('Passwords are not equal'))
            ->allowEmptyString('password', 'update')
            ->allowEmptyString('password_2', 'update')
            ->allowEmptyString('current_password')
            ->add('current_password', 'custom', [
                'rule' => function ($value, $context) {
                    $user = $this->get($context['data']['id']);
                    if ($user) {
                        if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                            return true;
                        }
                    }
                    return false;
                },
                'message' => __('The old password does not match the current password'),
            ]);

        $elements = ['pt', 'pd', 'rh'];
        $pricing_parameters = ['RC', 'PD', 'RR', 'LC'];
        $treatment_parameters = ['TC', 'DU_pp', 'DU_value'];
        $params = [];
        foreach ($pricing_parameters as $pricing_param) {
            foreach ($elements as $elem) {
                $params[] = $elem . '_' . $pricing_param;
            }
        }
        $params = array_merge($params, $treatment_parameters);

        foreach ($params as $param) {
            $validator
                ->allowEmptyString($param, __('Insert value'))
                ->add($param, [
                    'numeric' => [
                        'rule' => 'numeric',
                        'message' => __('Insert number'),
                    ],
                ]);
        }
        return $validator;
    }

    public function getAllUsersQuery($conditions = [])
    {
        $result = $this->find()
            ->contain(['Countries', 'Roles', 'UserStatuses'])
            ->where($conditions);
        return $result;
    }

    public function getUserExchangeRate($user_id)
    {
        $user = $this->get($user_id);

        $currencies = TableRegistry::get('Currencies');
        $currency = $currencies->get($user->currency_id);
        $exchange_rate = $currencies->getExchangeRateForUser($user_id);

        return $exchange_rate;
    }

}
