<?php
// src/Model/Table/ProductStatusesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductStatusesTable extends Table {

  public function initialize(array $config) {

    $this->hasMany('Products');
  }
}
