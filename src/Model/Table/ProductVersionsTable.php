<?php
// src/Model/Table/ProductsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class ProductVersionsTable extends Table
{

    public function initialize(array $config)
    {

        $this->belongsTo('Products')
            ->setForeignKey('product_id')
            ->setBindingKey('id');

        $this->belongsTo('Makes')
            ->setForeignKey('make_id')
            ->setBindingKey('id');

        $this->belongsTo('CatMakes')
            ->setForeignKey('cat_make_id')
            ->setBindingKey('id');

        $this->belongsTo('Types')
            ->setForeignKey('type_id')
            ->setBindingKey('id');

        $this->hasMany('StockProducts')
            ->setForeignKey('product_version_id')
            ->setBindingKey('id');
    }

    public function getProductVersion($product_id)
    {

        $product = TableRegistry::get('Products')->get($product_id);
        return $this->find()
            ->where([
                'product_id' => $product->id,
                'version' => $product->version,
            ])
            ->first();
    }

    public function queryByConditions(array $conditions)
    {

        $result = $this->find('all')
            ->select([
                'product_id',
                'ecr_code',
                'Makes.name',
                'Types.name',
                'weight',
            ])
            ->contain([
                'Makes',
                'Types',
            ])
            ->where($conditions);

        return $result;
    }

    public function createNewVersion($version_id)
    {

        $oldVersion = $this->get($version_id)->toArray();
        $newVersion = $this->newEntity($oldVersion);

        $newVersion->created = Time::now();
        $newVersion->version = $oldVersion->version + 1;

        return $newVersion;
    }

}
