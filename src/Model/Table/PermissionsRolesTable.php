<?php
// src/Model/Table/PermissionsRolesTable
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;


class PermissionsRolesTable extends Table {

  public function initialize(array $config) {

    $this->belongsTo('Roles');
    $this->belongsTo('Permissions');
  }

}
