<?php
// src/Model/Table/StockProductsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class StockProductsTable extends Table
{

    public function initialize(array $config)
    {

        $this->belongsTo('Stocks')
            ->setForeignKey('stock_id')
            ->setBindingKey('id');

        $this->belongsTo('ProductVersions')
            ->setForeignKey('product_version_id')
            ->setBindingKey('id');

        $this->belongsTo('ContentAmounts')
            ->setForeignKey('content_amount_id')
            ->setBindingKey('id');
    }

    public function getStockProductFromStock($product_version_id, $stock_id)
    {
        $product = $this->find('all')
            ->where([
                'stock_id' => $stock_id,
                'product_version_id' => $product_version_id,
            ])
            ->first();

        return $product;
    }

    public function incrementNoOfPcs($stock_product_id)
    {
        $stockProduct = $this->get($stock_product_id);
        $stockProduct->no_of_pcs += 1;
        return $this->save($stockProduct);
    }

    public function decrementNoOfPcs($stock_product_id)
    {
        $stockProduct = $this->get($stock_product_id);
        $stockProduct->no_of_pcs -= 1;
        return $this->save($stockProduct);
    }

    public function deleteAllStockProductsFromStock($stock_id)
    {

        $stockProducts = $this->find('all')
            ->where(['stock_id' => $stock_id]);

        foreach ($stockProducts as $stockProduct) {
            if ($stockProduct->preproduct) {
                $product_version_to_delete = $this->ProductVersions->get($stockProduct->product_version_id);
                $this->ProductVersions->delete($product_version_to_delete);
            }
            $this->delete($stockProduct);
        }
    }

    public function getProductId($stock_product_id)
    {

        $stock_product = $this->get($stock_product_id);
        $product_version = $this->ProductVersions->get($stock_product->product_version_id);
        return $product_version->product_id;
    }

    public function getStockProductDetailData($stock_product_id)
    {

        return $this->find()
            ->select([
                'make.name',
                'cat_make.name',
                'StockProducts.catalogue_value',
                'StockProducts.user_value',
                'product_version.cat_code',
                'product_version.weight',
                'StockProducts.no_of_pcs',
                'StockProducts.id',
                'StockProducts.stock_id',
                'content_amount.name',
                'content_amount.id',
            ])
            ->join([
                'product_version' => [
                    'table' => 'product_versions',
                    'type' => 'LEFT',
                    'conditions' => [
                        'product_version.id = StockProducts.product_version_id',
                    ],
                ],
                'make' => [
                    'table' => 'makes',
                    'type' => 'LEFT',
                    'conditions' => [
                        'make.id = product_version.make_id',
                    ],
                ],
                'cat_make' => [
                    'table' => 'cat_makes',
                    'type' => 'LEFT',
                    'conditions' => [
                        'cat_make.id = product_version.cat_make_id',
                    ],
                ],
                'content_amount' => [
                    'table' => 'content_amounts',
                    'type' => 'LEFT',
                    'conditions' => [
                        'content_amount.id = StockProducts.content_amount_id',
                    ],
                ],
            ])
            ->where([
                'StockProducts.id' => $stock_product_id,
            ]);
    }
}
