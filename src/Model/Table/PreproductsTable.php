<?php
// src/Model/Table/PreproductsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PreproductsTable extends Table {

  public function initialize(array $config) {

    $this-> belongsTo('Makes');
    $this->belongsTo('Stocks');
  }

  public function findByStockId($id) {

    $result_query = $this->find()
      ->where(['stock_id' => $id]);
    return $result_query;
  }
}
