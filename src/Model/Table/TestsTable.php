<?php
// src/Model/Table/TestsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class TestsTable extends Table
{

    public function initialize(array $config)
    {
        $this->belongsTo('Products');
        $this->belongsTo('AnalysisTypes');
    }

    public function getElementContentFromTest($element, $test_id)
    {

        if ($element === 'Pt') {
            return $this->get($test_id)->pt;
        } else if ($element === 'Pd') {
            return $this->get($test_id)->pd;
        } else if ($element === 'Rh') {
            return $this->get($test_id)->rh;
        }
        return [];
    }
}
