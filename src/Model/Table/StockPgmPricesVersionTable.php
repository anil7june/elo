<?php
// src/Model/Table/StockPgmPricesVersionTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class StockPgmPricesVersionTable extends Table
{

    public function initialize(array $config)
    {
        $this->belongsTo('Stocks');
        $this->belongsTo('PgmPrices');
    }

    public function savePgmPricesForStock($stock_id)
    {
        $prices = $this->PgmPrices->find('all');
        foreach ($prices as $price) {
            $this->savePgmPrice($price, $stock_id);
        }
    }

    public function updatePgmPricesForStock($stock_id)
    {
        $rows_to_delete = $this->find()
            ->where([
                'stock_id' => $stock_id,
            ]);
        foreach ($rows_to_delete as $row) {
            $this->delete($row);
        }

        $this->savePgmPricesForStock($stock_id);
    }

    private function savePgmPrice($price, $stock_id)
    {
        $row = $this->newEntity([
            'stock_id' => $stock_id,
            'pgm_price_id' => $price->id,
            'price' => $this->PgmPrices->getElementPrice($price->name),
        ]);

        $this->save($row);
    }

    public function getElementPrice($stock_id, $element_name)
    {
        $pgm_price_id = $this->PgmPrices->findByName($element_name)->first()->id;
        return $this->find()
            ->where([
                'stock_id' => $stock_id,
                'pgm_price_id' => $pgm_price_id,
            ])
            ->first()
            ->price;
    }

    public function getStockPricesArray($stock_id)
    {
        return [
            'pt' => $this->getElementPrice($stock_id, 'Pt'),
            'pd' => $this->getElementPrice($stock_id, 'Pd'),
            'rh' => $this->getElementPrice($stock_id, 'Rh'),
        ];
    }

    public function deleteAllStockRows($stock_id)
    {

        $rows_to_delete = $this->find()
            ->where(['stock_id' => $stock_id]);
        foreach ($rows_to_delete as $row) {
            $this->delete($row);
        }
    }
}
