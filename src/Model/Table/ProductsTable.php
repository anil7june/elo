<?php
// src/Model/Table/ProductsTable.php
namespace App\Model\Table;

use App\Utility\PriceFormulaGenerator;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class ProductsTable extends Table
{

    public function initialize(array $config)
    {

        $this->hasMany('ProductVersions')
            ->setForeignKey('product_id')
            ->setBindingKey('id');

        $this->hasMany('Tests')
            ->setForeignKey('product_id')
            ->setBindingKey('id');

        $this->belongsTo('ProductStatuses')
            ->setForeignKey('product_status_id')
            ->setBindingKey('id');

        $this->hasMany('Images');
    }

    public function emptySet()
    {
        return $this->find()
            ->where([
                'id' => -100,
            ]);
    }

    public function getProductContent($product_id)
    {

        $product = $this->findById($product_id)
            ->contain(['Tests'])
            ->first();

        if (!$product->test_id) {
            return [];
        }

        $test = $product->tests[0];

        $result = [
            'Pt' => $test->pt,
            'Pd' => $test->pd,
            'Rh' => $test->rh,
        ];

        return $result;
    }

    public function isDeleted($id)
    {

        $product = $this->get($id);
        return $product->deleted;
    }

    public function getLiveSearchQuery($conditions, $search_params)
    {

        $resultQuery = $this->find()
            ->select($search_params)
            ->join([
                'product_version' => [
                    'table' => 'product_versions',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Products.id = product_version.product_id',
                        'Products.version = product_version.version',
                    ],
                ],
                'make' => [
                    'table' => 'makes',
                    'type' => 'LEFT',
                    'conditions' => [
                        'make.id = product_version.make_id',
                    ],
                ],
            ])
            ->where($conditions)
            ->limit(5);

        return $resultQuery;
    }

    public function getFullProductsDataQuerry(array $select = [])
    {
        $select_default = [
            'product_version.ecr_code',
            'product_version.cat_code',
            'product_version.weight',
            'product_version.id',
            'product_version.description',
            'id',
            'make.name',
            'cat_make.name',
            'product_status.name',
            'image.file_name',
        ];

        $select_conditions = array_merge($select_default, $select);
        $resultQuery = $this->find()
            ->select($select_conditions)
            ->join([
                'product_version' => [
                    'table' => 'product_versions',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Products.id = product_version.product_id',
                        'Products.version = product_version.version',
                    ],
                ],
                'product' => [
                    'table' => 'products',
                    'type' => 'LEFT',
                    'conditions' => [
                        'product_version.product_id = product.id',
                    ],
                ],
                'image' => [
                    'table' => 'images',
                    'type' => 'LEFT',
                    'conditions' => [
                        'image.id = product.image_id',
                    ],
                ],
                'make' => [
                    'table' => 'makes',
                    'type' => 'LEFT',
                    'conditions' => [
                        'make.id = product_version.make_id',
                    ],
                ],
                'test' => [
                    'table' => 'tests',
                    'type' => 'LEFT',
                    'conditions' => [
                        'test.id = Products.test_id',
                    ],
                ],
                'cat_make' => [
                    'table' => 'cat_makes',
                    'type' => 'LEFT',
                    'conditions' => [
                        'product_version.cat_make_id = cat_make.id',
                    ],
                ],
                'analysis_type' => [
                    'table' => 'analysis_types',
                    'type' => 'LEFT',
                    'conditions' => [
                        'test.analysis_type_id = analysis_type.id',
                    ],
                ],
                'product_status' => [
                    'table' => 'product_statuses',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Products.product_status_id = product_status.id',
                    ],
                ],
            ])
            ->where([
                'Products.deleted' => false,
            ]);
        return $resultQuery;
    }

    public function findByMakeName($name)
    {

        $query = $this->getFullProductsDataQuerry();
        $query = $query->where([
            'make.name' => $name,
        ]);
        return $query;
    }

    public function findByTypeName($name)
    {

        $query = $this->getFullProductsDataQuerry();
        $query = $query->where([
            'type.name' => $name,
        ]);
        return $query;
    }

    public function findByECRCode($code)
    {

        $query = $this->getFullProductsDataQuerry();
        $query = $query->where([
            'product_version.ecr_code' => $code,
        ]);
        return $query;
    }

    public function findByKeyWords($key_words, $live_search = false)
    {

        $search_parameters = [
            'product_version.cat_code',
            'make.name',
        ];

        $param_expresions = [];
        foreach ($key_words as $key_word) {
            $or_expresion = [];
            foreach ($search_parameters as $param) {
                $or_expresion[] = [$param . ' LIKE' => '%' . $key_word . '%'];
            }
            $param_expresions[] = ['OR' => $or_expresion];
        }
        $conditions = ["AND" => $param_expresions];

        if ($live_search) {
            $query = $this->getLiveSearchQuery($conditions, $search_parameters);
        } else {
            $query = $this->findByConditions($conditions);
        }

        return $query;
    }

    public function getUserExchangeRate($user_id)
    {

        $users = TableRegistry::get('Users');
        $user = $users->get($user_id);

        $currencies = TableRegistry::get('Currencies');
        $currency = $currencies->get($user->currency_id);
        $exchange_rate = $currencies->getExchangeRateForUser($user_id);

        return $exchange_rate;
    }

    private function generateCatalogueValueFormula($user_id, $stock_id = 0)
    {
        $params = $this->getParamsForCatalogueValueFormula($user_id, $stock_id);
        $price_formula_generator = new PriceFormulaGenerator($params);
        return $price_formula_generator->generateCatalogueValueFormula();
    }

    private function getParamsForCatalogueValueFormula($user_id, $stock_id)
    {
        $user = $this->getUserParamsForCatalogueValueFormula($user_id);
        $prices = $this->getPgmPricesForCatalogueValueFormula($stock_id);

        $pgm_prices = TableRegistry::get('PgmPrices');
        return [
            'gram_ounce' => 31.1035,
            'treatment_charge' => $user->TC,
            'pgm_prices' => $prices,
            'metal_discount' => [
                'pt' => $pgm_prices->getElementDiscount('Pt') + $user->pt_PD,
                'pd' => $pgm_prices->getElementDiscount('Pd') + $user->pd_PD,
                'rh' => $pgm_prices->getElementDiscount('Rh') + $user->rh_PD,
            ],
            'recovery_rate' => [
                'pt' => $user->pt_RR / 100.0,
                'pd' => $user->pd_RR / 100.0,
                'rh' => $user->rh_RR / 100.0,
            ],
            'refining_charge' => [
                'pt' => $user->pt_RC,
                'pd' => $user->pd_RC,
                'rh' => $user->rh_RC,
            ],
            'leasing_charge' => [
                'pt' => $user->pt_LC,
                'pd' => $user->pd_LC,
                'rh' => $user->rh_LC,
            ],
            'user_discount' => [
                'percent' => $user->DU_pp,
                'value' => $user->DU_value,
            ],
            'leasing_days' => [
                'pt' => $user->pt_LD,
                'pd' => $user->pd_LD,
                'rh' => $user->rh_LD,
            ],
        ];

    }

    private function getUserParamsForCatalogueValueFormula($user_id)
    {
        $users = TableRegistry::get('Users');
        return $users->find()
            ->select([
                'rh_RC', 'pd_RC', 'pt_RC',
                'rh_PD', 'pd_PD', 'pt_PD',
                'rh_RR', 'pd_RR', 'pt_RR',
                'rh_LC', 'pd_LC', 'pt_LC',
                'pt_LD', 'pd_LD', 'rh_LD',
                'TC', 'DU_pp', 'DU_value',
            ])
            ->where(['id' => $user_id])
            ->first();
    }

    private function getPgmPricesForCatalogueValueFormula($stock_id)
    {
        $result = [];
        if ($stock_id == 0) {
            $result = TableRegistry::get('PgmPrices')->getActualPgmPricesArrary();
        } else {
            $result = TableRegistry::get('StockPgmPricesVersion')->getStockPricesArray($stock_id);
        }
        return $result;
    }

    public function addProductPricesToQuery($query, $user_id, $stock_id = 0)
    {

        $query = $query->select([
            'test.pt',
            'test.pd',
            'test.rh',
        ]);

        $exchange_rate = $this->getUserExchangeRate($user_id);

        $catalogue_value_formula = $this->generateCatalogueValueFormula($user_id, $stock_id);
        $catalogue_value_formula = '(' . $catalogue_value_formula . ')' . '* (' . $exchange_rate . ')';
        $catalogue_value_formula = 'CAST( (' . $catalogue_value_formula . ') AS DECIMAL(10,2))';
        $catalogue_value_formula = 'GREATEST(0, ' . $catalogue_value_formula . ')';

        $catalogue_value_expr = $query->newExpr()->add($catalogue_value_formula);
        $query->select(['catalogue_value' => $catalogue_value_expr]);

        return $query;
    }

    public function addUserPricesToQuery($query, $stock_id, $user_id)
    {

        $query = $query->select([
            'test.pt',
            'test.pd',
            'test.rh',
        ]);

        $exchange_rate = $this->getUserExchangeRate($user_id);

        $stocks = TableRegistry::get('Stocks');
        $stock = $stocks->get($stock_id);

        $catalogue_value_formula = $this->generateCatalogueValueFormula($user_id, $stock_id);
        $catalogue_value_formula = '(' . $catalogue_value_formula . ')' . '*' . $exchange_rate;
        $catalogue_value_formula = 'CAST( (' . $catalogue_value_formula . ') AS DECIMAL(10,2))';
        $catalogue_value_formula = 'GREATEST(0, ' . $catalogue_value_formula . ')';

        $user_value_formula = '(' . $catalogue_value_formula . ')' . ' * (1 - ' . 0.01 * $stock->margin . ') - ' . $stock->USD_margin;
        $user_value_formula = 'CAST( (' . $user_value_formula . ') AS DECIMAL(10,2))';
        $user_value_formula = 'GREATEST(0, ' . $user_value_formula . ')';

        $catalogue_value_expr = $query->newExpr()->add($catalogue_value_formula);
        $user_value_expr = $query->newExpr()->add($user_value_formula);
        $query->select(['catalogue_value' => $catalogue_value_expr]);
        $query->select(['user_value' => $user_value_expr]);

        return $query;
    }

    public function findByConditions(array $conditions = [], array $select_conditions = [])
    {

        $query = $this->getFullProductsDataQuerry($select_conditions);
        $query = $query->where($conditions);
        return $query;
    }

    public function getMinWeight()
    {

        $query = $this->getFullProductsDataQuerry();
        $query->find('all', [
            'order' => ['product_version.weight' => 'ASC'],
        ]);

        if ($query->count() > 0) {
            return $query->first()->product_version['weight'];
        }
    }

    public function getMaxWeight()
    {

        $query = $this->getFullProductsDataQuerry();
        $query->find('all', [
            'order' => ['product_version.weight' => 'DESC'],
        ]);

        if ($query->count() > 0) {
            return $query->first()->product_version['weight'];
        }
    }

    public function getProductFullDataQuery($product_id, $user_id)
    {
        $product = $this->findByConditions(['Products.id' => $product_id]);
        $product = $this->addProductPricesToQuery($product, $user_id);
        $product = $product->select([
            'cat_make.name',
            'product_version.additional_ref_data',
        ]);

        return $product;
    }

}
