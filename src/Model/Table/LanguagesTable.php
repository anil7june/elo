<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class LanguagesTable extends Table
{

    public function initialize(array $config)
    {

        $this->hasMany('Users');
        $this->hasMany('DisclaimerContents');

        $this->displayField('name');
    }

    public function getDefaultLanguageId()
    {
        return $this->findByCode('en')->first()->id;
    }

}
