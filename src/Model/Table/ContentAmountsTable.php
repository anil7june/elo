<?php
// src/Model/Table/ContentAmountsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ContentAmountsTable extends Table {

  public function initialize(array $config) {

    $this->hasMany('StockProducts');

    $this->displayField('name');
  }
}
