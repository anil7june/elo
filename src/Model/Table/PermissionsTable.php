<?php

// src/Model/Table/PermissionsTable
namespace App\Model\Table;

use Cake\ORM\Table;

class PermissionsTable extends Table
{

    public function initialize(array $config)
    {

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Roles', [
            'joinTable' => 'permissions_roles',
            'through' => 'PermissionsRoles',
        ]);
        $this->hasMany('PermissionsRoles');

        $this->addBehavior('Timestamp');
    }

}
