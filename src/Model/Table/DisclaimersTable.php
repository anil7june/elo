<?php
// src/Model/Table/DisclaimersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class DisclaimersTable extends Table
{

    public function initialize(array $config)
    {

        $this->hasMany('Users');
        $this->hasMany('DisclaimerContents');
    }

    public function getCurrentDisclaimer()
    {

        $disclaimer_version = $this->find()
            ->select([
                'version' => 'MAX(Disclaimers.version)',
            ]);
        return $this->findByVersion($disclaimer_version->first()->version);
    }
}
