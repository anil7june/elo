<?php

namespace App\Model\Table;

use Cake\I18n\Time;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class TokensTable extends Table
{
    public function initialize(array $config)
    {
        $this->hasOne('Users');
    }

    public function tokenExists($token_str)
    {
        $found = $this->findByToken($token_str)->first();
        if ($found) {
            return true;
        }
        return false;
    }

    public function existsAndIsValid($token_str)
    {
        $token = $this->findByToken($token_str)->first();
        if (!$token) {
            return false;
        } else {
            $user_assign_to_token = TableRegistry::get('Users')->findByTokenId($token->id)->first();
            $token_date_created = new Time($token->created);
            return $user_assign_to_token && $token_date_created->wasWithinLast('7 days');
        }
    }
}
