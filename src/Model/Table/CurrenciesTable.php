<?php
// src/Model/Table/CurrenciesTable.php
namespace App\Model\Table;

use Cake\I18n\Time;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class CurrenciesTable extends Table
{
    private $code_to_symbol = [
        'USD' => '&#36;',
        'PLN' => '&#122;&#322;',
        'GBP' => '&#163;',
        'EUR' => '&#8364;',
        'MYR' => '&#82;&#77;',
    ];

    public function initialize(array $config)
    {

        $this->belongsTo('Users');
        $this->hasMany('ExchangeRateVersions');

        $this->displayField('code');
    }

    public function getAllCodesArray()
    {

        $currencies = $this->find()
            ->select(['code']);

        $result = [];
        foreach ($currencies as $currency) {
            $result[] = $currency->code;
        }

        return $result;
    }

    public function updateCurrencyRate($currencyCode, $exchangeRate)
    {

        $currency = $this->findByCode($currencyCode)->first();

        $exchangeRates = TableRegistry::get('ExchangeRateVersions');
        $oldVersion = $exchangeRates->find()
            ->where([
                'currency_id' => $currency->id,
                'version' => $currency->version,
            ])
            ->first();

        $newExchangeRate = $exchangeRates->newEntity([
            'currency_id' => $currency->id,
            'version' => $oldVersion->version + 1,
            'exchange_rate' => $exchangeRate,
            'created' => Time::now(),
        ]);

        $currency->version += 1;
        return $this->save($currency) && $exchangeRates->save($newExchangeRate);

    }

    public function joinWithExchangeRates($conditions = [])
    {

        $query = $this->find()
            ->select([
                'Currencies.code',
                'Currencies.id',
                'Currencies.name',
                'rate.exchange_rate',
            ])
            ->join([
                'rate' => [
                    'table' => 'exchange_rate_versions',
                    'type' => 'LEFT',
                    'conditions' => [
                        'rate.currency_id = Currencies.id',
                        'rate.version = Currencies.version',
                    ],
                ],
            ])
            ->where($conditions);

        return $query;
    }

    public function getExchangeRateForUser($user_id)
    {

        $users = TableRegistry::get('Users');

        $user = $users->get($user_id);

        $currency = $this->get($user->currency_id);
        $exchangeRate = $this->ExchangeRateVersions->find()
            ->where([
                'currency_id' => $currency->id,
                'version' => $currency->version,
            ])
            ->first()->exchange_rate;

        return $exchangeRate;
    }

    public function getNewestExchangeRateForCurrency($currency_id)
    {
        $currency = $this->get($user->currency_id);
        return $this->ExchangeRateVersions->find()
            ->where([
                'currency_id' => $currency->id,
                'version' => $currency->version,
            ])
            ->first()->exchange_rate;

    }

    public function getCurrencyCodeForUser($user_id)
    {
        $user = TableRegistry::get('Users')->get($user_id);
        return $this->get($user->currency_id)->code;
    }

    public function getCurrencySymbol($currency_code)
    {
        return $this->code_to_symbol[$currency_code];
    }

    public function getCurerncySymbolForUser($user_id)
    {
        $currency_id = TableRegistry::get('Users')->get($user_id)->currency_id;
        $currency_code = $this->get($currency_id)->code;

        return $this->getCurrencySymbol($currency_code);

    }

}
