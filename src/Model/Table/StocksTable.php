<?php
// src/Model/Table/StocksTable.php
namespace App\Model\Table;

use App\Utility\Converter;
use App\Utility\Formater;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class StocksTable extends Table
{

    public function initialize(array $confid)
    {
        $this->belongsTo('Users');
        $this->belongsTo('StockStatuses');

        $this->hasMany('StockProducts');
        $this->hasMany('Preproducts');
        $this->hasMany('StockPgmPricesVersion');
    }

    public function deactivateAllStocks($user_id)
    {
        $exists = $this->findByUserId($user_id)->toArray();

        if (count($exists)) {
            $this
                ->query()
                ->update()
                ->set(['active' => false])
                ->where(['user_id' => $user_id])
                ->execute();
        }
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('name', __('Name can not be empty'))
            ->notEmpty(['margin', 'USD_margin'], __('Margin can not be empty'))
            ->add('name', [
                'notBlank' => [
                    'rule' => 'notBlank',
                    'message' => __('Please name your stock.'),
                ],
                'unique' => [
                    'rule' => function ($value, array $context) {
                        $find = $this->find()
                            ->where([
                                'user_id' => $context['data']['user_id'],
                                'name' => trim($context['data']['name']),
                            ]);
                        return $find->count() === 0;
                    },
                    'message' => __('Stock of given name already exists'),
                ],
            ]);

        return $validator;
    }

    public function getRecentStock($user_id)
    {
        $recent_stock_id = TableRegistry::get('Users')->get($user_id)->recent_stock_id;
        $recent_stock = $this->find('all')
            ->where([
                'id' => $recent_stock_id,
            ])
            ->first();

        if (!$recent_stock) {
            return 0;
        } else {
            return $recent_stock;
        }
    }

    public function getAllUserStocks($user_id)
    {

        return $this->find('all')
            ->where([
                'user_id' => $user_id,
            ])
            ->contain([
                'StockStatuses',
            ]);
    }

    public function getAllInactiveStocks($user_id)
    {

        $inactiveStocks = $this
            ->find('all')
            ->where([
                'user_id' => $user_id,
                'active' => false,
            ]);

        return $inactiveStocks;
    }

    public function getAllStockProductsFromStock($stock_id)
    {
        $stockProducts = $this->StockProducts->find('all')
            ->where([
                'stock_id' => $stock_id,
            ])
            ->contain(['ProductVersions']);
        return $stockProducts;
    }

    public function isStockProductDeletedFromCatalog($stock_product_id)
    {

        $result = $this->StockProducts->findById($stock_product_id)
            ->contain(['ProductVersions.Products'])
            ->first()->product_version->product;

        return $result->deleted;
    }

    public function getQueryForManageUsersStock($user_id, $conditions = [])
    {
        $stock_ids = $this->getStocksIdsFromConditions($conditions);
        if ($stock_ids->count() > 0) {
            return $this->combineFullStockQueries($stock_ids, $user_id);
        }
        return $this->emptyQuery();
    }

    private function getStocksIdsFromConditions($conditions)
    {
        return $stocks = $this->find()
            ->select(['Stocks.id'])
            ->contain(['Users'])
            ->where($conditions);
    }

    private function combineFullStockQueries($stock_ids, $user_id)
    {
        $result_query = $this->query();
        $i = 0;
        foreach ($stock_ids as $stock) {
            if ($i == 0) {
                $result_query = $this->getFullStockQuery($stock->id, $user_id);
            } else {
                $query = $this->getFullStockQuery($stock->id, $user_id);
                $result_query = $result_query->unionAll($query);
            }
            $i += 1;
        }
        return $result_query;
    }

    public function emptyQuery()
    {
        return $this->findById(-1);
    }

    private function getUsersStocksBasicData($query)
    {
        $query = $query
            ->select([
                'Stocks.id',
                'Stocks.created',
                'Stocks.name',
                'Stocks.margin',
                'Stocks.USD_margin',
                'StockStatuses.name',
                'Users.first_name',
                'Users.last_name',
                'Users.email',
                'Users.id',
            ])
            ->contain(['Users', 'StockStatuses']);
        return $query;
    }

    private function getStockPricingData($stock_id)
    {
        $query = $this->find()
            ->join([
                'stock_product' => [
                    'table' => 'stock_products',
                    'type' => 'LEFT',
                    'conditions' => [
                        'stock_product.stock_id =' . $stock_id,
                    ],
                ],
                'product_version' => [
                    'table' => 'product_versions',
                    'type' => 'LEFT',
                    'conditions' => [
                        'product_version.id = stock_product.product_version_id',
                    ],
                ],
            ])
            ->where(['Stocks.id' => $stock_id]);
        return $query;
    }

    public function getFullStockQuery($stock_id, $user_id)
    {
        $query = $this->getStockQuery($stock_id);
        $query = $this->addAllFormulasToStockQuery($query, $user_id);
        $query = $this->addValuesToHedgeToFullStockQuery($query, $user_id, $stock_id);
        return $query;
    }

    private function addValuesToHedgeToFullStockQuery($query, $user_id)
    {
        $query = $this->getTestsParametersForStockProductsInFullStockQuery($query);
        foreach (['pt', 'pd', 'rh'] as $element) {
            $query = $this->addExpressionToQuery($query, $element . '_to_hedge', $this->generateValueToHedgeFormulaForElement($element));
        }
        return $query;
    }

    private function getTestsParametersForStockProductsInFullStockQuery($query)
    {
        $query = $query
            ->join([
                'product' => [
                    'table' => 'products',
                    'type' => 'LEFT',
                    'conditions' => [
                        'product.version = product_version.version',
                        'product.id = product_version.product_id',
                    ],
                ],
                'test' => [
                    'table' => 'tests',
                    'type' => 'LEFT',
                    'conditions' => [
                        'test.id = product.test_id',
                    ],
                ],
                'content_amount' => [
                    'table' => 'content_amounts',
                    'type' => 'LEFT',
                    'conditions' => [
                        'stock_product.content_amount_id = content_amount.id',
                    ],
                ],
            ]);
        return $query;
    }

    private function generateValueToHedgeFormulaForElement($element)
    {
        return 'stock_product.no_of_pcs * test.' . $element . ' * content_amount.value * 0.8';
    }

    private function getStockQuery($stock_id)
    {

        $query = $this->getStockPricingData($stock_id);
        $query = $this->getUsersStocksBasicData($query);

        return $query;
    }

    private function addAllFormulasToStockQuery($query, $user_id)
    {
        $formula_generators = $this->getGeneratingMethods();
        $exchange_rate = $this->Users->getUserExchangeRate($user_id);
        foreach ($formula_generators as $generator) {
            $query = $this->addExpressionToQuery($query, $this->getGeneratorParamName($generator), $this->{$generator}($exchange_rate));
        }
        return $query;
    }

    private function addExpressionToQuery($query, $param_name, $formula)
    {
        return $query->select([$param_name => $query->newExpr()->add($formula)]);
    }

    private function getGeneratingMethods()
    {
        $methods = get_class_methods($this);
        $result = [];
        foreach ($methods as $method) {
            if (substr($method, 0, 10) == '_generate_') {
                $result[] = $method;
            }
        }
        return $result;
    }

    private function getGeneratorParamName($generator_name)
    {
        $arr = explode('__', $generator_name);
        return $arr[1];
    }

    private function roundFormula($formula)
    {
        return 'CAST( (' . $formula . ') AS DECIMAL(10,2))';
    }

    private function multiplyFormula($col_name, $value)
    {
        return '(' . $col_name . '*' . $value . ')';
    }

    private function generateSumOfValuesFormula($col_name, $exchange_rate = 1)
    {
        $formula = 'SUM( ' . $col_name . ' * ' . $exchange_rate . ')';
        return $formula;
    }

    private function generateAveragePerPcFormula($formula)
    {
        $formula = $this->divideFormulas($formula, $this->_generate__total_count__Formula());
        return $this->roundFormula($formula);
    }

    private function generateAveragePerKgFormula($formula)
    {
        $formula = $this->divideFormulas($formula, $this->_generate__total_weight__Formula());
        return $this->roundFormula($formula);
    }

    private function divideFormulas($formula1, $formula2)
    {
        return '(' . $formula1 . ')/' . '(' . $formula2 . ')';
    }

    private function _generate__total_count__Formula($exchange_rate = 1)
    {
        return $this->generateSumOfValuesFormula('stock_product.no_of_pcs');
    }

    private function _generate__total_weight__Formula($exchange_rate = 1)
    {
        return $this->generateSumOfValuesFormula('product_version.weight * stock_product.no_of_pcs');
    }

    private function _generate__total_catalogue_value__Formula($exchange_rate)
    {
        $formula = $this->generateSumOfValuesFormula('stock_product.catalogue_value * stock_product.no_of_pcs', $exchange_rate);
        return $this->roundFormula($formula);
    }

    private function _generate__total_user_value__Formula($exchange_rate)
    {
        $formula = $this->generateSumOfValuesFormula('stock_product.user_value * stock_product.no_of_pcs', $exchange_rate);
        return $this->roundFormula($formula);
    }

    private function _generate__total_profit_value__Fromula($exchange_rate)
    {
        return '( ' . $this->_generate__total_catalogue_value__Formula($exchange_rate) . '-' . $this->_generate__total_user_value__Formula($exchange_rate) . ' )';
    }

    private function _generate__catalogue_value_per_kg__Formula($exchange_rate)
    {
        $catalogue_value_formula = $this->_generate__total_catalogue_value__Formula($exchange_rate);
        return $this->generateAveragePerKgFormula($catalogue_value_formula);
    }

    private function _generate__profit_per_kg__Formula($exchange_rate)
    {
        $total_profit_formula = $this->_generate__total_profit_value__Fromula($exchange_rate);
        return $this->generateAveragePerKgFormula($total_profit_formula);
    }

    private function _generate__profit_per_pc__Formula($exchange_rate)
    {
        $total_profit_formula = $this->_generate__total_profit_value__Fromula($exchange_rate);
        return $this->generateAveragePerPcFormula($total_profit_formula);
    }

    private function _generate__avg_margin_per_pc__Formula($exchange_rate)
    {
        $total_profit_formula = $this->_generate__total_profit_value__Fromula($exchange_rate);
        return $this->generateAveragePerPcFormula($total_profit_formula);
    }

    private function _generate__avg_user_value_per_pc__Formula($exchange_rate)
    {
        $total_user_value_formula = $this->_generate__total_user_value__Formula($exchange_rate);
        return $this->generateAveragePerPcFormula($total_user_value_formula);
    }

    private function _generate__avg_catalogue_value_per_pc__Formula($exchange_rate)
    {
        $total_catalogue_value_formula = $this->_generate__total_catalogue_value__Formula($exchange_rate);
        return $this->generateAveragePerPcFormula($total_catalogue_value_formula);
    }

    public function getStockProductsQuery($stock_id, $user_id)
    {
        $query = $this->getStockProductsBasicDataQuery($stock_id, $user_id);
        $query = $this->getPricingParamsForStockProductsQuery($query, $user_id);
        return $query;
    }

    private function getStockProductsBasicDataQuery($stock_id, $user_id)
    {
        $query = $this->StockProducts->find()
            ->select([
                'StockProducts.id',
                'product_version.cat_code',
                'StockProducts.no_of_pcs',
                'ContentAmounts.id',
                'ContentAmounts.name',
                'make.name',
                'cat_make.name',
                'image.file_name',
                'product.id',
            ])
            ->join([
                'product_version' => [
                    'table' => 'product_versions',
                    'type' => 'LEFT',
                    'conditions' => [
                        'StockProducts.product_version_id = product_version.id',
                    ],
                ],
                'product' => [
                    'table' => 'products',
                    'type' => 'LEFT',
                    'conditions' => [
                        'product_version.product_id = product.id',
                    ],
                ],
                'image' => [
                    'table' => 'images',
                    'type' => 'LEFT',
                    'conditions' => [
                        'image.id = product.image_id',
                    ],
                ],
                'make' => [
                    'table' => 'makes',
                    'type' => 'LEFT',
                    'conditions' => [
                        'product_version.make_id = make.id',
                    ],
                ],
                'cat_make' => [
                    'table' => 'cat_makes',
                    'type' => 'LEFT',
                    'conditions' => [
                        'product_version.cat_make_id = cat_make.id',
                    ],
                ],
            ])
            ->contain([
                'ContentAmounts',
            ])
            ->where([
                'StockProducts.stock_id' => $stock_id,
            ]);
        return $query;
    }
    private function getPricingParamsForStockProductsQuery($query, $user_id)
    {
        $exchange_rate = TableRegistry::get('Users')->getUserExchangeRate($user_id);
        $generators = $this->getStockProductsGeneratotsMethods();
        foreach ($generators as $generator) {
            $query = $this->{$generator}($query, $exchange_rate);
        }
        return $query;
    }

    private function getStockProductsGeneratotsMethods()
    {
        $methods = get_class_methods($this);
        $result = [];
        foreach ($methods as $method) {
            if (strstr($method, '__stock_product_generator__')) {
                $result[] = $method;
            }
        }
        return $result;
    }

    private function __stock_product_generator__addCatalogueValueFormulaToQuery($query, $exchange_rate)
    {
        return $this->addExpressionToQuery($query, 'catalogue_value', $this->generateCatalogueValueFormula($exchange_rate));
    }

    private function __stock_product_generator__addUserValueFormulaToQuery($query, $exchange_rate)
    {
        return $this->addExpressionToQuery($query, 'user_value', $this->generateUserValueFormula($exchange_rate));
    }

    private function __stock_product_generator__addProftValueFormulaToQuery($query, $exchange_rate)
    {
        return $this->addExpressionToQuery($query, 'profit', $this->generateProfitFormula($exchange_rate));
    }

    private function generateCatalogueValueFormula($exchange_rate)
    {
        $formula = $this->multiplyFormula('StockProducts.catalogue_value', $exchange_rate);
        return $this->roundFormula($formula);
    }

    private function generateUserValueFormula($exchange_rate)
    {
        $formula = $this->multiplyFormula('StockProducts.user_value', $exchange_rate);
        return $this->roundFormula($formula);
    }

    private function generateProfitFormula($exchange_rate)
    {
        $formula = '(' . $this->generateCatalogueValueFormula($exchange_rate) . '-' . $this->generateUserValueFormula($exchange_rate) . ')';
        return $this->roundFormula($formula);
    }

    public function getKeyValueStockSummaryData($stock, $user_id)
    {
        $currency_symbol = TableRegistry::get('Currencies')->getCurerncySymbolForUser($user_id);

        $result = [
            [
                __('Products total') . ': ' => Formater::formatNumber($stock->total_count, $user_id),
                __('Total value') . ': ' => Formater::formatNumber($stock->total_catalogue_value, $user_id) . ' ' . $currency_symbol,
                __('Avg margin per pc') . ': ' => Formater::formatNumber($stock->avg_margin_per_pc, $user_id) . ' ' . $currency_symbol,
            ],
            [
                __('Margin') . ': ' => Formater::formatNumber($stock->margin, $user_id) . '%, ' . Formater::formatNumber(Converter::convert($stock->USD_margin, $user_id), $user_id) . ' ' . $currency_symbol,
                __('Total price') . ': ' => Formater::formatNumber($stock->total_user_value, $user_id) . ' ' . $currency_symbol,
                __('Avg price offered per pc') . ': ' => Formater::formatNumber($stock->avg_user_value_per_pc, $user_id) . ' ' . $currency_symbol,
            ],
            [
                __('Catalog value per kg') . ': ' => Formater::formatNumber($stock->catalogue_value_per_kg, $user_id) . ' ' . $currency_symbol,
                __('Total profit') . ': ' => Formater::formatNumber($stock->total_profit_value, $user_id) . ' ' . $currency_symbol,
                __('Avg catalog value per pc') . ': ' => Formater::formatNumber($stock->avg_catalogue_value_per_pc, $user_id) . ' ' . $currency_symbol,
            ],
        ];
        if (TableRegistry::get('Roles')->hasGranted($user_id, 'see_value_to_hedge')) {
            $result[] = [
                __('Pt to hedge:') . ' ' => Formater::formatNumber($stock->pt_to_hedge, $user_id) . ' ' . $currency_symbol . '/toz',
                __('Pd to hedge:') . ' ' => Formater::formatNumber($stock->pd_to_hedge, $user_id) . ' ' . $currency_symbol . '/toz',
                __('Rh to hedge:') . ' ' => Formater::formatNumber($stock->rh_to_hedge, $user_id) . ' ' . $currency_symbol . '/toz',
            ];
        }
        return $result;
    }

    public function changeStockStatus($stock_id, $status_id)
    {
        $stock = $this->get($stock_id);
        $stock->stock_status_id = $status_id;
        if ($this->save($stock)) {
            return true;
        } else {
            return false;
        }
    }

}
