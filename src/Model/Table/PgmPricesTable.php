<?php
// src/Model/Table/PgmPricesTable.php
namespace App\Model\Table;

use Cake\I18n\Time;
use Cake\ORM\Table;

class PgmPricesTable extends Table
{

    public function initialize(array $config)
    {
        $this->hasMany('PgmPriceVersions');
        $this->hasMany('StockPgmPricesVersion');
    }

    public function getActualPgmPricesArrary()
    {
        return [
            'pt' => $this->getElementPrice('Pt'),
            'pd' => $this->getElementPrice('Pd'),
            'rh' => $this->getElementPrice('Rh'),
        ];
    }

    public function getElementPrice($element)
    {
        $price = $this->findByName($element)->first();
        $priceVersion = $this->PgmPriceVersions->find()
            ->where([
                'pgm_price_id' => $price->id,
                'version' => $price->version,
            ])
            ->first();
        return $priceVersion->price;
    }

    public function setElementPrice($elementFullName, $value)
    {
        $price = $this->find()->where(['full_name' => $elementFullName])->first();
        $oldVersion = $this->PgmPriceVersions->find()
            ->where([
                'pgm_price_id' => $price->id,
                'version' => $price->version,
            ])
            ->first();

        $newVersion = $this->PgmPriceVersions->newEntity([
            'pgm_price_id' => $price->id,
            'version' => $price->version + 1,
            'price' => $value,
            'discount' => $oldVersion->discount,
            'created' => Time::now(),
        ]);

        $price->version += 1;

        $this->PgmPriceVersions->save($newVersion);
        $this->save($price);

    }

    public function getElementDiscount($element)
    {

        $price = $this->findByName($element)->first();
        $priceVersion = $this->PgmPriceVersions->find()
            ->where([
                'pgm_price_id' => $price->id,
                'version' => $price->version,
            ])
            ->first();

        return $priceVersion->discount;
    }

    public function setElementDiscount($element, $value)
    {

        $price = $this->findByName($element)->first();
        $oldVersion = $this->PgmPriceVersions->find()
            ->where([
                'pgm_price_id' => $price->id,
                'version' => $price->version,
            ])
            ->first();

        $newVersion = $this->PgmPriceVersions->newEntity([
            'pgm_price_id' => $price->id,
            'price' => $oldVersion->price,
            'version' => $price->version + 1,
            'discount' => $value,
            'created' => Time::now(),
        ]);

        $this->PgmPriceVersions->save($newVersion);
        $price->version += 1;
        $this->save($price);
    }

}
