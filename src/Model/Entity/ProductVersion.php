<?php
// src/Model/Entity/ProductVersion.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class ProductVersion extends Entity {

  protected $_accessible = [
    '*' => true
  ];
}
